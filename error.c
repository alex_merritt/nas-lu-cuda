// C port of NPB3.2
// subroutine error
#include "applu.h"
#include "mpinpb.h"
#include <math.h>

/**
 * Compute the solution error.
 */
void error() {

	/**
 	 * Local variables.
	 */
	int i, j, k, m;
	int iglob, jglob;
	fp_type tmp;
	fp_type u000ijk[5], dummy[5];
	
	const fp_type fpzero = 0.0e+00;
	
	for (m = 0; m < 5; m++) {
		errnm[m] = fpzero;
		dummy[m] = fpzero;
	}

	for (k = 1; k <= nz - 2; k++) {
		for (j = jst; j <= jend; j++) {
			jglob = jpt + j;
			for (i = ist; i <= iend; i++) {
				iglob = ipt + i;
				exact(iglob, jglob, k, u000ijk);
				for (m = 0; m < 5; m++) {
					tmp = ( u000ijk[m] - u[k][j][i][m] );
					dummy[m] = dummy[m] + (tmp * tmp);
				}
			}
		}		
	}

	/**
	 * Compute the global sum of individual contributions to dot product.
	 */
	MPI_Allreduce(dummy, errnm, 5, MPI_FP_TYPE, MPI_SUM, MPI_COMM_WORLD);

	for (m = 0; m < 5; m++) {
		errnm[m] = sqrt ( errnm[m] / ( (nx0-2)*(ny0-2)*(nz0-2) ) );
	}

	/* if (id != 0) {
		printf("RMS-norm of error in soln. to first pde = %d.\n", errnm[0]);
		printf("RMS-norm of error in soln. to second pde = %d.\n", errnm[1]);
		printf("RMS-norm of error in soln. to third pde = %d.\n", errnm[2]);
		printf("RMS-norm of error in soln. to fourth pde = %d.\n", errnm[3]);
		printf("RMS-norm of error in soln. to fifth pde = %d.\n", errnm[4]);
	}*/

}
