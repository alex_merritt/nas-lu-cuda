// C port of NPB3.2
// subroutine l2norm
#include "size.h"
#include "applu.h"
#include "mpinpb.h"
#include "timers.h"
#include <math.h>

void l2norm (int ldx, int ldy, int ldz,
			 int nx0, int ny0, int nz0,
			 int ist, int iend,
			 int jst, int jend,
			 fp_type**** v, fp_type sum[5]) {
			 

	/**
	 * Local variables.
	 */
	int i, j, k, m;
	
	const fp_type fpzero = 0.0e+00;
	
	timer_start(7);	

	for (m = 0; m < 5; m++) {
		sum[m] = fpzero;
	}
	
	for (k = 1; k <= nz0 - 2; k++) {
		for (j = jst; j <= jend; j++) {
			for (i = ist; i <= iend; i++) {
				for (m = 0; m < 5; m++) {
					sum[m] = sum[m] + v[k][j][i][m] * v[k][j][i][m];
				}
			}
		}
	}
	
	/**
	 * Compute the global sum of individual contributions to dot product.
	 */
	MPI_Allreduce(MPI_IN_PLACE, sum, 5, MPI_FP_TYPE, MPI_SUM, MPI_COMM_WORLD);
	 
	 for (m = 0; m < 5; m++) {
	 	sum[m] = sqrt ( sum[m] / ( (nx0-2)*(ny0-2)*(nz0-2) ) );
	 }
	 
	timer_stop(7);

}
