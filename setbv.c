// C port of NPB3.2
// subroutine setbv
#include "applu.h"

/**
 * Set the boundary values of dependent variables.
 */
void setbv() {

	/**
	 * Local variables.
	 */
	int i, j, k;
	int iglob, jglob;
	
	/**
	 * Set the dependent variable values along the top and bottom faces.
	 */
	for (j = 2; j <= ny + 1; j++) {
	
		jglob = jpt + j;
		
		for (i = 2; i <= nx + 1; i++) {
		
			iglob = ipt + i;
			exact(iglob, jglob, 0, &u[0][j][i][0]);
			exact(iglob, jglob, nz - 1, &u[nz-1][j][i][0]);
		
		}
	
	}
	
	/**
	 * Set the dependent variable values along north and south faces.
	 */
	if (west == -1 ) {
	
		for (k = 0; k <= nz - 1; k++) {
			for (i = 2;  i <= nx + 1; i++) {
				
				iglob = ipt + i;
				exact(iglob, 2, k, &u[k][2][i][0]);	
				
			}
		}
	
	}
	
	if (east == -1) {
	
		for (k = 0; k <= nz - 1; k++) {
			for (i = 2; i <= nx + 1; i++) {
			
				iglob = ipt + i;
				exact(iglob, ny0 + 1, k, &u[k][ny+1][i][0]);
			
			}
		}
	
	}

	/**
	 * Set the dependent variable values along east and west faces.
	 */
	if (north == -1) {
	
		for (k = 0; k <= nz - 1; k++) {
			for (j = 2; j <= ny + 1; j++) {
			
				jglob = jpt + j;
				exact(2, jglob, k, &u[k][j][2][0]);
			
			}
		}
	
	}
	
	if (south == -1) {
		
		for (k = 0; k <= nz - 1; k++) {
			for (j = 2; j <= ny + 1; j++) {
			
				jglob = jpt + j;
				exact(nx0 + 1, jglob, k, &u[k][j][nx+1][0]);
			
			}
		}	
		
	}

}
