#include <stdio.h>
#include <stdlib.h>

#include "applu.h"
#include "timers.h"

/**
 * Print all of the timers.
 */
void print_timers() {

	printf(" TIMING RESULTS:\n");
	timer_print_format();	
	timer_print("SSOR  ", 0);
	timer_print("PRE   ", 1);
	timer_print("JACLD ", 2);
	timer_print("BLTS  ", 3);
	timer_print("JACU  ", 4);
	timer_print("BUTS  ", 5);
	timer_print("POST  ", 6);
	timer_print("L2NORM", 7);
	timer_print("RHS   ", 8);
	timer_print("REARR ", 9);
	timer_print("EX1   ", 10);
	timer_print("EX3   ", 11);

}

