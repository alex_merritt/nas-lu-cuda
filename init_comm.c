// C port of NPB3.2
// subroutine init-comm
#include "applu.h"
#include "mpinpb.h"

/**
 *   initialize MPI and establish rank and size
 *
 * This is a module in the MPI implementation of LUSSOR
 * pseudo application from the NAS Parallel Benchmarks. 
*/
void init_comm(int argc, char** argv) {

	// Initialize MPI communication.
	MPI_Init(&argc, &argv);

	// Establish the global rank of this process.
	MPI_Comm_rank(MPI_COMM_WORLD, &id);

	// Establish the size of the global group.
	MPI_Comm_size(MPI_COMM_WORLD, &num);
	
	/**
	 * num - number of nodes (processors)
	 * nodedim - a function such that it computes the exponent where num = 2 ^ nodedim
	 *			 i.e. ndim is the square-root of num.
	 */
	 ndim = nodedim(num);

}
