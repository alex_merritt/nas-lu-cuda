// C port of NPB3.2
// subroutine subdomain
#include "applu.h"
#include "mpinpb.h"
#include <stdio.h>

/**
 * Set up the sub-domain sizes.
 */
void subdomain() {

	/**
	 * Local variables.
	 */
	int mm, ierror, errorcode;
	
	// x dimension
	mm = nx0 % xdim;
	if (row <= mm) {
		nx = nx0 / xdim + 1;
		ipt = (row - 1) * nx;
	} else {
		nx = nx0 / xdim;
		ipt = (row - 1) * nx + mm;
	}
	
	// y dimension
	mm = ny0 % ydim;
	if (col <= mm) {
		ny = ny0 / ydim + 1;
		jpt = (col - 1) * ny;
	} else {
		ny = ny0 / ydim;
		jpt = (col - 1) * ny + mm;
	}
	
	// z dimension
	nz = nz0;

	// Check the sub-domain size.
	if (nx < 4 || ny < 4 || nz < 4 ) {
		printf(" Subdomain size is too small - adjust problem size or number of processors so that ");
		printf(" nx, ny and nz are greater than or equal to 4.  They are currently %d, %d, %d.\n", nx, ny, nz);
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	
	if (nx > isiz1 || ny > isiz2 || nz > isiz3) {
		printf(" Subdomain size is too large - adjust problem size or number of processors so that ");
		printf(" nx, ny and nz are less than or equal to isiz1, isiz2 and isiz3 respectively. They are ");
		printf(" currently %d, %d, %d.\n", nx, ny, nz);
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	
	if (id == 0) {
		printf(" Subdomain: %d x %d x %d.\n", nx, ny, nz);
	}

	// Set up the start and end in i and j extents for all processors.
	// Originals:
	/*ist = 1;
	iend = nx;
	if (north == -1) { ist = 2; }
	if (south == -1) { iend = nx - 1; }
	
	jst = 1;
	jend = ny;
	if (west == -1) { jst = 2; }
	if (east == -1) { jend = ny - 1; }*/
	
	// New versions.
	ist = 2;
	iend = nx + 1;
	if (north == -1) { ist = 3; }
	if (south == -1) { iend = nx; }
	
	jst = 2;
	jend = ny + 1;
	if (west == -1) { jst = 3; }
	if (east == -1) { jend = ny; }	

}
