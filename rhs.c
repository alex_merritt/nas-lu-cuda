// C port of NPB3.2
// subroutine rhs
#include "applu.h"

/**
 * Compute the right hand sides.
 */
void rhs() {

	/**
	 * Local variables.
	 */
	int i, j, k, m;
	int iex;
	int L1, L2;
	int ist1, iend1;
	int jst1, jend1;
	fp_type q;
	fp_type u21, u31, u41;
	fp_type tmp;
	fp_type u21i, u31i, u41i, u51i;
	fp_type u21j, u31j, u41j, u51j;
	fp_type u21k, u31k, u41k, u51k;
	fp_type u21im1, u31im1, u41im1, u51im1;
	fp_type u21jm1, u31jm1, u41jm1, u51jm1;
	fp_type u21km1, u31km1, u41km1, u51km1;	
	
	 // Constants.
	 const fp_type c1 = c1_def;
	 const fp_type c2 = c2_def;
	 const fp_type c3 = c3_def;
	 const fp_type c4 = c4_def;
	 const fp_type c5 = c5_def;		 
	 
	 const fp_type fpzero = 0.0e+00;
	 const fp_type fphalf = 0.50e+00;
	 const fp_type fpone = 1.0e+00;
	 const fp_type fptwo = 2.0e+00;
	 const fp_type fpthree = 3.0e+00;
	 const fp_type fpfour = 4.0e+00;
	 const fp_type fpfive = 5.0e+00;
	 const fp_type fpsix = 6.0e+00;

	for (k = 0; k <= nz - 1; k++) {
		for (j = 2; j <= ny + 1; j++) {
			for (i = 2; i <= nx + 1; i++) {
				for (m = 0; m < 5; m++) {
				
					rsd[k][j][i][m] = -frct[k][j][i][m];
				
				}
			}
		}
	}
		
	// xi-direction flux differences.
	
	/**
	 * iex = flag : iex = 0  north/south communication
	 *            : iex = 1  east/west communication
	 */
	iex = 0;
	
	// Communicate and receive/send two rows of data.
	exchange_3(u, iex);
	
	L1 = 1;
	if (north == -1) { L1 = 2; }
	L2 = nx + 2;
	if (south == -1) { L2 = nx + 1; }
	
	for (k = 1; k <= nz - 2; k++) {
		for (j = jst; j<= jend; j++) {
			for (i = L1; i <= L2; i++) {
			
				flux[k][j][i][0] = u[k][j][i][1];
				u21 = u[k][j][i][1] / u[k][j][i][0];
				
				q = fphalf * (  u[k][j][i][1] * u[k][j][i][1]
								+ u[k][j][i][2] * u[k][j][i][2]
								+ u[k][j][i][3] * u[k][j][i][3] )
								/ u[k][j][i][0];
								
				flux[k][j][i][1] = u[k][j][i][1] * u21 + c2 *
									( u[k][j][i][4] - q );
				flux[k][j][i][2] = u[k][j][i][2] * u21;
				flux[k][j][i][3] = u[k][j][i][3] * u21;
				flux[k][j][i][4] = ( c1 * u[k][j][i][4] - c2 * q ) * u21;
				
			}
		}
	}

	for (k = 1; k <= nz - 2; k++) {
		for (j = jst; j <= jend; j++) {
			for (i = ist; i <= iend; i++) {
				for (m = 0; m < 5; m++) {
				
					rsd[k][j][i][m] = rsd[k][j][i][m]
										- tx2 * ( flux[k][j][i+1][m] - flux[k][j][i-1][m] );										
										
				}
			}		
			
			L2 = nx + 2;
			if (south == -1) { L2 = nx + 1; }
			
			for (i = ist; i <= L2; i++) {
			
				tmp = fpone / u[k][j][i][0];
				
				u21i = tmp * u[k][j][i][1];
				u31i = tmp * u[k][j][i][2];
				u41i = tmp * u[k][j][i][3];
				u51i = tmp * u[k][j][i][4];
				
				tmp = fpone / u[k][j][i-1][0];
				
				u21im1 = tmp * u[k][j][i-1][1];
				u31im1 = tmp * u[k][j][i-1][2];
				u41im1 = tmp * u[k][j][i-1][3];
				u51im1 = tmp * u[k][j][i-1][4];
			
				flux[k][j][i][1] = ( fpfour / fpthree ) * tx3 * (u21i - u21im1);
				flux[k][j][i][2] = tx3 * ( u31i - u31im1 );
				flux[k][j][i][3] = tx3 * ( u41i - u41im1 );
				flux[k][j][i][4] = fphalf * ( fpone - c1 * c5 )
									* tx3 * ( ( u21i * u21i + u31i * u31i + u41i * u41i )
											- ( u21im1 * u21im1 + u31im1 * u31im1 + u41im1 * u41im1 ) )
									+ (fpone/fpsix)
									* tx3 * ( u21i * u21i - u21im1 * u21im1 )
									+ c1 * c5 * tx3 * ( u51i - u51im1 );
								
			}


			for (i = ist; i <= iend; i++) {
			
				rsd[k][j][i][0] = rsd[k][j][i][0]
									+ dx1 * tx1 * (             u[k][j][i-1][0]
													- fptwo * u[k][j][i][0]
													+           u[k][j][i+1][0] );
				rsd[k][j][i][1] = rsd[k][j][i][1]
									+ tx3 * c3 * c4 * ( flux[k][j][i+1][1] - flux[k][j][i][1] )
									+ dx2 * tx1 *  (            u[k][j][i-1][1]
													- fptwo * u[k][j][i][1]
													+           u[k][j][i+1][1] );
				rsd[k][j][i][2] = rsd[k][j][i][2]
									+ tx3 * c3 * c4 * ( flux[k][j][i+1][2] - flux[k][j][i][2] )
									+ dx3 * tx1 * (             u[k][j][i-1][2]
													- fptwo * u[k][j][i][2]
													+           u[k][j][i+1][2] );
				rsd[k][j][i][3] = rsd[k][j][i][3]
									+ tx3 * c3 * c4 * ( flux[k][j][i+1][3] - flux[k][j][i][3] )
									+ dx4 * tx1 * (             u[k][j][i-1][3]
													- fptwo * u[k][j][i][3]
													+           u[k][j][i+1][3] );
				rsd[k][j][i][4] = rsd[k][j][i][4]
									+ tx3 * c3 * c4 * ( flux[k][j][i+1][4] - flux[k][j][i][4] )
									+ dx5 * tx1 * (             u[k][j][i-1][4]
													- fptwo * u[k][j][i][4]
													+           u[k][j][i+1][4] );

			}
			
			/**
			 * Fourth-order dissipation.
			 */
			if (north == -1) {
			
				for (m = 0; m < 5; m++) {
				
					rsd[k][j][3][m] = rsd[k][j][3][m]
										 - dssp * ( + fpfive * u[k][j][3][m]
													- fpfour * u[k][j][4][m]
													+           u[k][j][5][m] );
					rsd[k][j][4][m] = rsd[k][j][4][m]
										 - dssp * ( - fpfour * u[k][j][3][m]
										 			+ fpsix * u[k][j][4][m]
										 			- fpfour * u[k][j][5][m]
										 			+           u[k][j][6][m] );
				
				}
			
			}

			ist1 = 2;
			iend1 = nx + 1;
			if (north == -1) { ist1 = 5; }
			if (south == -1) { iend1 = nx - 2; }
			
			for (i = ist1; i <= iend1; i++) {
				for (m = 0; m < 5; m++) {
				
					rsd[k][j][i][m] = rsd[k][j][i][m]
										 - dssp * (             u[k][j][i-2][m]
										 			- fpfour * u[k][j][i-1][m]
										 			+ fpsix * u[k][j][i][m]
										 			- fpfour * u[k][j][i+1][m]
										 			+           u[k][j][i+2][m] );
				
				}
			}
			
			if (south == -1) {
			
				for (m = 0; m < 5; m++) {
				
					rsd[k][j][nx-1][m] = rsd[k][j][nx-1][m]
											- dssp * (              u[k][j][nx-3][m]
														- fpfour * u[k][j][nx-2][m]
														+ fpsix * u[k][j][nx-1][m]
														- fpfour * u[k][j][nx][m] );
					rsd[k][j][nx][m] = rsd[k][j][nx][m]
										 - dssp * (             u[k][j][nx-2][m]
										 			- fpfour * u[k][j][nx-1][m]
										 			+ fpfive * u[k][j][nx][m] );
				
				}
			
			}

		}
		
	}
					
	/**
	 * eta-direction flux differences.
	 */
	 
	// iex = flag : iex = 0  north/south communication
	iex = 1;
	 
	// Communicate and receive/send two rows of data.
	exchange_3(u, iex);

	L1 = 1;
	if (west == -1) { L1 = 2; }
	L2 = ny + 2;
	if (east == -1) { L2 = ny + 1; }
	
	for (k = 1; k <= nz - 2; k++) {
		for (i = ist; i <= iend; i++) {
			for (j = L1; j <= L2; j++) {
			
				flux[k][j][i][0] = u[k][j][i][2];
				u31 = u[k][j][i][2] / u[k][j][i][0];
				
				q = fphalf * (  u[k][j][i][1] * u[k][j][i][1]
								+ u[k][j][i][2] * u[k][j][i][2]
								+ u[k][j][i][3] * u[k][j][i][3] )
								/ u[k][j][i][0];

				flux[k][j][i][1] = u[k][j][i][1] * u31;
				flux[k][j][i][2] = u[k][j][i][2] * u31 + c2 * ( u[k][j][i][4] - q );
				flux[k][j][i][3] = u[k][j][i][3] * u31;
				flux[k][j][i][4] = ( c1 * u[k][j][i][4] - c2 * q ) * u31;
				
			}
		}
	}
		
	for (k = 1; k <= nz - 2; k++) {
		for (i = ist; i <= iend; i++) {
		
			for (j = jst; j <= jend; j++) {
				for (m = 0; m < 5; m++) {
				
					rsd[k][j][i][m] = rsd[k][j][i][m]
										- ty2 * ( flux[k][j+1][i][m] - flux[k][j-1][i][m] );
					
					}
			}
			
			L2 = ny + 2;
			if (east == -1) { L2 = ny + 1; }
			
			for (j = jst; j <= L2; j++) {
			
				tmp = fpone / u[k][j][i][0];
				
				u21j = tmp * u[k][j][i][1];
				u31j = tmp * u[k][j][i][2];
				u41j = tmp * u[k][j][i][3];
				u51j = tmp * u[k][j][i][4];
				
				tmp = fpone / u[k][j-1][i][0];
				u21jm1 = tmp * u[k][j-1][i][1];
				u31jm1 = tmp * u[k][j-1][i][2];
				u41jm1 = tmp * u[k][j-1][i][3];
				u51jm1 = tmp * u[k][j-1][i][4];

				flux[k][j][i][1] = ty3 * ( u21j - u21jm1 );
				flux[k][j][i][2] = (fpfour/fpthree) * ty3 * (u31j - u31jm1);
				flux[k][j][i][3] = ty3 * ( u41j - u41jm1 );
				flux[k][j][i][4] = fphalf * ( fpone - c1 * c5 )
									* ty3 * ( ( u21j * u21j + u31j * u31j + u41j * u41j )
											- ( u21jm1 * u21jm1 + u31jm1 * u31jm1 + u41jm1 * u41jm1 ) )
											+ (fpone/fpsix)
											* ty3 * ( u31j * u31j - u31jm1 * u31jm1 )
											+ c1 * c5 * ty3 * ( u51j - u51jm1 );
											
			}

			for (j = jst; j <= jend; j++) {
			
				rsd[k][j][i][0] = rsd[k][j][i][0]
									+ dy1 * ty1 * (             u[k][j-1][i][0]
													- fptwo * u[k][j][i][0]
													+           u[k][j+1][i][0] );
													
				rsd[k][j][i][1] = rsd[k][j][i][1]
									+ ty3 * c3 * c4 * ( flux[k][j+1][i][1] - flux[k][j][i][1] )
									+ dy2 * ty1 * (             u[k][j-1][i][1]
													- fptwo * u[k][j][i][1]
													+           u[k][j+1][i][1] );
													
				rsd[k][j][i][2] = rsd[k][j][i][2]
									+ ty3 * c3 * c4 * (flux[k][j+1][i][2] - flux[k][j][i][2] )
									+ dy3 * ty1 * (             u[k][j-1][i][2]
													- fptwo * u[k][j][i][2]
													+           u[k][j+1][i][2] );
													
				rsd[k][j][i][3] = rsd[k][j][i][3]
									+ ty3 * c3 * c4 * (flux[k][j+1][i][3] - flux[k][j][i][3] )
									+ dy4 * ty1 * (             u[k][j-1][i][3]
													- fptwo * u[k][j][i][3]
													+           u[k][j+1][i][3] );
													
				rsd[k][j][i][4] = rsd[k][j][i][4]
									+ ty3 * c3 * c4 * (flux[k][j+1][i][4] - flux[k][j][i][4] )
									+ dy5 * ty1 * (             u[k][j-1][i][4]
													- fptwo * u[k][j][i][4]
													+           u[k][j+1][i][4] );

			}
			
			/**
			 * Fourth-order dissipation.
			 */
			if (west == -1) {
			
				for (m = 0; m < 5; m++) {
				
					rsd[k][3][i][m] = rsd[k][3][i][m]
										 - dssp * ( + fpfive * u[k][3][i][m]
										 			- fpfour * u[k][4][i][m]
										 			+           u[k][5][i][m] );
					rsd[k][4][i][m] = rsd[k][4][i][m]
										 - dssp * ( - fpfour * u[k][3][i][m]
													+ fpsix * u[k][4][i][m]
													- fpfour * u[k][5][i][m]
													+           u[k][6][i][m] );
				
				}
			
			}

			jst1 = 2;
			jend1 = ny + 1;
			if (west == -1) { jst1 = 5; }
			if (east == -1) { jend1 = ny - 2; }
			
			for (j = jst1; j <= jend1; j++) {
				for (m = 0; m < 5; m++) {
				
					rsd[k][j][i][m] = rsd[k][j][i][m]
										- dssp * (              u[k][j-2][i][m]
													- fpfour * u[k][j-1][i][m]
													+ fpsix * u[k][j][i][m]
													- fpfour * u[k][j+1][i][m]
													+           u[k][j+2][i][m] );
				
				}
			}

			if (east == -1) {
			
				for (m = 0; m < 5; m++) {
				
					rsd[k][ny-1][i][m] = rsd[k][ny-1][i][m]
											- dssp * (              u[k][ny-3][i][m]
														- fpfour * u[k][ny-2][i][m]
														+ fpsix * u[k][ny-1][i][m]
														- fpfour * u[k][ny][i][m] );
					rsd[k][ny][i][m] = rsd[k][ny][i][m]
											- dssp * (              u[k][ny-2][i][m]
														- fpfour * u[k][ny-1][i][m]
														+ fpfive * u[k][ny][i][m] );
				
				}
			
			}
			
		}
		
	}
	
	/**
	 * zeta-direction flux differences.
	 */
	for (j = jst; j <= jend; j++) {
		for (i = ist; i <= iend; i++) {
			
			for (k = 0; k <= nz - 1; k++) {
			
				flux[k][j][i][0] = u[k][j][i][3];
				u41 = u[k][j][i][3] / u[k][j][i][0];
				
				q = fphalf * (   u[k][j][i][1] * u[k][j][i][1]
								 + u[k][j][i][2] * u[k][j][i][2]
								 + u[k][j][i][3] * u[k][j][i][3] )
								 / u[k][j][i][0];
				
				flux[k][j][i][1] = u[k][j][i][1] * u41;
				flux[k][j][i][2] = u[k][j][i][2] * u41;
				flux[k][j][i][3] = u[k][j][i][3] * u41 + c2 * ( u[k][j][i][4] - q );
				flux[k][j][i][4] = ( c1 * u[k][j][i][4] - c2 * q ) * u41;
			
			}

			for (k = 1; k <= nz - 2; k++) {
				for (m = 0; m < 5; m++) {
			
					rsd[k][j][i][m] = rsd[k][j][i][m]
										- tz2 * ( flux[k+1][j][i][m] - flux[k-1][j][i][m] );
				
				}
			}

			for (k = 1; k <= nz - 1; k++) {
			
				tmp = fpone / u[k][j][i][0];
				
				u21k = tmp * u[k][j][i][1];
				u31k = tmp * u[k][j][i][2];
				u41k = tmp * u[k][j][i][3];
				u51k = tmp * u[k][j][i][4];
				
				tmp = fpone / u[k-1][j][i][0];
				
				u21km1 = tmp * u[k-1][j][i][1];
				u31km1 = tmp * u[k-1][j][i][2];
				u41km1 = tmp * u[k-1][j][i][3];
				u51km1 = tmp * u[k-1][j][i][4];
				
				flux[k][j][i][1] = tz3 * ( u21k - u21km1 );
				flux[k][j][i][2] = tz3 * ( u31k - u31km1 );
				flux[k][j][i][3] = (fpfour/fpthree) * tz3 * ( u41k - u41km1 );
				flux[k][j][i][4] = fphalf * ( fpone - c1 * c5 )
									* tz3 * ( ( u21k * u21k + u31k * u31k + u41k * u41k )
											- ( u21km1 * u21km1 + u31km1 * u31km1 + u41km1 * u41km1 ) )
									+ (fpone/fpsix)
									* tz3 * ( u41k * u41k - u41km1 * u41km1 )
									+ c1 * c5 * tz3 * ( u51k - u51km1 );

			}
			
			for (k = 1; k <= nz - 2; k++) {
			
				rsd[k][j][i][0] = rsd[k][j][i][0]
									+ dz1 * tz1 * (             u[k-1][j][i][0]
													- fptwo * u[k][j][i][0]
													+           u[k+1][j][i][0] );
				rsd[k][j][i][1] = rsd[k][j][i][1]
									+ tz3 * c3 * c4 * ( flux[k+1][j][i][1] - flux[k][j][i][1] )
									+ dz2 * tz1 * (             u[k-1][j][i][1]
													- fptwo * u[k][j][i][1]
													+           u[k+1][j][i][1] );
				rsd[k][j][i][2] = rsd[k][j][i][2]
									+ tz3 * c3 * c4 * ( flux[k+1][j][i][2] - flux[k][j][i][2] )
									+ dz3 * tz1 * (             u[k-1][j][i][2]
													- fptwo * u[k][j][i][2]
													+           u[k+1][j][i][2] );
													
				rsd[k][j][i][3] = rsd[k][j][i][3]
									+ tz3 * c3 * c4 * ( flux[k+1][j][i][3] - flux[k][j][i][3] )
									+ dz4 * tz1 * (             u[k-1][j][i][3]
													- fptwo * u[k][j][i][3]
													+           u[k+1][j][i][3] );
				rsd[k][j][i][4] = rsd[k][j][i][4]
									+ tz3 * c3 * c4 * ( flux[k+1][j][i][4] - flux[k][j][i][4] )
									+ dz5 * tz1 * (             u[k-1][j][i][4]
													- fptwo * u[k][j][i][4]
													+           u[k+1][j][i][4] );

			}
			
			/**
			 * Fourth-order dissipation.
			 */
			for (m = 0; m < 5; m++) {
			
				rsd[1][j][i][m] = rsd[1][j][i][m]
									 - dssp * ( + fpfive * u[1][j][i][m]
												- fpfour * u[2][j][i][m]
												+           u[3][j][i][m] );
				rsd[2][j][i][m] = rsd[2][j][i][m]
									 - dssp * ( - fpfour * u[1][j][i][m]
									 			+ fpsix * u[2][j][i][m]
									 			- fpfour * u[3][j][i][m]
									 			+           u[4][j][i][m] );
				
			}

			for (k = 3; k <= nz - 4; k++) {
				for (m = 0; m < 5; m++) {
			
					rsd[k][j][i][m] = rsd[k][j][i][m]
										 - dssp * (             u[k-2][j][i][m]
										 			- fpfour * u[k-1][j][i][m]
										 			+ fpsix * u[k][j][i][m]
										 			- fpfour * u[k+1][j][i][m]
										 			+           u[k+2][j][i][m] );
				
				}
			}
			
			for (m = 0; m < 5; m++) {
			
				rsd[nz-3][j][i][m] = rsd[nz-3][j][i][m]
									 - dssp * (             u[nz-5][j][i][m]
									 			- fpfour * u[nz-4][j][i][m]
									 			+ fpsix * u[nz-3][j][i][m]
									 			- fpfour * u[nz-2][j][i][m] );
									 			
				rsd[nz-2][j][i][m] = rsd[nz-2][j][i][m]
									 - dssp * (             u[nz-4][j][i][m]
									 			- fpfour * u[nz-3][j][i][m]
									 			+ fpfive * u[nz-2][j][i][m] );
			
			}
			
		}
		
	}	
	
}
