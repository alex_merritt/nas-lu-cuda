// C port of NPB3.2
// subroutine pintgr
#include "applu.h"
#include "mpinpb.h"
#include <stdio.h>
#include <stdlib.h>

/** 
 * Set up the sub-domains for integration in each processor.
 */
void pintgr() {

	/**
	 * Local variables.
	 */
	int i, j, k;
	int ibeg, ifin, ifin1;
	int jbeg, jfin, jfin1;
	int iglob, iglob1, iglob2;
	int jglob, jglob1, jglob2;
	int ind1, ind2;
	fp_type **phi1, **phi2;
	fp_type frc1, frc2, frc3;
	fp_type dummy;
	
	// Constants.
	const fp_type c2 = c2_def;
	
	const fp_type fpzero = 0.0e+00;
	const fp_type fpquarter = 0.25e+00;
	const fp_type fphalf = 0.50e+00;
	
	// Allocate for phi1 and phi2.
	phi1 = malloc( (isiz3 + 2) * sizeof(fp_type*) );
	phi2 = malloc( (isiz3 + 2) * sizeof(fp_type*) );	
	
	// Allocate arrays.
	for (k = 0; k < (isiz3 + 2); k++) {
		phi1[k] = malloc( (isiz2 + 2) * sizeof(fp_type) );
		phi2[k] = malloc( (isiz2 + 2) * sizeof(fp_type) );		
	}	

	ibeg = nx + 1;
	ifin = 0;
	iglob1 = ipt + 1;
	iglob2 = ipt + nx;
	if (iglob1 >= ii1 && iglob2 < ii2 + nx) { ibeg = 1; }
	if (iglob1 > ii1 - nx && iglob2 <= ii2) { ifin = nx; }
	if (ii1 >= iglob1 && ii1 <= iglob2) { ibeg = ii1 - ipt; }
	if (ii2 >= iglob1 && ii2 <= iglob2) { ifin = ii2 - ipt; }
	jbeg = ny + 1;
	jfin = 0;
	jglob1 = jpt + 1;
	jglob2 = jpt + ny;
	if (jglob1 >= ji1 && jglob2 < ji2 + ny) { jbeg = 1; }
	if (jglob1 > ji1 - ny && jglob2 <= ji2) { jfin = ny; }
	if (ji1 >= jglob1 && ji1 <= jglob2) { jbeg = ji1 - jpt; }
	if (ji2 >= jglob1 && ji2 <= jglob2) { jfin = ji2 - jpt; }
	ifin1 = ifin;
	jfin1 = jfin;
	if (ipt + ifin1 == ii2) { ifin1 = ifin - 1; }
	if (jpt + jfin1 == ji2) { jfin1 = jfin - 1; }

	// Initialize.
	for (i = 0; i <= isiz2 + 1; i++) {
		for (k = 0; k <= isiz3 + 1; k++) {
			phi1[k][i] = 0;
			phi2[k][i] = 0;
		}
	}
	
	for (j = jbeg + 1; j <= jfin + 1; j++) {
	
		jglob = jpt + j;
		
		for (i = ibeg + 1; i <= ifin + 1; i++) {
		
			iglob = ipt + i;
			k = ki1 - 1;
			
			phi1[j-1][i-1] = c2 * ( u[k][j][i][4]
								- fphalf * ( u[k][j][i][1] * u[k][j][i][1]
											+  u[k][j][i][2] * u[k][j][i][2]
											+  u[k][j][i][3] * u[k][j][i][3] )
											/ u[k][j][i][0] );
			k = ki2 - 1;
			
			phi2[j-1][i-1] = c2 * ( u[k][j][i][4]
								- fphalf * ( u[k][j][i][1] * u[k][j][i][1]
											+  u[k][j][i][2] * u[k][j][i][2]
											+  u[k][j][i][3] * u[k][j][i][3] )
											/ u[k][j][i][0] );
											
		}
		
	}
	
	// Communicate in i and j directions.
	exchange_4(phi1, phi2, ibeg, ifin1, jbeg, jfin1);
	
	frc1 = fpzero;

	for (j = jbeg; j <= jfin1; j++) {
		for (i = ibeg; i <= ifin1; i++) {
		
			frc1 = frc1 + (   phi1[j][i]
							+ phi1[j][i+1]
							+ phi1[j+1][i]
							+ phi1[j+1][i+1]
							+ phi2[j][i]
							+ phi2[j][i+1]
							+ phi2[j+1][i]
							+ phi2[j+1][i+1] );

		}
	}
	
	// Compute the global sum of individual contributions to frc1.
	dummy = frc1;
	MPI_Allreduce(&dummy, &frc1, 1, MPI_FP_TYPE, MPI_SUM, MPI_COMM_WORLD);
	frc1 = dxi * deta * frc1;

	// Initialize.
	for (i = 0; i <= isiz2 + 1; i++) {
		for (k = 0; k <= isiz3 + 1; k++) {
			phi1[k][i] = 0;
			phi2[k][i] = 0;
		}
	}
	
	jglob = jpt + jbeg;
	ind1 = 0;
	
	if (jglob == ji1) {
		
		ind1 = 1;
		for (k = ki1 - 1; k <= ki2 - 1; k++) {
			for (i = ibeg + 1; i <= ifin + 1; i++) {
				
				iglob = ipt + i;
				phi1[k+1][i-1] = c2 * ( u[k][jbeg+1][i][4]
							- fphalf * ( u[k][jbeg+1][i][1] * u[k][jbeg+1][i][1]
										+  u[k][jbeg+1][i][2] * u[k][jbeg+1][i][2]
										+  u[k][jbeg+1][i][3] * u[k][jbeg+1][i][3] )
										/ u[k][jbeg+1][i][0] );
				
			}
		}
		
	}
	
	jglob = jpt + jfin;
	ind2 = 0;
	if (jglob == ji2) {
	
		ind2 = 1;
		for (k = ki1 - 1; k <= ki2 - 1; k++) {
			for (i = ibeg + 1; i <= ifin + 1; i++) {
			
				iglob = ipt + i;
				phi2[k+1][i-1] = c2 * ( u[k][jfin+1][i][4]
							- fphalf * ( u[k][jfin+1][i][1] * u[k][jfin+1][i][1]
										+  u[k][jfin+1][i][2] * u[k][jfin+1][i][2]
										+  u[k][jfin+1][i][3] * u[k][jfin+1][i][3] )
										/ u[k][jfin+1][i][0] );
				
			
			}
		}
	
	}

	// Communicate in i direction.
	if (ind1 == 1) { 
		exchange_5(phi1, ibeg, ifin1);
	}
	if (ind2 == 1) {
		exchange_5(phi2, ibeg, ifin1);
	}

	frc2 = fpzero;
	for (k = ki1; k <= ki2 - 1; k++) {
		for (i = ibeg; i <= ifin1; i++) {
			
			frc2 = frc2 + (   phi1[k][i]
							+ phi1[k][i+1]
							+ phi1[k+1][i]
							+ phi1[k+1][i+1]
							+ phi2[k][i]
							+ phi2[k][i+1]
							+ phi2[k+1][i]
							+ phi2[k+1][i+1] );
			
		}
	}

	// Compute the global sum of individual contributions to frc2.
	dummy = frc2;
	MPI_Allreduce(&dummy, &frc2, 1, MPI_FP_TYPE, MPI_SUM, MPI_COMM_WORLD);
	frc2 = dxi * dzeta * frc2;

	// Initialize.
	for (i = 0; i <= isiz2 + 1; i++) {
		for (k = 0; k <= isiz3 + 1; k++) {
		
			phi1[k][i] = 0;
			phi2[k][i] = 0;
			
		}
	}
	
	iglob = ipt + ibeg;
	ind1 = 0;
	if (iglob == ii1) { 
		
		ind1 = 1;
		for (k = ki1 - 1; k <= ki2 - 1; k++) {
			for (j = jbeg + 1; j <= jfin + 1; j++) {
			
				jglob = jpt + j;
				phi1[k+1][j-1] = c2 * ( u[k][j][ibeg+1][4]
							- fphalf * ( u[k][j][ibeg+1][1] * u[k][j][ibeg+1][1]
										+  u[k][j][ibeg+1][2] * u[k][j][ibeg+1][2]
										+  u[k][j][ibeg+1][3] * u[k][j][ibeg+1][3] )
										/ u[k][j][ibeg+1][0] );
			
			}
		}
	
	}

	iglob = ipt + ifin;
	ind2 = 0;
	if (iglob == ii2) {
	
		ind2 = 1;
		for (k = ki1 - 1; k <= ki2 - 1; k++) {
			for (j = jbeg + 1; j <= jfin + 1; j++) {
			
				jglob = jpt + j;
				phi2[k+1][j-1] = c2 * ( u[k][j][ifin+1][4]
							- fphalf * ( u[k][j][ifin+1][1] * u[k][j][ifin+1][1]
										+  u[k][j][ifin+1][2] * u[k][j][ifin+1][2]
										+  u[k][j][ifin+1][3] * u[k][j][ifin+1][3] )
										/ u[k][j][ifin+1][0] );				
			
			}
		}
		
	}
	
	// Communicate in j direction.
	if (ind1 == 1) {
		exchange_6(phi1, jbeg, jfin1);
	}
	if (ind2 == 1) {
		exchange_6(phi2, jbeg, jfin1);
	}
	
	frc3 = fpzero;
	for (k = ki1; k <= ki2 - 1; k++) {
		for (j = jbeg; j <= jfin1; j++) {
			
			frc3 = frc3 + (   phi1[k][j]
							+ phi1[k][j+1]
							+ phi1[k+1][j]
							+ phi1[k+1][j+1]
							+ phi2[k][j]
							+ phi2[k][j+1]
							+ phi2[k+1][j]
							+ phi2[k+1][j+1] );
			
		}
	}	

	// Compute the global sum of individual contributions to frc3.
	dummy = frc3;
	MPI_Allreduce(&dummy, &frc3, 1, MPI_FP_TYPE, MPI_SUM, MPI_COMM_WORLD);
	frc3 = deta * dzeta * frc3;
	frc = fpquarter * (frc1 + frc2 + frc3);
	
	if (id == 0) {
		printf(" Surface integral = %e.\n", frc);
	}
	
	// Free arrays.
	for (k = 0; k < (isiz3 + 2); k++) {
		free(phi1[k]);
		free(phi2[k]);
	}		
	
	// Free phi1 and 2.
	free(phi1);
	free(phi2);	

}
