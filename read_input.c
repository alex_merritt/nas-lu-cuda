#include "mpinpb.h"
#include "applu.h"
#include <stdio.h>

void read_input(){

	int fstatus, nnodes;
	FILE *fp;
	char line[1024];
	
/**
 *    only root reads the input file
 *    if input file does not exist, it uses defaults
 *       ipr = 1 for detailed progress output
 *       inorm = how often the norm is printed (once every inorm iterations)
 *       itmax = number of pseudo time steps
 *       dt = time step
 *       omega 1 over-relaxation factor for SSOR
 *       tolrsd = steady state residual tolerance levels
 *       nx, ny, nz = number of grid points in x, y, z directions
*/
	root = 0;

	if(id == root){
	
		fp = fopen("inputlu.data2", "r");

		if(fp != NULL){
		
			printf(" Reading from input file inputlu.data\n");
			
			#ifdef SINGLE_PRECISION_LU
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%d\t%d\n", &ipr, &inorm);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%d\n", &itmax);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%f\n", &dt);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%f\n", &omega);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%f\t%f\t%f\t%f\t%f\n", &tolrsd[0], &tolrsd[1], &tolrsd[2], &tolrsd[3], &tolrsd[4]);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%d\t%d\t%d\n", &nx0, &ny0, &nz0);
			#endif			
			
			#ifdef DOUBLE_PRECISION_LU
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%d\t%d\n", &ipr, &inorm);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%d\n", &itmax);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%lf\n", &dt);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%lf\n", &omega);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%lf\t%lf\t%lf\t%lf\t%lf\n", &tolrsd[0], &tolrsd[1], &tolrsd[2], &tolrsd[3], &tolrsd[4]);
			fgets(line, 1000, fp);
			fgets(line, 1000, fp);
			fscanf(fp, "%d\t%d\t%d\n", &nx0, &ny0, &nz0);
			#endif
			
			fclose(fp);

		} else {
			printf(" Using defaults.\n");
			ipr = ipr_default;
			inorm = inorm_default;
			itmax = itmax_default;
			dt = dt_default;
			omega = omega_default;
			tolrsd[0] = tolrsd1_def;
			tolrsd[1] = tolrsd2_def;
			tolrsd[2] = tolrsd3_def;
			tolrsd[3] = tolrsd4_def;
			tolrsd[4] = tolrsd5_def;
			nx0 = isiz01;
			ny0 = isiz02;
			nz0 = isiz03;
		}
		

		// Check problem size.
		MPI_Comm_size(MPI_COMM_WORLD, &nnodes);
		if(nnodes != nnodes_compiled) {
			printf(" Warning: program is running on %d processors but was compiled for %d\n", nnodes, nnodes_compiled);
		}         
 
		if(nx0 < 4 || ny0 < 4 || nz0 < 4){
			printf(" PROBLEM SIZE IS TOO SMALL - SET EACH OF NX, NY AND NZ AT LEAST EQUAL TO 5\n");
			MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
		}

		if(nx0 > isiz01 || ny0 > isiz02 || nz0 > isiz03) {
			printf(" PROBLEM SIZE IS TOO LARGE - NX, NY AND NZ SHOULD BE LESS THAN OR EQUAL TO ISIZ01, ISIZ02 AND ISIZ03 RESPECTIVELY\n");
			MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
		}

		printf(" NAS Parallel Benchmarks 3.2 -- LU Benchmark\n");
		printf(" University of Warwick CUDA Port\n");
		printf(" Size: %d %d %d\n", nx0, ny0, nz0);
		printf(" Iterations: %d\n", itmax);
		printf(" Number of processors: %d\n", nnodes);
		
	}

		bcast_inputs();

}

