// C port of NPB3.2
// subroutine bcast_inputs
#include "applu.h"
#include "mpinpb.h"

void bcast_inputs() {

	root = 0;

	/** 
	 * Root broadcasts the data.
	 * The data isn't contiguous or of the same type, so it's not
	 * clear how to send it in the "MPI" way.
	 * We could pack the info into a buffer or we could create
	 * an obscene datatype to handle it all at once. Since we only
	 * broadcast the data once, just use a separate broadcast for
	 * each piece.
	 */
	MPI_Bcast(&ipr, 1, MPI_INT, root, MPI_COMM_WORLD);
	MPI_Bcast(&inorm, 1, MPI_INT, root, MPI_COMM_WORLD);
	MPI_Bcast(&itmax, 1, MPI_INT, root, MPI_COMM_WORLD);
	MPI_Bcast(&dt, 1, MPI_FP_TYPE, root, MPI_COMM_WORLD);
	MPI_Bcast(&omega, 1, MPI_FP_TYPE, root, MPI_COMM_WORLD);
	MPI_Bcast(&tolrsd, 5, MPI_FP_TYPE, root, MPI_COMM_WORLD);
	MPI_Bcast(&nx0, 1, MPI_INT, root, MPI_COMM_WORLD);
	MPI_Bcast(&ny0, 1, MPI_INT, root, MPI_COMM_WORLD);
	MPI_Bcast(&nz0, 1, MPI_INT, root, MPI_COMM_WORLD);

}
