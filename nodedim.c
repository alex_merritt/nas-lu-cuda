#include "applu.h"
#include <math.h>

//      integer function nodedim(num)

int nodedim(int num){

/**
 *  compute the exponent where num = 2**nodedim
 *  NOTE: assumes a power-of-two number of nodes
*/

//      implicit none

/**
 *  input parameters
*/

// integer num

/**
 *  local variables
*/

//fp_type precision fnum
	fp_type fnum;
	

//fnum = dble(num)
	fnum = (fp_type) num;
	
//nodedim = log(fnum)/log(2.0d+0) + 0.00001
	return log(fnum)/log((fp_type)2.0) + 0.00001;

//      return
//      end
}

