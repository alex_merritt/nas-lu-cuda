//subroutine exchange_5(g,ibeg,ifin1)

/**
 *   compute the right hand side based on exact solution
*/

/**
      implicit none

      include 'mpinpb.h'
      include 'applu.incl'
*/

#include "mpinpb.h"
#include "applu.h"

void exchange_5(fp_type** g, int ibeg, int ifin1){


/**
 *  input parameters
*/

/*
      fp_type precision  g(0:isiz2+1,0:isiz3+1)
      integer ibeg, ifin1
*/


/**
 *  local variables
*/

/**
      integer k
      fp_type precision  dum(1024)

      integer msgid1
      integer status(MPI_status_SIZE)
      integer IERROR
*/

	int k;
	fp_type dum[1024];
	MPI_Request msgid1;
	MPI_Status status;


/**
 *   communicate in the south and north directions
*/


/**
 *   receive from south
*/

/**
      if (ifin1.eq.nx) then
        call MPI_IRECV( dum,
     >                  nz,
     >                  dp_type,
     >                  MPI_ANY_SOURCE,
     >                  from_s,
     >                  MPI_COMM_WORLD,
     >                  msgid1,
     >                  IERROR )

        call MPI_WAIT( msgid1, status, IERROR )

        do k = 1,nz
          g(nx+1,k) = dum(k)
        end do

      end if
      
*/

	if( ifin1 == nx){
		MPI_Irecv(dum, nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_s, MPI_COMM_WORLD, &msgid1);
		MPI_Wait(&msgid1, &status);
		
		for(k = 1; k <= nz; k++){
			g[k][nx+1] =dum[k-1];
		}
		
	}       

/**
 *   send north
*/

/**
      if (ibeg.eq.1) then
        do k = 1,nz
          dum(k) = g(1,k)
        end do

        call MPI_SEND( dum,
     >                 nz,
     >                 dp_type,
     >                 north,
     >                 from_s,
     >                 MPI_COMM_WORLD,
     >                 IERROR )

      end if
*/

	if(ibeg == 1){
		for(k = 1; k <= nz; k++){
			dum[k-1] = g[k][1];
		}
		
		MPI_Send( dum, nz, MPI_FP_TYPE, north, from_s, MPI_COMM_WORLD); 
	}

//      return
//      end    
} 
