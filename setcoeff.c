// C port of NPB3.2
// subroutine setcoeff
#include "applu.h"

// Define max function.
#define max2(x,y)	( (x > y) ? x : y )
#define max3(x,y,z)	( max2( max2(x, y), z ) )

/**
 * Setup coefficients.
 */
void setcoeff() {

	dxi = 1.0e+00 / ( nx0 - 1 );
	deta = 1.0e+00 / ( ny0 - 1 );
	dzeta = 1.0e+00 / ( nz0 - 1 );

	tx1 = 1.0e+00 / ( dxi * dxi );
	tx2 = 1.0e+00 / ( 2.0e+00 * dxi );
	tx3 = 1.0e+00 / dxi;

	ty1 = 1.0e+00 / ( deta * deta );
	ty2 = 1.0e+00 / ( 2.0e+00 * deta );
	ty3 = 1.0e+00 / deta;

	tz1 = 1.0e+00 / ( dzeta * dzeta );
	tz2 = 1.0e+00 / ( 2.0e+00 * dzeta );
	tz3 = 1.0e+00 / dzeta;

	ii1 = 2;
	ii2 = nx0 - 1;
	ji1 = 2;
	ji2 = ny0 - 2;
	ki1 = 3;
	ki2 = nz0 - 1;
	
	
	// Diffusion coefficients.
	dx1 = 0.75e+00;
	dx2 = dx1;
	dx3 = dx1;
	dx4 = dx1;
	dx5 = dx1;

	dy1 = 0.75e+00;
	dy2 = dy1;
	dy3 = dy1;
	dy4 = dy1;
	dy5 = dy1;

	dz1 = 1.00e+00;
	dz2 = dz1;
	dz3 = dz1;
	dz4 = dz1;
	dz5 = dz1;
	
	// Fourth difference dissipation.
	dssp = ( max3 (dx1, dy1, dz1 ) ) / 4.0e+00;
	//dssp = 1.00e+00 / 4.0e+00;	// We know that dz1 is the maximum.
	
	// Coefficients of the exact solution to the first pde.
	ce[0][0] = 2.0e+00;
	ce[1][0] = 0.0e+00;
	ce[2][0] = 0.0e+00;
	ce[3][0] = 4.0e+00;
	ce[4][0] = 5.0e+00;
	ce[5][0] = 3.0e+00;
	ce[6][0] = 5.0e-01;
	ce[7][0] = 2.0e-02;
	ce[8][0] = 1.0e-02;
	ce[9][0] = 3.0e-02;
	ce[10][0] = 5.0e-01;
	ce[11][0] = 4.0e-01;
	ce[12][0] = 3.0e-01;	

	// Coefficients of the exact solution to the second pde.
	ce[0][1] = 1.0e+00;
	ce[1][1] = 0.0e+00;
	ce[2][1] = 0.0e+00;
	ce[3][1] = 0.0e+00;
	ce[4][1] = 1.0e+00;
	ce[5][1] = 2.0e+00;
	ce[6][1] = 3.0e+00;
	ce[7][1] = 1.0e-02;
	ce[8][1] = 3.0e-02;
	ce[9][1] = 2.0e-02;
	ce[10][1] = 4.0e-01;
	ce[11][1] = 3.0e-01;
	ce[12][1] = 5.0e-01;
	
	// Coefficients of the exact solution to the third pde.
	ce[0][2] = 2.0e+00;
	ce[1][2] = 2.0e+00;
	ce[2][2] = 0.0e+00;
	ce[3][2] = 0.0e+00;
	ce[4][2] = 0.0e+00;
	ce[5][2] = 2.0e+00;
	ce[6][2] = 3.0e+00;
	ce[7][2] = 4.0e-02;
	ce[8][2] = 3.0e-02;
	ce[9][2] = 5.0e-02;
	ce[10][2] = 3.0e-01;
	ce[11][2] = 5.0e-01;
	ce[12][2] = 4.0e-01;
	
	// Coefficients of the exact solution to the fourth pde.
	ce[0][3] = 2.0e+00;
	ce[1][3] = 2.0e+00;
	ce[2][3] = 0.0e+00;
	ce[3][3] = 0.0e+00;
	ce[4][3] = 0.0e+00;
	ce[5][3] = 2.0e+00;
	ce[6][3] = 3.0e+00;
	ce[7][3] = 3.0e-02;
	ce[8][3] = 5.0e-02;
	ce[9][3] = 4.0e-02;
	ce[10][3] = 2.0e-01;
	ce[11][3] = 1.0e-01;
	ce[12][3] = 3.0e-01;
	
	// Coefficients of the exact solution to the fifth pde.
	ce[0][4] = 5.0e+00;
	ce[1][4] = 4.0e+00;
	ce[2][4] = 3.0e+00;
	ce[3][4] = 2.0e+00;
	ce[4][4] = 1.0e-01;
	ce[5][4] = 4.0e-01;
	ce[6][4] = 3.0e-01;
	ce[7][4] = 5.0e-02;
	ce[8][4] = 4.0e-02;
	ce[9][4] = 3.0e-02;
	ce[10][4] = 1.0e-01;
	ce[11][4] = 3.0e-01;
	ce[12][4] = 2.0e-01;	
	
}
