# Default to single processor.
NPROCS ?= 1

# Default to class S.
CLASS ?= S

# These are available in case of kernel changes;
# current kernels assume particular block sizes.
GPUBLOCKY = 1
GPUBLOCKZ = 1
GPUGRIDY = 1
RHSBLOCKZ = 1

# Choose other default values based on class.
ifeq ($(CLASS), S)
	KBLOCK ?= 1
	GPUBLOCKX ?= 1
	GPUGRIDX ?= 1
	GPUGRIDY ?= 1
	RHSBLOCKX ?= 4
	RHSBLOCKY ?= 4
	RHSGRIDX ?= 4
	RHSGRIDY ?= 4
else ifeq ($(CLASS), A)
	KBLOCK ?= 64
        GPUBLOCKX ?= 64
        GPUGRIDX ?= 73
        RHSBLOCKX ?= 8
        RHSBLOCKY ?= 8
        RHSGRIDX ?= 9
        RHSGRIDY ?= 9
else ifeq ($(CLASS), B)
	KBLOCK ?= 102
        GPUBLOCKX ?= 64
        GPUGRIDX ?= 176
        RHSBLOCKX ?= 8
        RHSBLOCKY ?= 8
        RHSGRIDX ?= 14
        RHSGRIDY ?= 14
else ifeq ($(CLASS), C)
	KBLOCK ?= 162
        GPUBLOCKX ?= 64
        GPUGRIDX ?= 431
        RHSBLOCKX ?= 8
        RHSBLOCKY ?= 8
        RHSGRIDX ?= 21
        RHSGRIDY ?= 21
endif

DEBUG ?= 0

ARCH ?= sm_20

# Default to double precision.
PRECISION ?= 2
ifeq ($(PRECISION),1)
	PREC = SP
	PRE_PROC += -D SINGLE_PRECISION_LU
	PRECISION_FLAGS = #-fsingle-precision-constant
else ifeq ($(PRECISION),2)
	PREC = DP
	PRE_PROC += -D DOUBLE_PRECISION_LU
endif

# Define compilers.
CC = mpicc
NVCC = nvcc

# Reference to MPICC compiler.
CCPATH=`which mpicc`

# Compiler flags.
CFLAGS = -O2 -funroll-loops -msse3 -I ./headers $(PRE_PROC) -Wall
NVCCFLAGS = -ccbin=$(CCPATH) --compiler-options "$(CFLAGS)" -O2 -I $(NVIDIA_CUDA_SDK)/common/inc -I . -I ./headers -I ./cuda/ -I ./cuda/kernels -Xptxas -v -arch="$(ARCH)" $(PRE_PROC) 
LDFLAGS = -lcuda -lcudart -lcutil_x86_64

# Paths to library files.
CUDA_DIR = /opt/cuda/toolkit/4.0.11/cuda/lib64/
SDK_DIR = /opt/cuda/sdk/4.0.11/C/lib/

# Check for DEBUG
ifeq ($(DEBUG),1)
	CFLAGS += -g
	LDFLAGS += -g
	NVCCFLAGS += -g -G
endif

# Define executable name.
EXE ?= applu.$(PREC).$(CLASS).$(NPROCS).$(KBLOCK)

OBJS =  main.o \
	bcast_inputs.o \
	blts.o \
	buts.o \
	erhs.o \
	error.o \
	exact.o \
	exchange_1.o \
	exchange_3.o \
	exchange_4.o \
	exchange_5.o \
	exchange_6.o \
	init_comm.o \
	jacld.o \
	jacu.o \
	l2norm.o \
	neighbors.o \
	nodedim.o \
	pintgr.o \
	proc_grid.o \
	read_input.o \
	rhs.o \
	setbv.o \
	setcoeff.o \
	sethyper.o \
	setiv.o \
	ssor.o \
	ssor_cuda.o \
	subdomain.o \
	verify.o \
	alloc.o \
	util.o \
	timers.o

all: clean setparams applu

applu: $(OBJS)
	#$(CC) $(CFLAGS) $(PRECISION_FLAGS) -L /opt/cuda/toolkit/4.0.11/cuda/lib64/ -L /opt/cuda/sdk/4.0.11/C/lib -o ./bin/$(EXE) $(OBJS) -lm $(LDFLAGS)
	$(CC) $(CFLAGS) $(PRECISION_FLAGS) -L $(CUDA_DIR) -L $(SDK_DIR) -o ./bin/$(EXE) $(OBJS) -lm $(LDFLAGS)

ssor_cuda.o:
	$(NVCC) $(NVCCFLAGS) -c ./cuda/ssor_cuda.cu

.c.o: $*.c applu.h
	$(CC) $(CFLAGS) $(PRECISION_FLAGS) -c $*.c 

setparams:
	$(CC) $(CFLAGS) -o setparams setparams.c -lm
	./setparams $(NPROCS) $(CLASS) $(KBLOCK) $(GPUBLOCKX) $(GPUBLOCKY) $(GPUBLOCKZ) $(GPUGRIDX) $(GPUGRIDY) $(RHSBLOCKX) $(RHSBLOCKY) $(RHSBLOCKZ) $(RHSGRIDX) $(RHSGRIDY)
	cp size.h ./headers/size.h

clean:
	rm -f *.o core gmon.out setparams *~
	rm -f size.h ./headers/size.h
