#include <stdio.h>
#include <stdlib.h>

#include "applu.h"

/** 
 * Set up pointers for a 4D ursd array using the provided contiguous space.
 */
fp_type**** alloc_ursd(fp_type* space) {

	int k, j, i;
	fp_type **** pointers;
	
	// Now allocate all of our pointer space.
	pointers = malloc(isiz3 * sizeof(fp_type***));
	for (k = 0; k < isiz3; k++) {
	
		pointers[k] = malloc( (isiz2 + 4) * sizeof(fp_type**) );
	
		for (j = 0; j < (isiz2 + 4); j++) {
		
			pointers[k][j] = malloc( (isiz1 + 4) * sizeof(fp_type*) );
		
			for (i = 0; i < (isiz1 + 4); i++) {
			
				// Calculate i, j, k index.
				int index = (k * (isiz2 + 4) * (isiz1 + 4) * 5) + (j * (isiz1 + 4) * 5) + (i * 5);
			
				pointers[k][j][i] = space + index;

			}

		}

	}
	
	return pointers;

}

/**
 * Free a memory for a ursd array.
 */
void free_ursd(fp_type**** pointers) {

	int k, j, i;
	
	for (k = 0; k < isiz3; k++) {
	
		for (j = 0; j < (isiz2 + 4); j++) {
			
			free(pointers[k][j]);

		}
		
		free(pointers[k]);

	}
	free(pointers);
	
}

/** 
 * Set up pointers for a 5D abcd array using the provided contiguous space.
 */
fp_type***** alloc_abcd(fp_type* space) {

	int k, j, i, m;
	fp_type ***** pointers;
	
	// Now allocate all of our pointer space.
	pointers = malloc(kblock * sizeof(fp_type****));
	for (k = 0; k < kblock; k++) {
	
		pointers[k] = malloc( (isiz2 + 4) * sizeof(fp_type***) );
	
		for (j = 0; j < (isiz2 + 4); j++) {
		
			pointers[k][j] = malloc( (isiz1 + 4) * sizeof(fp_type**) );
		
			for (i = 0; i < (isiz1 + 4); i++) {
			
				pointers[k][j][i] = malloc ( 5 * sizeof(fp_type*) );
			
				for (m = 0; m < 5; m++) {
			
					// Calculate i, j, k, m index.
					int index = (k * (isiz2 + 4) * (isiz1 + 4) * 5 * 5) + (j * (isiz1 + 4) * 5 * 5) + (i * 5 * 5) + (m * 5);
			
					pointers[k][j][i][m] = space + index;
				
				}

			}

		}

	}
	
	return pointers;

}

void free_abcd(fp_type***** pointers) {

	int k, j, i, m;

	for (k = 0; k < kblock; k++) {
	
		for (j = 0; j < (isiz2 + 4); j++) {
		
			for (i = 0; i < (isiz1 + 4); i++) {
				
				free(pointers[k][j][i]);

			}
			
			free(pointers[k][j]);

		}
		
		free(pointers[k]);

	}
	
	free(pointers);

}

/**
 * Set up 2D pointers for our buffer.
 */
fp_type** alloc_buffer(fp_type* space) {

	int k;
	fp_type** pointers;
	
	// Now allocate all of our pointer space.
	pointers = malloc( (isiz3 * isiz2 * 2) * sizeof(fp_type*) );
	for (k = 0; k < (isiz3 * isiz2 * 2); k++) {
		
		// Calculate k, m, index.
		int index = k * 5;
		
		pointers[k] = space + index;
		
	}
	
	return pointers;	

}

void free_buffer(fp_type** pointers) {

	free(pointers);		
	
}

/**
 * Set up 2D pointers for our ibuf.
 */
fp_type*** alloc_ibuf(fp_type* space) {

	int i, k, m;
	fp_type*** pointers;
	
	// Now allocate all of our pointer space.
	pointers = malloc( kblock * sizeof(fp_type**) );
	for (k = 0; k < kblock; k++) {
	
		pointers[k] = malloc( (iend - ist + 1) * sizeof(fp_type*) );
		
		for (i = 0; i < (iend - ist + 1); i++) {
		
			int index = (k * (iend - ist + 1) * 5) + (i * 5);
			
			pointers[k][i] = space + index;
			
			
		}
		
	}
	
	return pointers;	

}

void free_ibuf(fp_type*** pointers) {

	int k;

	for (k = 0; k < kblock; k++) {
		
		free(pointers[k]);
		
	}
	
	free(pointers);
	
}

/**
 * Set up 2D pointers for our jbuf.
 */
fp_type*** alloc_jbuf(fp_type* space) {

	int j, k, m;
	fp_type*** pointers;
	
	// Now allocate all of our pointer space.
	pointers = malloc( kblock * sizeof(fp_type**) );
	for (k = 0; k < kblock; k++) {
	
		pointers[k] = malloc( (jend - jst + 1) * sizeof(fp_type*) );
		
		for (j = 0; j < (jend - jst + 1); j++) {
		
			int index = (k * (jend - jst + 1) * 5) + (j * 5);
			
			pointers[k][j] = space + index;
			
			
		}
		
	}
	
	return pointers;	

}

void free_jbuf(fp_type*** pointers) {

	int k;

	for (k = 0; k < kblock; k++) {
		
		free(pointers[k]);
		
	}
	
	free(pointers);
	
}
