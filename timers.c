#include "timers.h"

#include <sys/time.h>
#include <sys/times.h>
#include <sys/resource.h>

#include <stdio.h>
#include <stdlib.h>

// Array of timers.
Timer* timers;
int no_timers;

/**
 * Return the current cpu and wall time.
 */
void timer(double* cpu, double* et) {

	struct rusage r;
	struct timeval t;
	
	getrusage( RUSAGE_SELF, &r );
	*cpu = r.ru_utime.tv_sec + r.ru_utime.tv_usec*1.0e-6;
	
	gettimeofday( &t, (struct timezone *)0 );
	*et = t.tv_sec + t.tv_usec*1.0e-6;
	
}

/**
 * Create the timers.
 */
void timer_init(int number) {

	// Allocate an array of timers.
	timers = (Timer*) malloc(number * sizeof(Timer));
	no_timers = number;
	
	// Initialise them all to 0.
	int i;
	for (i = 0; i < no_timers; i++) {
		timer_reset(i);
	}

}

/**
 * Reset all timers.
 */
void timer_reset() {
	
	int i;
	for (i = 0; i < no_timers; i++) {
	
		timers[i].cpu_start = 0;
		timers[i].cpu_end = 0;
	
		timers[i].wall_start = 0;
		timers[i].wall_end = 0;
	
		timers[i].cpu_total = 0;
		timers[i].wall_total = 0;
	
		timers[i].calls = 0;
	
	}
	
}

/**
 * Start a timer.
 */
void timer_start(int i) {

	timer(&timers[i].cpu_start, &timers[i].wall_start);

}

/**
 * Stop a timer.
 */
void timer_stop(int i) {
	
	// Update the timer.
	timer(&timers[i].cpu_end, &timers[i].wall_end);
	timers[i].cpu_total = timers[i].cpu_total + (timers[i].cpu_end - timers[i].cpu_start);
	timers[i].wall_total = timers[i].wall_total + (timers[i].wall_end - timers[i].wall_start);
	
	timers[i].calls = timers[i].calls + 1;
	
}

/**
 * Pause a timer.
 */
void timer_pause(int i) {

	timer(&timers[i].cpu_end, &timers[i].wall_end);
	timers[i].cpu_total = timers[i].cpu_total + (timers[i].cpu_end - timers[i].cpu_start);
	timers[i].wall_total = timers[i].wall_total + (timers[i].wall_end - timers[i].wall_start);

}

/**
 * Restart a timer.
 */
void timer_restart(int i) {
	
	timer(&timers[i].cpu_start, &timers[i].wall_start);
	
}

/**
 * Clean up.
 */
void timer_finalize() {
	
	free(timers);	
	
}

/**
 * Return the CPU timer total.
 */
double timer_cpu_total(int i) {

	return timers[i].cpu_total;
	
}

/**
 * Return the CPU timer average.
 */
double timer_cpu_avg(int i) {

	if (timers[i].calls == 0) {
		return 0;
	}

	return timers[i].cpu_total / timers[i].calls;

}

/**
 * Return the Wall timer total.
 */
double timer_wall_total(int i) {

	return timers[i].wall_total;

}

/**
 * Return the Wall timer average.
 */
double timer_wall_avg(int i) {

	if (timers[i].calls == 0) {
		return 0;
	}

	return timers[i].wall_total / timers[i].calls;

}

/**
 * Print the timer format information.
 */
void timer_print_format() {
	printf(" NAME  :\tCPU TOTAL\tWALL TOTAL\tCPU AVG\tWALL AVG\tCALLS\n");
}

/**
 * Print the timer information in a sensible way.
 */
void timer_print(const char* name, int i) {

	// Prints as 'Name: CPU_TOTAL WALL_TOTAL CPU_AVG WALL_AVG CALLS
	printf(" %s:\t%f\t%f\t%f\t%f\t%d\n", name, timer_cpu_total(i), timer_wall_total(i), timer_cpu_avg(i), timer_wall_avg(i), timers[i].calls);

}
