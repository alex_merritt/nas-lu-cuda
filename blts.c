// C port of NPB3.2
// subroutine blts
#include "applu.h"

/**
 * Compute the regular-sparse, block lower triangular solution.
 *                     v <-- ( L-inv ) * v
 */
void blts ( const int ldmx, const int ldmy, const int ldmz,
			const int nx, const int ny, const int nz, const int starting_k,
			fp_type omega,
			fp_type**** v,
			fp_type***** ldz,
			fp_type***** ldy,
			fp_type***** ldx,
			fp_type***** d,			
			const int ist, const int iend, const int jst, const int jend,
			const int nx0, const int ny0, const int ipt, const int jpt) {

	/**
	 * Local variables.
	 */
	int i, j, k, m;
	int iex;
	fp_type tmp, tmp1;
	fp_type tmat[5][5];
	
	// Constants.
	const fp_type fpone = 1.0e+00;
		
	// Receive data from north and west.
	iex = 0;
	exchange_1(v, starting_k, iex);
	
	for (k = starting_k; k < starting_k + kblock; k++) {
	
		int level = k % kblock;

		for (j = 0; j < ny + 4; j++) {
			for (i = 0; i < nx + 4; i++) {
			
				if (k >= 1 && k <= nz - 2
					&& j >= jst && j <= jend
					&& i >= ist && i <= iend) {		
			
					for (m = 0; m < 5; m++) {
									
						v[k][j][i][m] =  v[k][j][i][m]
											- omega * ( ldz[level][j][i][0][m] * v[k-1][j][i][0]
													  + ldz[level][j][i][1][m] * v[k-1][j][i][1]
													  + ldz[level][j][i][2][m] * v[k-1][j][i][2]
													  + ldz[level][j][i][3][m] * v[k-1][j][i][3]
													  + ldz[level][j][i][4][m] * v[k-1][j][i][4] );
			
					}
				
				}	
				
			}
		}
	
		for (j = 0; j < ny + 4; j++) {
			for (i = 0; i < nx + 4; i++) {
			
				if (k >= 1 && k <= nz - 2
					&& j >= jst && j <= jend
					&& i >= ist && i <= iend) {				
		
					for (m = 0; m < 5; m++) {
				
						v[k][j][i][m] = v[k][j][i][m]
											- omega * ( ldy[level][j][i][0][m] * v[k][j-1][i][0]
													  + ldx[level][j][i][0][m] * v[k][j][i-1][0]
													  + ldy[level][j][i][1][m] * v[k][j-1][i][1]
													  + ldx[level][j][i][1][m] * v[k][j][i-1][1]
													  + ldy[level][j][i][2][m] * v[k][j-1][i][2]
													  + ldx[level][j][i][2][m] * v[k][j][i-1][2]
													  + ldy[level][j][i][3][m] * v[k][j-1][i][3]
													  + ldx[level][j][i][3][m] * v[k][j][i-1][3]
													  + ldy[level][j][i][4][m] * v[k][j-1][i][4]
													  + ldx[level][j][i][4][m] * v[k][j][i-1][4] );
			
					}

					/**
					 * Diagonal block inversion.
					 * Forward elimination.
					 */
					for (m = 0; m < 5; m++) {
						tmat[0][m] = d[level][j][i][0][m];
						tmat[1][m] = d[level][j][i][1][m];
						tmat[2][m] = d[level][j][i][2][m];
						tmat[3][m] = d[level][j][i][3][m];
						tmat[4][m] = d[level][j][i][4][m];
					}
	
					// ip = 0.
					tmp1 = fpone / tmat[0][0];
					tmp = tmp1 * tmat[0][1];
					tmat[1][1] = tmat[1][1] - tmp * tmat[1][0];
					tmat[2][1] = tmat[2][1] - tmp * tmat[2][0];
					tmat[3][1] = tmat[3][1] - tmp * tmat[3][0];
					tmat[4][1] = tmat[4][1] - tmp * tmat[4][0];
					v[k][j][i][1] = v[k][j][i][1] - v[k][j][i][0] * tmp;
	
					tmp = tmp1 * tmat[0][2];
					tmat[1][2] = tmat[1][2] - tmp * tmat[1][0];
					tmat[2][2] = tmat[2][2] - tmp * tmat[2][0];
					tmat[3][2] = tmat[3][2] - tmp * tmat[3][0];
					tmat[4][2] = tmat[4][2] - tmp * tmat[4][0];
					v[k][j][i][2] = v[k][j][i][2] - v[k][j][i][0] * tmp;
					   
					tmp = tmp1 * tmat[0][3];
					tmat[1][3] = tmat[1][3] - tmp * tmat[1][0];
					tmat[2][3] = tmat[2][3] - tmp * tmat[2][0];
					tmat[3][3] = tmat[3][3] - tmp * tmat[3][0];
					tmat[4][3] = tmat[4][3] - tmp * tmat[4][0];
					v[k][j][i][3] = v[k][j][i][3] - v[k][j][i][0] * tmp;
	
					tmp = tmp1 * tmat[0][4];
					tmat[1][4] = tmat[1][4] - tmp * tmat[1][0];
					tmat[2][4] = tmat[2][4] - tmp * tmat[2][0];
					tmat[3][4] = tmat[3][4] - tmp * tmat[3][0];
					tmat[4][4] = tmat[4][4] - tmp * tmat[4][0];
					v[k][j][i][4] = v[k][j][i][4] - v[k][j][i][0] * tmp;
	
					// ip = 1.
					tmp1 = fpone / tmat[1][1];
					tmp = tmp1 * tmat[1][2];
					tmat[2][2] = tmat[2][2] - tmp * tmat[2][1];
					tmat[3][2] = tmat[3][2] - tmp * tmat[3][1];
					tmat[4][2] = tmat[4][2] - tmp * tmat[4][1];
					v[k][j][i][2] = v[k][j][i][2] - v[k][j][i][1] * tmp;
	
					tmp = tmp1 * tmat[1][3];
					tmat[2][3] = tmat[2][3] - tmp * tmat[2][1];
					tmat[3][3] = tmat[3][3] - tmp * tmat[3][1];
					tmat[4][3] = tmat[4][3] - tmp * tmat[4][1];
					v[k][j][i][3] = v[k][j][i][3] - v[k][j][i][1] * tmp;
	
					tmp = tmp1 * tmat[1][4];
					tmat[2][4] = tmat[2][4] - tmp * tmat[2][1];
					tmat[3][4] = tmat[3][4] - tmp * tmat[3][1];
					tmat[4][4] = tmat[4][4] - tmp * tmat[4][1];
					v[k][j][i][4] = v[k][j][i][4] - v[k][j][i][1] * tmp;
	
					// ip = 2
					tmp1 = fpone / tmat[2][2];
					tmp = tmp1 * tmat[2][3];
					tmat[3][3] = tmat[3][3] - tmp * tmat[3][2];
					tmat[4][3] = tmat[4][3] - tmp * tmat[4][2];
					v[k][j][i][3] = v[k][j][i][3] - v[k][j][i][2] * tmp;
		
					tmp = tmp1 * tmat[2][4];
					tmat[3][4] = tmat[3][4] - tmp * tmat[3][2];
					tmat[4][4] = tmat[4][4] - tmp * tmat[4][2];
					v[k][j][i][4] = v[k][j][i][4] - v[k][j][i][2] * tmp;
	
					// ip = 3
					tmp1 = fpone / tmat[3][3];
					tmp = tmp1 * tmat[3][4];
					tmat[4][4] = tmat[4][4] - tmp * tmat[4][3];
					v[k][j][i][4] = v[k][j][i][4] - v[k][j][i][3] * tmp;
	
	
					/**
					 * Back substitution.
					 */
					v[k][j][i][4] = v[k][j][i][4] / tmat[4][4];
					 
					v[k][j][i][3] = v[k][j][i][3]
									- tmat[4][3] * v[k][j][i][4];
					v[k][j][i][3] = v[k][j][i][3] / tmat[3][3];
					 
					v[k][j][i][2] = v[k][j][i][2]
									- tmat[3][2] * v[k][j][i][3]
									- tmat[4][2] * v[k][j][i][4];
					v[k][j][i][2] = v[k][j][i][2] / tmat[2][2];
			
					v[k][j][i][1] = v[k][j][i][1]
									- tmat[2][1] * v[k][j][i][2]
									- tmat[3][1] * v[k][j][i][3]
									- tmat[4][1] * v[k][j][i][4];
					v[k][j][i][1] = v[k][j][i][1] / tmat[1][1];
			
					v[k][j][i][0] = v[k][j][i][0]
									- tmat[1][0] * v[k][j][i][1]
									- tmat[2][0] * v[k][j][i][2]
									- tmat[3][0] * v[k][j][i][3]
									- tmat[4][0] * v[k][j][i][4];
					v[k][j][i][0] = v[k][j][i][0] / tmat[0][0];
					
				}				

			}
		}

	}

	// Send data to south and east.
	iex = 2;
	exchange_1(v, starting_k + kblock - 1, iex);
	
}
