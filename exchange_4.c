//subroutine exchange_4(g,h,ibeg,ifin1,jbeg,jfin1)
#include "mpinpb.h"
#include "applu.h"

void exchange_4(fp_type** g, fp_type** h, int ibeg, int ifin1, int jbeg, int jfin1){

	/**
	 * Local variables.
	 */
	int i, j;
	int ny2;
	fp_type dum[1024];
	
	MPI_Request msgid1, msgid3;
	MPI_Status status;

	ny2 = ny + 2;
	
	/**
	 * Communicate in the east and west directions.
	 */
	 
	 // Receive from east.
	 if (jfin1 == ny) {
	 
	 	MPI_Irecv(dum, 2*nx, MPI_FP_TYPE, MPI_ANY_SOURCE, from_e, MPI_COMM_WORLD, &msgid3);
	 	MPI_Wait(&msgid3, &status);
	 	
	 	for (i = 1; i <= nx; i++) {
	 		g[ny+1][i] = dum[i-1];
	 		h[ny+1][i] = dum[i+nx-1];
	 	}
	 	
	 }

	// Send west.
	if (jbeg == 1) {
	
		for (i = 1; i <= nx; i++) {
			dum[i-1] = g[1][i];
			dum[i+nx-1] = h[1][i];
		}
		
		MPI_Send(dum, 2*nx, MPI_FP_TYPE, west, from_e, MPI_COMM_WORLD);
		
	}

	/**
	 * Communicate in the south and north directions.
	 */
	 
	// Receive from south.     
	if (ifin1 == nx) {
	
		MPI_Irecv(dum, 2*ny2, MPI_FP_TYPE, MPI_ANY_SOURCE, from_s, MPI_COMM_WORLD, &msgid1);
		MPI_Wait(&msgid1, &status);
		
		for (j = 0; j <= ny + 1; j++){
			g[j][nx+1] = dum[j+1-1];
			h[j][nx+1] = dum[j+ny2+1-1];
		}
		
	}
	
	// Send north.
	if (ibeg == 1) {
	
		for (j = 0; j <= ny+1; j++) {
			dum[j+1-1] = g[j][1];
			dum[j+ny2+1-1] = h[j][1];
		}
	
		MPI_Send(dum, 2*ny2, MPI_FP_TYPE, north, from_s, MPI_COMM_WORLD);
		
	}

}   
