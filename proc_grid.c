
/**
 *      subroutine proc_grid
*/

#include <math.h>
#include <stdio.h>
#include "applu.h"


/**
 *      implicit none
 *
 *      include 'applu.incl'
*/

/**
 *  local variables
*/

/**
 *   set up a two-d grid for processors: column-major ordering of unknowns
 *   NOTE: assumes a power-of-two number of processors
*/

/**
 * num - number of nodes(processors)
 * nodedim - a function such that it compute the exponent where num = 2**nodedim
 *	    i.e. ndim is the squar-root of num
*/
void proc_grid(){
//      xdim   = 2**(ndim/2)
	xdim = pow(2, (ndim/2));

//      if (mod(ndim,2).eq.1) xdim = xdim + xdim
	if(ndim%2 == 1){
		xdim = xdim + xdim;
	}

//      ydim   = num/xdim
        ydim = num/xdim;

//      write( *, 200) ndim
//200           format(' ndim ', i4)

	if (id == 0) {
		printf(" ndim %d\n", ndim);

	//      write( *, 210) xdim
	//210           format(' xdim ', i4)

		printf(" xdim %d\n", xdim);

	//      write( *, 201) ydim
	//201           format(' ydim ', i4)
	 
		printf(" ydim %d\n", ydim);
		
	}

	//      row    = mod(id,xdim) + 1
		row = (id%xdim) + 1;  

	//      col    = id/xdim + 1
		col = (id/xdim) + 1;

		//printf("Rank %d: Row = %d, Column = %d.\n", id, row, col);

//      return
}


