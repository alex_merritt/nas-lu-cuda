// C port of NPB3.2
// subroutine sethyper
#include "applu.h"

/**
 * Fore each column in a hyperplane, istart = first row,
 */ 
void sethyper() {

	/**
	 * Local variables.
	 */
	int i, j;
	int iglob, jglob;
	int kp;

	/**
	 * Compute the pointers for hyperplanes.
	 */
	for (kp = 2; kp <= nx0 + ny0; kp++) {

		icomms[kp-1] = 0;
		icommn[kp-1] = 0;
		icomme[kp-1] = 0;
		icommw[kp-1] = 0;

		/**
		 * Check to see if comm. to south is required.
		 */
		if (south != -1) {

			i = iend;
			iglob = ipt + i;
			jglob = kp - iglob;
			j = jglob - jpt;

			if (jglob >= 2 && jglob <= ny0 - 1 && j >= jst && j <= jend) {
				icomms[kp-1] = 1;
			}

		}

		/**
		 * Check to see if comm. to north is required.
		 */
		if (north != -1) {

			i = ist;
			iglob = ipt + i;
			jglob = kp - iglob;
			j = jglob - jpt;

			if (jglob >= 2 && jglob <= ny0 - 1 && j >= jst && j <= jend) {
				icommn[kp-1] = 1;
			}

		}

		/**
		 * Check to see if comm. to east is required.
		 */
		if (east != -1) {

			j = jend;
			jglob = jpt + j;
			iglob = kp - jglob;
			i = iglob - ipt;
		
			if (iglob >= 2 && iglob <= nx0 - 1 && i >= ist && i <= iend) {
				icomme[kp-1] = 1;
			}

		}
	
		/**
		 * Check to see if comm. to west is required.
		 */
		if (west != -1) {
		 
		 	j = jst;
		 	jglob = jpt + j;
		 	iglob = kp - jglob;
		 	i = iglob - ipt;
		 	
		 	if (iglob >= 2 && iglob <= nx0 - 1 && i >= ist && i <= iend) {
		 		icommw[kp-1] = 1;
		 	}
		 
		}
		
	}

	//intf("Memory issues: %d, %d, %d, %d, %d, %d, %d, %d, %d\n", &icomms[0], &icommn[0], &icomme[0], &icommw[0], &icomms[nx0 + ny0 + 1], &icommn[nx0 + ny0 + 1], &icomme[nx0 + ny0 + 1], &icommw[nx0 + ny0 + 1], &dzeta);

	//printf("npmax = %d\n", npmax);	
	//printf("nx0 + ny0 + 1 = %d\n", nx0 + ny0 + 1);

	icomms[0] = 0;
	icommn[0] = 0;
	icomme[0] = 0;
	icommw[0] = 0;

	icomms[nx0 + ny0] = 0;
	icommn[nx0 + ny0] = 0;
	icomme[nx0 + ny0] = 0;
	icommw[nx0 + ny0] = 0;

}
