#include <stdio.h>
#include <cuda.h>
#include <cutil.h>

#include "mpinpb.h"
#include "applu.h"
#include "kernels.cuh"

// Need to tell the compiler this is C code.
extern "C" {

	// Include some headers.
	#include "applu_cuda.h"
	#include "timers.h"	
	
	/**
	 * Global variables for this class; shared between all GPU wrapper functions.
	 */
	dim3 waveGrid, waveBlock;
	dim3 rhsGrid, rhsBlock;
	dim3 rearGrid, rearBlock;
	
	int max_buffer_size;
	
	// Include the other GPU functions.
	#include "util_cuda.cu"
		
	#define exblock 64
	#include "exchange_1.cu"
	#include "exchange_3.cu"
	
	#include "preprocessing.cu"
	#include "postprocessing.cu"
	#include "blts.cu"
	#include "buts.cu"
	#include "rhs.cu"
	#include "l2norm.cu"	
	
	/**
	 * Dirty hack for swapping pointers.
	 */	
	inline void swap_pointers(fp_type** x, fp_type** y) {
		
		fp_type* tmp = *x;
		*x = *y;
		*y = tmp;
		
	}

	/**
	 * Performs pseudo-time stepping SSOR iterations for given nonlinear pde's.
	 * Carries out work (jacld, blts, jacu, buts) on CUDA-capable GPUs.
	 */
	void ssor_gpu(int niter) {
	
		/**
		 * Local variables.
		 */
		int k;
		int istep;
		fp_type tmp;
		fp_type delunm[5];
		root = 0;
		
		// Block and grid for wavefront methods.
		waveGrid.x = gpugrid_x; waveGrid.y = 1; waveGrid.z = 1;
		waveBlock.x = gpublock_x; waveBlock.y = 1; waveBlock.z = 1;		

		// Block and grid for embarassingly parallel methods.
		rhsGrid.x = rhsgrid_x; rhsGrid.y = rhsgrid_y; rhsGrid.z = 1;
		rhsBlock.x = rhsblock_x; rhsBlock.y = rhsblock_y; rhsBlock.z = 1;
		
		// Block and grid for rearrangement step.
		rearGrid.x = rhsgrid_x; rearGrid.y = rhsgrid_y * isiz3; rearGrid.z = 1;
		rearBlock.x = rhsblock_x; rearBlock.y = rhsblock_y; rearBlock.z = 1;
		
		// Calculate the size of the biggest array we need.
		int flat_size = isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type);
		int hyper_size = problem_height * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type);
		int tiled_size = problem_height * (rhsblock_x * rhsgrid_x) * (rhsblock_y * rhsgrid_y) * 5 * sizeof(fp_type);
		if (flat_size > hyper_size) { max_buffer_size = flat_size; } else { max_buffer_size = hyper_size; }
		if (tiled_size > max_buffer_size) { max_buffer_size = tiled_size; }
		printf(" Buffer Size = %d\n", max_buffer_size);
		
		// Tell all functions that we are more interested in cache than smem.
		cudaFuncSetCacheConfig(blts_kernel, cudaFuncCachePreferL1);						
		cudaFuncSetCacheConfig(buts_kernel, cudaFuncCachePreferL1);	
		cudaFuncSetCacheConfig(rhs_setup_kernel, cudaFuncCachePreferL1);
		cudaFuncSetCacheConfig(rhs_xi2_kernel, cudaFuncCachePreferL1);
		cudaFuncSetCacheConfig(rhs_xi4_kernel, cudaFuncCachePreferL1);
		cudaFuncSetCacheConfig(rhs_eta2_kernel, cudaFuncCachePreferL1);
		cudaFuncSetCacheConfig(rhs_eta4_kernel, cudaFuncCachePreferL1);
		cudaFuncSetCacheConfig(rhs_zeta1_kernel, cudaFuncCachePreferL1);
		cudaFuncSetCacheConfig(rhs_zeta2_kernel, cudaFuncCachePreferL1);
		cudaFuncSetCacheConfig(rhs_zeta3_kernel, cudaFuncCachePreferL1);
		cudaFuncSetCacheConfig(rhs_zeta4_kernel, cudaFuncCachePreferL1);
		cudaFuncSetCacheConfig(rhs_zeta_dissipation_kernel, cudaFuncCachePreferL1);	
				
		// Get some GPU information in the first iteration.
		if (niter == 1 && id == 0) {
			print_gpu_info();		
	    }
		
		// Copy across constants.
		cudaMemcpyToSymbol("ist_d", &ist, sizeof(int));
		cudaMemcpyToSymbol("iend_d", &iend, sizeof(int));
		cudaMemcpyToSymbol("jst_d", &jst, sizeof(int));
		cudaMemcpyToSymbol("jend_d", &jend, sizeof(int));						
		cudaMemcpyToSymbol("nx_d", &nx, sizeof(int));
		cudaMemcpyToSymbol("ny_d", &ny, sizeof(int));
		cudaMemcpyToSymbol("nz_d", &nz, sizeof(int));
		cudaMemcpyToSymbol("dt_d", &dt, sizeof(fp_type));
		cudaMemcpyToSymbol("tx1_d", &tx1, sizeof(fp_type));
		cudaMemcpyToSymbol("tx2_d", &tx2, sizeof(fp_type));
		cudaMemcpyToSymbol("tx3_d", &tx3, sizeof(fp_type));
		cudaMemcpyToSymbol("ty1_d", &ty1, sizeof(fp_type));
		cudaMemcpyToSymbol("ty2_d", &ty2, sizeof(fp_type));
		cudaMemcpyToSymbol("ty3_d", &ty3, sizeof(fp_type));
		cudaMemcpyToSymbol("tz1_d", &tz1, sizeof(fp_type));
		cudaMemcpyToSymbol("tz2_d", &tz2, sizeof(fp_type));
		cudaMemcpyToSymbol("tz3_d", &tz3, sizeof(fp_type));	
		cudaMemcpyToSymbol("dx1_d", &dx1, sizeof(fp_type));
		cudaMemcpyToSymbol("dx2_d", &dx2, sizeof(fp_type));
		cudaMemcpyToSymbol("dx3_d", &dx3, sizeof(fp_type));
		cudaMemcpyToSymbol("dx4_d", &dx4, sizeof(fp_type));
		cudaMemcpyToSymbol("dx5_d", &dx5, sizeof(fp_type));
		cudaMemcpyToSymbol("dy1_d", &dy1, sizeof(fp_type));
		cudaMemcpyToSymbol("dy2_d", &dy2, sizeof(fp_type));
		cudaMemcpyToSymbol("dy3_d", &dy3, sizeof(fp_type));
		cudaMemcpyToSymbol("dy4_d", &dy4, sizeof(fp_type));
		cudaMemcpyToSymbol("dy5_d", &dy5, sizeof(fp_type));	
		cudaMemcpyToSymbol("dz1_d", &dz1, sizeof(fp_type));
		cudaMemcpyToSymbol("dz2_d", &dz2, sizeof(fp_type));
		cudaMemcpyToSymbol("dz3_d", &dz3, sizeof(fp_type));
		cudaMemcpyToSymbol("dz4_d", &dz4, sizeof(fp_type));
		cudaMemcpyToSymbol("dz5_d", &dz5, sizeof(fp_type));		
		cudaMemcpyToSymbol("omega_d", &omega, sizeof(fp_type));	
		cudaMemcpyToSymbol("dssp_d", &dssp, sizeof(fp_type));
		CUT_CHECK_ERROR("Error in a cudaMemcpyToSymbol.\n");
	
		// Allocate all of the buffers.
		luBuffers buffers;
		allocate_buffers(&buffers);
		
		// Compute wavefront offsets etc.
		int* wave2d_offsets = (int*) malloc( ((isiz2 + 4) + (isiz1 + 4)) * sizeof(int) );
		int* wave3d_offsets = (int*) malloc( ((isiz2 + 4) + (isiz1 + 4) + kblock - 1) * sizeof(int) );
		int* thread_map = (int*) malloc( ((isiz2 + 4) * (isiz1 + 4)) * sizeof(int) );
		
		int* columns = (int*) malloc ( ((isiz2 + 4) * (isiz1 + 4)) * sizeof(int) );
		int*    rows = (int*) malloc ( ((isiz2 + 4) * (isiz1 + 4)) * sizeof(int) );	
		memset(wave2d_offsets, 0, ((isiz2 + 4) + (isiz1 + 4)) * sizeof(int));
		memset(wave3d_offsets, 0, ((isiz2 + 4) + (isiz1 + 4) + kblock - 1) * sizeof(int));
		memset(thread_map, 0, ((isiz2 + 4) * (isiz1 + 4)) * sizeof(int));
		memset(columns, 0, ((isiz2 + 4) * (isiz1 + 4)) * sizeof(int));
		memset(rows, 0, ((isiz2 + 4) * (isiz1 + 4)) * sizeof(int));
		prepare_lookup_tables(wave2d_offsets, wave3d_offsets, columns, rows, thread_map, &buffers);
			
		// Begin pseudo-time stepping iterations.
		const fp_type fpone = 1.0e+00;
		const fp_type fptwo = 2.0e+00;
		
		tmp = fpone / ( omega * ( fptwo - omega ) );

		cudaMemcpyToSymbol("tmp_d", &tmp, sizeof(fp_type));
		CUT_CHECK_ERROR("Error in cudaMemcpyToSymbol for tmp.\n");	
		
		// Put all the data on the card.	
		cudaMemcpy(buffers.u,    u_flat,    isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type), cudaMemcpyHostToDevice);
		cudaMemcpy(buffers.rsd,  rsd_flat,  isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type), cudaMemcpyHostToDevice);
		cudaMemcpy(buffers.frct, frct_flat, isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type), cudaMemcpyHostToDevice);	
		cudaMemcpy(buffers.flux, flux_flat, isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type), cudaMemcpyHostToDevice);
		
		// Make the data tiled.
		flat_to_tiled(buffers.u, buffers.rearrangement, &buffers);
		swap_pointers(&buffers.u, &buffers.rearrangement);
		flat_to_tiled(buffers.rsd, buffers.rearrangement, &buffers);
		swap_pointers(&buffers.rsd, &buffers.rearrangement);		
		flat_to_tiled(buffers.frct, buffers.rearrangement, &buffers);
		swap_pointers(&buffers.frct, &buffers.rearrangement);		
		flat_to_tiled(buffers.flux, buffers.rearrangement, &buffers);
		swap_pointers(&buffers.flux, &buffers.rearrangement);		
		cudaThreadSynchronize();				
		CUT_CHECK_ERROR("Error in flat_to_tiled.\n");
		
		// Compute the steady-state residuals.
		rhs_gpu(&buffers);
		
		// Compute the L2 norms of newton iteration residuals.
		l2norm_gpu(rsdnm, &buffers);
		MPI_Barrier(MPI_COMM_WORLD);
	
		// Make sure all aggregate timers are initialised.
		timer_reset();

		// Start the timer for the ssor loop.
		cudaThreadSynchronize();
		timer_start(0);
		
		// Start the timestep loop - do for niter times.
		for (istep = 1; istep <= niter; istep++) {
	
			if (id == 0) {
				if (istep % 20 == 0 || istep == itmax || istep == 1) {
					if (niter > 1) {
						printf(" Pseudo-time SSOR iteration no. = %d.\n", istep);
					}
				}
			}
					
			// Perform SSOR iteration.
			preprocessing_gpu(&buffers);
			
			timer_start(9);	
			
			// Move to the hyperplane layout.
			tiled_to_hyperplane(buffers.u, buffers.rearrangement, &buffers);
			swap_pointers(&buffers.u, &buffers.rearrangement);			
			tiled_to_hyperplane(buffers.rsd, buffers.rearrangement, &buffers);
			swap_pointers(&buffers.rsd, &buffers.rearrangement);			
			CUT_CHECK_ERROR(" Error in tiled_to_hyperplane.\n");		
			cudaThreadSynchronize();
			
			timer_stop(9);
			
			// For each tile in k-dimension (from bottom to top).			
			// Perform the lower triangular solution.				
			for (k = 0; k < nz; k += kblock) {
			
				// Receive data from north and west.
				exchange_1_gpu(k, 0, &buffers);
				
				timer_start(3);		
				blts_gpu(k, &buffers);
				timer_stop(3);	

				// Send data south and east.
				exchange_1_gpu(k + kblock - 1, 2, &buffers);
				
			}

			// For each tile in k-dimension (from top to bottom).
			// Perform the upper triangular solution.				
			for (k = problem_height - 1; k >= 0; k -= kblock) {
									
				// Receive data from south and east.
				exchange_1_gpu(k, 1, &buffers);

				timer_start(5);
				buts_gpu(k, &buffers);
				timer_stop(5);		
				
				// Receive data from south and east.
				exchange_1_gpu(k - kblock + 1, 3, &buffers);			
				
			}	
			
			timer_start(9);	

			// Move to the tiled memory layout.
			hyperplane_to_tiled(buffers.u, buffers.rearrangement, &buffers);
			swap_pointers(&buffers.u, &buffers.rearrangement);			
			hyperplane_to_tiled(buffers.rsd, buffers.rearrangement, &buffers);
			swap_pointers(&buffers.rsd, &buffers.rearrangement);			
			CUT_CHECK_ERROR(" Error in hyperplane_to_tiled.\n");		
			cudaThreadSynchronize();
			
			timer_stop(9);
		
			// Update the variables.
			postprocessing_gpu(&buffers);
			
			// Compute the max-norms of newton iteration corrections.
			if (istep % inorm == 0) {
				l2norm_gpu(delunm, &buffers);
				if (ipr == 1 && id == 0) {
					printf(" RMS-norm of SSOR-iteration correction for first pde = %e.\n", delunm[0]);
					printf(" RMS-norm of SSOR-iteration correction for second pde = %e.\n", delunm[1]);
					printf(" RMS-norm of SSOR-iteration correction for third pde = %e.\n", delunm[2]);
					printf(" RMS-norm of SSOR-iteration correction for fourth pde = %e.\n", delunm[3]);
					printf(" RMS-norm of SSOR-iteration correction for fifth pde = %e.\n", delunm[4]);	
				} else if (ipr == 2 && id == 0) {
					printf(" %d, %f.\n", istep, delunm[4]);
				}
			}
					
			// Compute the steady-state residuals.
			rhs_gpu(&buffers);
			
			// Compute the max-norms of newton iteration residuals.
			if (istep % inorm == 0 || istep == itmax ) {		
				l2norm_gpu(rsdnm, &buffers);
				if (ipr == 1 && id == 0) {
					printf(" RMS-norm of steady state residual for first pde = %e.\n", rsdnm[0]);
					printf(" RMS-norm of steady state residual for second pde = %e.\n", rsdnm[1]);
					printf(" RMS-norm of steady state residual for third pde = %e.\n", rsdnm[2]);
					printf(" RMS-norm of steady state residual for fourth pde = %e.\n", rsdnm[3]);
					printf(" RMS-norm of steady state residual for fifth pde = %e.\n", rsdnm[4]);																
				}
			}
		
			// Check the newton-iteration residuals against the tolerance levels.
			if (rsdnm[0] < tolrsd[0] && rsdnm[1] < tolrsd[1] && rsdnm[2] < tolrsd[2]
				&& rsdnm[3] < tolrsd[3] && rsdnm[4] < tolrsd[4]) {
				if (ipr == 1 && id == 0) {
					printf(" Convergence was achieved after %d pseudo-time steps.\n", istep);
					return;
				}
			}
	
		}
	
		// End the timers.
		cudaThreadSynchronize();
		timer_stop(0);
		
		// Pull back all data that might be necessary.
		tiled_to_flat(buffers.u, buffers.rearrangement, &buffers);
		swap_pointers(&buffers.u, &buffers.rearrangement);		
		tiled_to_flat(buffers.rsd, buffers.rearrangement, &buffers);
		swap_pointers(&buffers.rsd, &buffers.rearrangement);		
		tiled_to_flat(buffers.frct, buffers.rearrangement, &buffers);
		swap_pointers(&buffers.frct, &buffers.rearrangement);		
		tiled_to_flat(buffers.flux, buffers.rearrangement, &buffers);
		swap_pointers(&buffers.flux, &buffers.rearrangement);		
		cudaThreadSynchronize();
		CUT_CHECK_ERROR(" Error in tiled_to_flat.\n");
		
		cudaMemcpy(u_flat,    buffers.u,    isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type), cudaMemcpyDeviceToHost);	
		cudaMemcpy(rsd_flat,  buffers.rsd,  isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type), cudaMemcpyDeviceToHost);
		cudaMemcpy(frct_flat, buffers.frct, isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type), cudaMemcpyDeviceToHost);
		cudaMemcpy(flux_flat, buffers.flux, isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type), cudaMemcpyDeviceToHost);
		cudaThreadSynchronize();			
		
		/**
		 * C cleanup.
		 */
		free(wave2d_offsets);
		free(wave3d_offsets);
		free(thread_map);
		free(columns);
		free(rows);

		free_buffers(&buffers);
		
	}

}

