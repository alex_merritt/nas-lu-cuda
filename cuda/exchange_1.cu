/**
 * iex = 0 : Receive north/west.
 * iex = 1 : Receive south/east.
 * iex = 2 : Send south/east.
 * iex = 3 : Send north/west.
 */
void exchange_1_gpu(int k, int iex, luBuffers* buffers) {
	
	MPI_Status status;
	
	// Thread blocks / grid.
	dim3 unpackGrid, unpackBlock;
	dim3 packGrid, packBlock;
	
	/**
	 * Receive north/west.
	 */
	if (iex == 0) {

		if (north != -1) {
		
			// Receive from north.
			MPI_Recv(jbuf_flat, kblock * (jend-jst+1) * 5, MPI_FP_TYPE, north, from_n, MPI_COMM_WORLD, &status);
			
			timer_start(10);			
			
			// Send the buffer to the GPU.
			cudaMemcpy(buffers->jbuffer, jbuf_flat, kblock * (jend-jst+1) * 5 * sizeof(fp_type), cudaMemcpyHostToDevice);
			
			// Unpack.
			unpackGrid.x = ceil( (jend - jst + 1) / (fp_type) exblock ); unpackGrid.y = kblock; unpackGrid.z = 1;
			unpackBlock.x = exblock; unpackBlock.y = 1; unpackBlock.z = 1;
			ex1_unpack_north <<< unpackGrid, unpackBlock >>> (buffers->jbuffer, buffers->rsd, k, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);
			
			cudaThreadSynchronize();
			timer_stop(10);
			
		
		}          

		if (west != -1) {
		
			// Receive from west.
			MPI_Recv(ibuf_flat, kblock * (iend-ist+1) * 5, MPI_FP_TYPE, west, from_w, MPI_COMM_WORLD, &status);
			
			timer_start(10);		
			
			// Send the buffer to the GPU.
			cudaMemcpy(buffers->ibuffer, ibuf_flat, kblock * (iend-ist+1) * 5 * sizeof(fp_type), cudaMemcpyHostToDevice);
			
			// Unpack.
			unpackGrid.x = ceil( (iend - ist + 1) / (fp_type) exblock ); unpackGrid.y = kblock; unpackGrid.z = 1;
			unpackBlock.x = exblock; unpackBlock.y = 1; unpackBlock.z = 1;
			ex1_unpack_west <<< unpackGrid, unpackBlock >>> (buffers->ibuffer, buffers->rsd, k, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);
			
			cudaThreadSynchronize();
			timer_stop(10);			
		
		}
	
	/**
	 * Receive south/east.
	 */
	} else if (iex == 1) {
	
		if (south != -1) {
		
			// Receive from south.
          	MPI_Recv(jbuf_flat, kblock * (jend-jst+1) * 5, MPI_FP_TYPE, south, from_s, MPI_COMM_WORLD, &status);
          	
			timer_start(10);	          	
          	
          	// Send the buffer to the GPU.
          	cudaMemcpy(buffers->jbuffer, jbuf_flat, kblock * (jend-jst+1) * 5 * sizeof(fp_type), cudaMemcpyHostToDevice);
          	
          	// Unpack.
			unpackGrid.x = ceil( (jend - jst + 1) / (fp_type) exblock ); unpackGrid.y = kblock; unpackGrid.z = 1;
			unpackBlock.x = exblock; unpackBlock.y = 1; unpackBlock.z = 1;
			ex1_unpack_south <<< unpackGrid, unpackBlock >>> (buffers->jbuffer, buffers->rsd, k, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);   
			
			cudaThreadSynchronize();
			timer_stop(10);	      	
          			
		}
		
		if (east != -1) {

			// Receive from east.		
			MPI_Recv(ibuf_flat, kblock*(iend-ist+1)*5, MPI_FP_TYPE, east, from_e, MPI_COMM_WORLD, &status);
			
			timer_start(10);		
			
			// Send the buffer to the GPU.
			cudaMemcpy(buffers->ibuffer, ibuf_flat, kblock * (iend-ist+1) * 5 * sizeof(fp_type), cudaMemcpyHostToDevice);
			
			// Unpack.
			unpackGrid.x = ceil( (iend - ist + 1) / (fp_type) exblock ); unpackGrid.y = kblock; unpackGrid.z = 1;
			unpackBlock.x = exblock; unpackBlock.y = 1; unpackBlock.z = 1;
			ex1_unpack_east <<< unpackGrid, unpackBlock >>> (buffers->ibuffer, buffers->rsd, k, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);	
			
			cudaThreadSynchronize();
			timer_stop(10);					
			
		}

	/**
	 * Send south/east.
	 */
	} else if (iex == 2) {
	
		if (south != -1) {
		
			timer_start(10);			
		
			// Pack.
			packGrid.x = ceil( (jend - jst + 1) / (fp_type) exblock ); packGrid.y = kblock; packGrid.z = 1;
			packBlock.x = exblock; packBlock.y = 1; packBlock.z = 1;
			ex1_pack_south <<< packGrid, packBlock >>> (buffers->jbuffer, buffers->rsd, k, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);		

			// Get buffer from the GPU.
			cudaMemcpy(jbuf_flat, buffers->jbuffer, kblock * (jend-jst+1) * 5 * sizeof(fp_type), cudaMemcpyDeviceToHost);
			
			cudaThreadSynchronize();
			timer_stop(10);			
			
			// Send south.			
			MPI_Send(jbuf_flat, kblock*(jend-jst+1)*5, MPI_FP_TYPE, south, from_n, MPI_COMM_WORLD);			
			
		}
		
		if (east != -1) {
		
			timer_start(10);
		
			// Pack.		
			packGrid.x = ceil( (iend - ist + 1) / (fp_type) exblock ); packGrid.y = kblock; packGrid.z = 1;
			packBlock.x = exblock; packBlock.y = 1; packBlock.z = 1;
			ex1_pack_east <<< packGrid, packBlock >>> (buffers->ibuffer, buffers->rsd, k, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);	
			
			// Get buffer from the GPU.
			cudaMemcpy(ibuf_flat, buffers->ibuffer, kblock * (iend-ist+1) * 5 * sizeof(fp_type), cudaMemcpyDeviceToHost);
			
			cudaThreadSynchronize();
			timer_stop(10);		
			
			// Send east.		
			MPI_Send(ibuf_flat, kblock*(iend-ist+1)*5, MPI_FP_TYPE, east, from_w, MPI_COMM_WORLD);
			
		}		
	
	/**
	 * Send north/west.
	 */
	} else {
	
		if (north != -1) {
		
			timer_start(10);
		
			// Pack.
			packGrid.x = ceil( (jend - jst + 1) / (fp_type) exblock ); packGrid.y = kblock; packGrid.z = 1;
			packBlock.x = exblock; packBlock.y = 1; packBlock.z = 1;
			ex1_pack_north <<< packGrid, packBlock >>> (buffers->jbuffer, buffers->rsd, k, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);
			
			// Get buffer from the GPU.
			cudaMemcpy(jbuf_flat, buffers->jbuffer, kblock * (jend-jst+1) * 5 * sizeof(fp_type), cudaMemcpyDeviceToHost);
			
			cudaThreadSynchronize();
			timer_stop(10);	
			
			// Send north.			
			MPI_Send(jbuf_flat, kblock*(jend-jst+1)*5, MPI_FP_TYPE, north, from_s, MPI_COMM_WORLD);		
			
		}

		if (west != -1) {
		
			timer_start(10);
		
			// Pack.
			packGrid.x = ceil( (iend - ist + 1) / (fp_type) exblock ); packGrid.y = kblock; packGrid.z = 1;
			packBlock.x = exblock; packBlock.y = 1; packBlock.z = 1;
			ex1_pack_west <<< packGrid, packBlock >>> (buffers->ibuffer, buffers->rsd, k, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);
			
			// Get buffer from the GPU.
			cudaMemcpy(ibuf_flat, buffers->ibuffer, kblock * (iend-ist+1) * 5 * sizeof(fp_type), cudaMemcpyDeviceToHost);	
			
			cudaThreadSynchronize();
			timer_stop(10);		

			// Send west.
			MPI_Send(ibuf_flat, kblock*(iend-ist+1)*5, MPI_FP_TYPE, west, from_e, MPI_COMM_WORLD);
			
		}

	}

}
