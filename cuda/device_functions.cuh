/**
 * Calculates the flat index for [k][j][i][m].
 */
__device__ int flat_index(int k, int j, int i, int m) {

	return ((k * (isiz2 + 4) + j) * (isiz1 + 4) + i) * 5 + m;
	
}

/**
 * Device function to calculate hyperplane index.
 */
__device__ int hyperplane_index(int k, int j, int i, int m, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {

	int offset = 0;

	// Calculate thread id.
	offset += thread_map_d[(j * (isiz1 + 4)) + i];
	
    // Jump enough blocks.
	int block_depth = k / kblock;
	int depth = k - (kblock * block_depth);
	offset += block_depth * ((isiz1 + 4) * (isiz2 + 4) * kblock);
	
	// Jump to the right wavefront.
	offset += wave_offset_3d[i + j + depth];

	// Update thread_offset.
	if ( (i + j + depth) >= kblock - 1 ) {
		offset = offset - wave_offset_2d[(i + j + depth) - (kblock - 1)];
	}
	
	// Add angle offset.
	offset += (m * problem_height * (isiz2 + 4) * (isiz1 + 4));
		
	return offset;
	
}

/**
 * Calculate the tiled index for [k][j][i][m].
 */
__device__ int tiled_index(int k, int j, int i, int m) {

	int offset = 0;

	// Add block offset.
	int block_i  = (i / rhsblock_x);
	int block_j  = (j / rhsblock_y);
	int block_id = (block_j * rhsgrid_x) + block_i;
	offset += block_id * (rhsblock_x * rhsblock_y * isiz3);

	// Add thread offset.
	int thread_i = i - (block_i * rhsblock_x);
	int thread_j = j - (block_j * rhsblock_y);
	offset += (thread_j * rhsblock_x) + thread_i;

	// Add k offset.
	offset += k * (rhsblock_x * rhsblock_y);

	// Add angle offset.
	offset += m * (rhsgrid_x * rhsgrid_y) * (rhsblock_x * rhsblock_y * isiz3);

	// Return.
	return offset;

}
