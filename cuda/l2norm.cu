/**
 * Wrapper function for l2norm on the gpu.
 */
void l2norm_gpu(fp_type* sum, luBuffers* buffers) {

	timer_start(7);		

	int m = 0;
	for (m = 0; m < 5; m++) {
		sum[m] = 0.0;
	}	
	cudaMemcpy(buffers->sum, sum, 5 * sizeof(fp_type), cudaMemcpyHostToDevice);
	l2norm_kernel <<< 5, 1 >>> (buffers->rsd, buffers->sum);
	cudaMemcpy(sum, buffers->sum, 5 * sizeof(fp_type), cudaMemcpyDeviceToHost);		
	
	MPI_Allreduce(MPI_IN_PLACE, sum, 5, MPI_FP_TYPE, MPI_SUM, MPI_COMM_WORLD);
	
	for (m = 0; m < 5; m++) {
		sum[m] = sqrt ( sum[m] / ( (nx0-2)*(ny0-2)*(nz0-2) ) );
	}

	CUT_CHECK_ERROR("Error in l2norm.\n");		
	cudaThreadSynchronize();
	
	timer_stop(7);
	
}
