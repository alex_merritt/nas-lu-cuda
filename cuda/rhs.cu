/**
 * Wrapper function for rhs step.
 */
void rhs_gpu(luBuffers* buffers) {
	
	// Rhs setup.
	timer_start(8);	
	rhs_setup_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->frct);
	cudaThreadSynchronize();
	timer_pause(8);
	
	// Exchange borders of u with neighbouring processes.
	exchange_3_gpu(0, buffers);
	cudaThreadSynchronize();
	
	timer_restart(8);		
	
	// Xi-direction buffers->flux differences.
	// Horrible if statements due to templates on north and south.
	if (north == -1 && south == -1) {
	
		rhs_xi1_kernel<-1, -1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);
		rhs_xi2_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->flux);
		rhs_xi3_kernel<-1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);	
		rhs_xi4_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd, buffers->flux);
		rhs_xi_dissipation_kernel<-1, -1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);
		
	} else if (north == -1 && south != -1) {
	
		rhs_xi1_kernel<-1, 0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);
		rhs_xi2_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->flux);
		rhs_xi3_kernel<0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);	
		rhs_xi4_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd, buffers->flux);
		rhs_xi_dissipation_kernel<-1, 0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);
		
	} else if (north != -1 && south == -1) {
	
		rhs_xi1_kernel<0, -1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);
		rhs_xi2_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->flux);
		rhs_xi3_kernel<-1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);	
		rhs_xi4_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd, buffers->flux);
		rhs_xi_dissipation_kernel<0, -1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);
		
	} else if (north != -1 && south != -1) {
	
		rhs_xi1_kernel<0, 0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);
		rhs_xi2_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->flux);
		rhs_xi3_kernel<0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);	
		rhs_xi4_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd, buffers->flux);
		rhs_xi_dissipation_kernel<0, 0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);
			
	}
	cudaThreadSynchronize();
	
	timer_pause(8);
	
	// Exchange borders of u with neighbouring processes.
	exchange_3_gpu(1, buffers);			
	cudaThreadSynchronize();
	
	timer_restart(8);			
	
	// Eta-direction buffers->flux differences.
	// Horrible if statements due to templates on east and west.
	if (west == -1 && east == -1) {
		
		rhs_eta1_kernel<-1, -1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);
		rhs_eta2_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->flux);
		rhs_eta3_kernel<-1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);	
		rhs_eta4_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd, buffers->flux);
		rhs_eta_dissipation_kernel<-1, -1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);
		
	} else if (west == -1 && east != -1) {
	
		rhs_eta1_kernel<-1, 0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);
		rhs_eta2_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->flux);
		rhs_eta3_kernel<0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);	
		rhs_eta4_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd, buffers->flux);
		rhs_eta_dissipation_kernel<-1, 0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);
		
	} else if (west != -1 && east == -1) {
	
		rhs_eta1_kernel<0, -1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);
		rhs_eta2_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->flux);
		rhs_eta3_kernel<-1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);	
		rhs_eta4_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd, buffers->flux);
		rhs_eta_dissipation_kernel<0, -1> <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);
		
	} else if (west != -1 && east != -1) {
	
		rhs_eta1_kernel<0, 0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);
		rhs_eta2_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->flux);
		rhs_eta3_kernel<0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);	
		rhs_eta4_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd, buffers->flux);
		rhs_eta_dissipation_kernel<0, 0> <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);
			
	}
	
	// Zeta-direction buffers->flux differences.
	rhs_zeta1_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);
	rhs_zeta2_kernel <<< rearGrid, rearBlock >>> (buffers->rsd, buffers->flux);
	rhs_zeta3_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->flux);	
	rhs_zeta4_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd, buffers->flux);
	rhs_zeta_dissipation_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);
	cudaThreadSynchronize();	

	CUT_CHECK_ERROR("Error in rhs.\n");		
	cudaThreadSynchronize();
	
	timer_stop(8);	
	
}
