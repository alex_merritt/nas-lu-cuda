/**
 * Wrapper function for preprocessing step.
 * Performs the SSOR iteration itself.
 */
void preprocessing_gpu(luBuffers* buffers) {

	timer_start(1);
	preprocessing_kernel <<< rearGrid, rearBlock >>> (buffers->rsd);
	
	CUT_CHECK_ERROR("Error in preprocessing.\n");		
	cudaThreadSynchronize();
	
	timer_stop(1);	
	
}
