/**
 * Wrapper function to replace buts().
 * Blocks proceed in a wavefront over the grid.
 */
void buts_gpu(int k, luBuffers* buffers) {

	int wave = 0;
	for (wave = (isiz1 + 4) + (isiz2 + 4) + kblock - 3; wave >= 0; wave--) {	
		buts_kernel <<< waveGrid, waveBlock >>> (buffers->rsd, buffers->u, wave, k, buffers->columns, buffers->rows, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);		
	}

	CUT_CHECK_ERROR("Error in buts.\n");		
	cudaThreadSynchronize();		

}	
