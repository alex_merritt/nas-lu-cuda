#include "size.h"
#include "debug.cuh"

/**********************************************************
 * CONSTANTS                                              *
 **********************************************************/
 
__constant__ int ist_d, iend_d, jst_d, jend_d;
__constant__ int nx_d, ny_d, nz_d;
__constant__ fp_type tx1_d, tx2_d, tx3_d,
					ty1_d, ty2_d, ty3_d,
					tz1_d, tz2_d, tz3_d;
__constant__ fp_type dx1_d, dx2_d, dx3_d, dx4_d, dx5_d,
					dy1_d, dy2_d, dy3_d, dy4_d, dy5_d,
					dz1_d, dz2_d, dz3_d, dz4_d, dz5_d;
__constant__ fp_type omega_d;
__constant__ fp_type dt_d;
__constant__ fp_type tmp_d;
__constant__ fp_type dssp_d;

/**********************************************************
 * DEVICE FUNCTIONS                                       *
 **********************************************************/

#include "device_functions.cuh"


/**********************************************************
 * OTHER KERNELS                                          *
 **********************************************************/
 
// Simple kernels to replace SSOR iteration and variable update.
#include "kernels/preprocessing.cuh"
#include "kernels/postprocessing.cuh"

// jacld and jacu kernels
#include "kernels/jacld/jacld_a.cuh"
#include "kernels/jacld/jacld_b.cuh"
#include "kernels/jacld/jacld_c.cuh"
#include "kernels/jacld/jacld_d.cuh"

#include "kernels/jacu/jacu_a.cuh"
#include "kernels/jacu/jacu_b.cuh"
#include "kernels/jacu/jacu_c.cuh"
#include "kernels/jacu/jacu_d.cuh"

// blts and buts kernels.
#include "kernels/blts/blts.cuh"
#include "kernels/buts/buts.cuh"

// Kernels for memory rearrangement.
#include "kernels/data_rearrangement.cuh"
#include "kernels/exchange/exchange_1_recv.cuh"
#include "kernels/exchange/exchange_1_send.cuh"
#include "kernels/exchange/exchange_3_recv.cuh"
#include "kernels/exchange/exchange_3_send.cuh"

// Kernels for rhs.
#include "kernels/rhs/rhs_setup.cuh"

// Kernels for xi-direction rhs.
#include "kernels/rhs/xi/rhs_xi1.cuh"
#include "kernels/rhs/xi/rhs_xi2.cuh"
#include "kernels/rhs/xi/rhs_xi3.cuh"
#include "kernels/rhs/xi/rhs_xi4.cuh"
#include "kernels/rhs/xi/rhs_xi_dissipation.cuh"

// Kernels for eta-direction rhs.
#include "kernels/rhs/eta/rhs_eta1.cuh"
#include "kernels/rhs/eta/rhs_eta2.cuh"
#include "kernels/rhs/eta/rhs_eta3.cuh"
#include "kernels/rhs/eta/rhs_eta4.cuh"
#include "kernels/rhs/eta/rhs_eta_dissipation.cuh"

// Kernels for zeta-direction rhs.
#include "kernels/rhs/zeta/rhs_zeta1.cuh"
#include "kernels/rhs/zeta/rhs_zeta2.cuh"
#include "kernels/rhs/zeta/rhs_zeta3.cuh"
#include "kernels/rhs/zeta/rhs_zeta4.cuh"
#include "kernels/rhs/zeta/rhs_zeta_dissipation.cuh"

// Kernel for l2norm.
#include "kernels/l2norm.cuh"
