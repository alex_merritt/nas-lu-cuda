#include "applu_cuda.h"

/**
 * Allocate all of the CUDA buffers.
 */
void allocate_buffers(luBuffers* buffers) {
	
	//
	// CUDAMALLOC CALLS
	//
	
	// Solution arrays.
	cudaMalloc((void **) &buffers->u,    max_buffer_size);
	cudaMalloc((void **) &buffers->rsd,  max_buffer_size);
	cudaMalloc((void **) &buffers->flux, max_buffer_size);		
	cudaMalloc((void **) &buffers->frct, max_buffer_size);
	
	// Reduction array.
	cudaMalloc((void **) &buffers->sum, 5 * sizeof(fp_type));
	
	// Rearrangement buffer.
	cudaMalloc((void **) &buffers->rearrangement, max_buffer_size);

	// Lookup tables.	
	cudaMalloc((void **) &buffers->columns, (isiz2 + 4) * (isiz1 + 4) * sizeof(int));
	cudaMalloc((void **) &buffers->rows, (isiz2 + 4) * (isiz1 + 4) * sizeof(int));
	cudaMalloc((void **) &buffers->wave2d_offsets, ((isiz2 + 4) + (isiz1 + 4)) * sizeof(int));
	cudaMalloc((void **) &buffers->wave3d_offsets, ((isiz2 + 4) + (isiz1 + 4) + kblock - 1) * sizeof(int));
	cudaMalloc((void **) &buffers->thread_map, (isiz2 + 4) * (isiz1 + 4) * sizeof(int));
	
	// Ex1
	cudaMalloc((void **) &buffers->ibuffer, kblock * 5 * (iend - ist + 1) * sizeof(fp_type));
	cudaMalloc((void **) &buffers->jbuffer, kblock * 5 * (jend - jst + 1) * sizeof(fp_type));
	
	// Ex3
	cudaMalloc((void **) &buffers->buf, 10 * isiz3 * isiz2 * sizeof(fp_type));
	cudaMalloc((void **) &buffers->buf1, 10 * isiz3 * isiz2 * sizeof(fp_type));
	
	CUT_CHECK_ERROR("Error in a cudaMalloc: ");	
	
	
	//
	// CUDAMEMSET CALLS
	//
	
	// Solution arrays.
	cudaMemset(buffers->u, 0,    max_buffer_size);
	cudaMemset(buffers->rsd, 0,  max_buffer_size);
	cudaMemset(buffers->flux, 0, max_buffer_size);		
	cudaMemset(buffers->frct, 0, max_buffer_size);
	
	// Reduction array.
	cudaMemset(buffers->sum, 0, 5 * sizeof(fp_type));
	
	// Rearrangement buffer.
	cudaMemset(buffers->rearrangement, 0, max_buffer_size);

	// Lookup tables.	
	cudaMemset(buffers->columns, 0, (isiz2 + 4) * (isiz1 + 4) * sizeof(int));
	cudaMemset(buffers->rows, 0, (isiz2 + 4) * (isiz1 + 4) * sizeof(int));
	cudaMemset(buffers->wave2d_offsets, 0, ((isiz2 + 4) + (isiz1 + 4)) * sizeof(int));
	cudaMemset(buffers->wave3d_offsets, 0, ((isiz2 + 4) + (isiz1 + 4) + kblock - 1) * sizeof(int));
	cudaMemset(buffers->thread_map, 0, (isiz2 + 4) * (isiz1 + 4) * sizeof(int));
	
	// Ex1
	cudaMemset(buffers->ibuffer, 0, kblock * 5 * (iend - ist + 1) * sizeof(fp_type));
	cudaMemset(buffers->jbuffer, 0, kblock * 5 * (jend - jst + 1) * sizeof(fp_type));
	
	// Ex3
	cudaMemset(buffers->buf, 0, 10 * isiz3 * isiz2 * sizeof(fp_type));
	cudaMemset(buffers->buf1, 0, 10 * isiz3 * isiz2 * sizeof(fp_type));
	
	CUT_CHECK_ERROR("Error in a cudaMemset: ");
	
}

/**
 * Free all of the CUDA buffers.
 */
void free_buffers(luBuffers* buffers) {

	cudaFree(buffers->u);
	cudaFree(buffers->rsd);
	cudaFree(buffers->frct);
	cudaFree(buffers->flux);
	
	cudaFree(buffers->sum);
	
	cudaFree(buffers->rearrangement);
	
	cudaFree(buffers->columns);
	cudaFree(buffers->rows);
	cudaFree(buffers->wave2d_offsets);
	cudaFree(buffers->wave3d_offsets);
	cudaFree(buffers->thread_map);
	
	cudaFree(buffers->ibuffer);
	cudaFree(buffers->jbuffer);
	
	cudaFree(buffers->buf);
	cudaFree(buffers->buf1);

}

/**
 * Prepare the lookup tables (and transfer them to the GPU).
 */
void prepare_lookup_tables(int* wave2d_offsets, int* wave3d_offsets, int* columns, int* rows, int* thread_map, luBuffers* buffers) {

	int wavefront = 0;
	int counter = 0;
	
	// Calculate the 2D offsets.
	for (wavefront = 0; wavefront < ( (isiz1 + 4) + (isiz2 + 4) ); wavefront++) {

		wave2d_offsets[wavefront] = counter;
		int i, j;
	
		// Note: Has to be for i, for j in order to maintain counting order (from bottom left).
		for (i = 0; i < (isiz1 + 4); i++) {
		
			// No point checking if this is true.
			if (i > wavefront) {
				break;
			}
			
			j = wavefront - i;
			if (j < (isiz2 + 4)) {
				thread_map[(j * (isiz1 + 4)) + i] = counter;
				columns[counter] = i;
				   rows[counter] = j;
				counter++;
			}
			
		}

	}
	
	wavefront = 0;
	counter = 0;
	
	// Calculate the 3D offsets.
	for (wavefront = 0; wavefront < ( (isiz1 + 4) + (isiz2 + 4) + kblock - 1 ); wavefront++) {

		wave3d_offsets[wavefront] = counter;
		int k, j, i;
			
		for (k = 0; k < kblock; k++) {
		
			// No point checking if this is true.
			if (k > wavefront) {
				break;
			}
		
			for (j = 0; j < (isiz2 + 4); j++) {
			
				// Also no point checking if this is true.
				if (j + k > wavefront) {
					break;
				}
			
				i = wavefront - (j + k);
				if (i < (isiz1 + 4)) {
					counter++;
				}

			}
			
		}

	}
	
	// Copy all of the lookup tables to the GPU.
	cudaMemcpy(buffers->wave2d_offsets, wave2d_offsets, ((isiz2 + 4) + (isiz1 + 4)) * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(buffers->wave3d_offsets, wave3d_offsets, ((isiz2 + 4) + (isiz1 + 4) + kblock - 1) * sizeof(int), cudaMemcpyHostToDevice);
	
	cudaMemcpy(buffers->columns, columns, (isiz2 + 4) * (isiz1 + 4) * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(buffers->rows, rows, (isiz2 + 4) * (isiz1 + 4) * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(buffers->thread_map, thread_map, (isiz2 + 4) * (isiz1 + 4) * sizeof(int), cudaMemcpyHostToDevice);

}

/**
 * Print device and application configuration information.
 */
void print_gpu_info() {

	cudaDeviceProp pdev;
	cudaGetDeviceProperties(&pdev, 0);
	cudaThreadSynchronize();
	printf(" CUDA Information:\n");
	printf("  Version: %d.%d.\n", pdev.major, pdev.minor);
	printf("  Multiprocessors: %d.\n", pdev.multiProcessorCount);
	printf("  Clock Rate: %d.\n", pdev.clockRate);
	printf("  Wave Grid Size: %d x %d x %d.\n", waveGrid.x, waveGrid.y, waveGrid.z);
	printf("  Wave Block Size: %d x %d x %d.\n", waveBlock.x, waveBlock.y, waveBlock.z);
	printf("  RHS Grid Size: %d x %d x %d.\n", rhsGrid.x, rhsGrid.y, rhsGrid.z);
	printf("  RHS Block Size: %d x %d x %d.\n", rhsBlock.x, rhsBlock.y, rhsBlock.z);			
	printf("  k-blocking factor of %d.\n", kblock);

}

/**
 * Move flat to tiled.
 */
void flat_to_tiled(fp_type* buffer, fp_type* rearrangement, luBuffers* buffers) {

	// Initially set the rearrangement buffer to 0.
	//cudaMemset(rearrangement, 0, max_buffer_size);

	// Permute the data in buffer and store it in rearrangement.
	flat_to_tiled_kernel <<< rhsGrid, rhsBlock >>> (buffer, rearrangement);
	
	// Copy rearrangement over the original buffer.
	//cudaMemcpy(buffer, rearrangement, max_buffer_size, cudaMemcpyDeviceToDevice);

}

/**
 * Move tiled to flat.
 */
void tiled_to_flat(fp_type* buffer, fp_type* rearrangement, luBuffers* buffers) {

	// Initially set the rearrangement buffer to 0.
	//cudaMemset(rearrangement, 0, max_buffer_size);

	// Permute the data in buffer and store it in rearrangement.
	tiled_to_flat_kernel <<< rhsGrid, rhsBlock >>> (buffer, rearrangement);
	
	// Copy rearrangement over the original buffer.
	//cudaMemcpy(buffer, rearrangement, max_buffer_size, cudaMemcpyDeviceToDevice);

}

/**
 * Move tiled to hyperplane.
 */
void tiled_to_hyperplane(fp_type* buffer, fp_type* rearrangement, luBuffers* buffers) {

	// Initially set the rearrangement buffer to 0.
	//cudaMemset(rearrangement, 0, max_buffer_size);

	// Permute the data in buffer and store it in rearrangement.
	tiled_to_hyperplane_kernel <<< rearGrid, rearBlock >>> (buffer, rearrangement, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);
	
	// Copy rearrangement over the original buffer.
	//cudaMemcpy(buffer, rearrangement, max_buffer_size, cudaMemcpyDeviceToDevice);

}

/**
 * Move hyperplane to tiled.
 */
void hyperplane_to_tiled(fp_type* buffer, fp_type* rearrangement, luBuffers* buffers) {

	// Initially set the rearrangement buffer to 0.
	//cudaMemset(rearrangement, 0, max_buffer_size);

	// Permute the data in buffer and store it in rearrangement.
	hyperplane_to_tiled_kernel <<< rearGrid, rearBlock >>> (buffer, rearrangement, buffers->wave2d_offsets, buffers->wave3d_offsets, buffers->thread_map);
	
	// Copy rearrangement over the original buffer.
	//cudaMemcpy(buffer, rearrangement, max_buffer_size, cudaMemcpyDeviceToDevice);

}
