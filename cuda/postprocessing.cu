/**
 * Wrapper function for postprocessing step.
 * Updates the variables.
 */
void postprocessing_gpu(luBuffers* buffers) {

	timer_start(6);
	postprocessing_kernel <<< rearGrid, rearBlock >>> (buffers->u, buffers->rsd);

	CUT_CHECK_ERROR("Error in postprocessing.\n");		
	cudaThreadSynchronize();
	
	timer_stop(6);
	
}
