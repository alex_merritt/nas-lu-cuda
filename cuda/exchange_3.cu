/**
 * iex = 0: north/south.
 * iex = 1: east/west.
 */
void exchange_3_gpu(int iex, luBuffers* buffers) {

	/**
	 * Local variables.
	 */

	MPI_Request mid;
	MPI_Status status;
	
	// Thread blocks / grid.
	dim3 unpackGrid, unpackBlock;
	dim3 packGrid, packBlock;	
	
	/**
	 * Communicate in the south and north directions.
	 */
	if (iex == 0) {

		if (north != -1) {
			MPI_Irecv(buf1_flat, 10*ny*nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_n, MPI_COMM_WORLD, &mid);
		}

		/**
		 * Send south.
		 */
		if (south != -1) {
		
			timer_start(11);	
		
			// Pack.
			packGrid.x = ceil( ny  / (fp_type) exblock ); packGrid.y = nz; packGrid.z = 1;
			packBlock.x = exblock; packBlock.y = 1; packBlock.z = 1;
			ex3_pack_south <<< packGrid, packBlock >>> (buffers->buf, buffers->u);		

			// Get buffer from the GPU.
			cudaMemcpy(buf_flat, buffers->buf, 10 * isiz3 * isiz2 * sizeof(fp_type), cudaMemcpyDeviceToHost);
			
			cudaThreadSynchronize();
			timer_stop(11);			

			// Send.
			MPI_Send(buf_flat, 10*ny*nz, MPI_FP_TYPE, south, from_n, MPI_COMM_WORLD);
			
		}

		/**
		 * Receive from north.
		 */
		if (north != -1) {
			
			MPI_Wait(&mid, &status);
			
			timer_start(11);				
			
			// Copy the buffer to the GPU.
			cudaMemcpy(buffers->buf1, buf1_flat, 10 * isiz3 * isiz2 * sizeof(fp_type), cudaMemcpyHostToDevice);
			
			// Unpack.
			unpackGrid.x = ceil( ny  / (fp_type) exblock ); unpackGrid.y = nz; unpackGrid.z = 1;
			unpackBlock.x = exblock; unpackBlock.y = 1; unpackBlock.z = 1;
			ex3_unpack_north <<< unpackGrid, unpackBlock >>> (buffers->buf1, buffers->u);
			
			cudaThreadSynchronize();
			timer_stop(11);			
			
		}

		if (south != -1) {
			MPI_Irecv(buf1_flat, 10*ny*nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_s, MPI_COMM_WORLD, &mid);
		}
		
		/**
		 * Send north.
		 */
		if (north != -1) {
		
			timer_start(11);			
		
			// Pack.
			packGrid.x = ceil( ny  / (fp_type) exblock ); packGrid.y = nz; packGrid.z = 1;
			packBlock.x = exblock; packBlock.y = 1; packBlock.z = 1;
			ex3_pack_north <<< packGrid, packBlock >>> (buffers->buf, buffers->u);		

			// Get buffer from the GPU.
			cudaMemcpy(buf_flat, buffers->buf, 10 * isiz3 * isiz2 * sizeof(fp_type), cudaMemcpyDeviceToHost);	
			
			cudaThreadSynchronize();
			timer_stop(11);				
			
			MPI_Send(buf_flat, 10*ny*nz, MPI_FP_TYPE, north, from_s, MPI_COMM_WORLD);	
			
		}
		
		/**
		 * Receive from south.
		 */
		if (south != -1) {
		
			MPI_Wait(&mid, &status);
			
			timer_start(11);				
			
			// Copy the buffer to the GPU.
			cudaMemcpy(buffers->buf1, buf1_flat, 10 * isiz3 * isiz2 * sizeof(fp_type), cudaMemcpyHostToDevice);
			
			// Unpack.
			unpackGrid.x = ceil( ny  / (fp_type) exblock ); unpackGrid.y = nz; unpackGrid.z = 1;
			unpackBlock.x = exblock; unpackBlock.y = 1; unpackBlock.z = 1;
			ex3_unpack_south <<< unpackGrid, unpackBlock >>> (buffers->buf1, buffers->u);
			
			cudaThreadSynchronize();
			timer_stop(11);			
		
		}		 

	/**
	 * Communicate in the east and west directions.
	 */
	} else {
	
		if (west != -1) {
			MPI_Irecv(buf1_flat, 10*nx*nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_w, MPI_COMM_WORLD, &mid);
		}	
	
		/**
		 * Send east.
		 */
		if (east != -1) {
		
			timer_start(11);			
		
			// Pack.
			packGrid.x = ceil( nx  / (fp_type) exblock ); packGrid.y = nz; packGrid.z = 1;
			packBlock.x = exblock; packBlock.y = 1; packBlock.z = 1;
			ex3_pack_east <<< packGrid, packBlock >>> (buffers->buf, buffers->u);		

			// Get buffer from the GPU.
			cudaMemcpy(buf_flat, buffers->buf, 10 * isiz3 * isiz2 * sizeof(fp_type), cudaMemcpyDeviceToHost);
			
			cudaThreadSynchronize();
			timer_stop(11);			

			MPI_Send(buf_flat, 10*nx*nz, MPI_FP_TYPE, east, from_w, MPI_COMM_WORLD);
			
		}	 

		/**
		 * Receive from west.
		 */
		if (west != -1) {
		
			MPI_Wait(&mid, &status);
			
			timer_start(11);				
			
			// Copy the buffer to the GPU.
			cudaMemcpy(buffers->buf1, buf1_flat, 10 * isiz3 * isiz2 * sizeof(fp_type), cudaMemcpyHostToDevice);
			
			// Unpack.
			unpackGrid.x = ceil( nx  / (fp_type) exblock ); unpackGrid.y = nz; unpackGrid.z = 1;
			unpackBlock.x = exblock; unpackBlock.y = 1; unpackBlock.z = 1;
			ex3_unpack_west <<< unpackGrid, unpackBlock >>> (buffers->buf1, buffers->u);
			
			cudaThreadSynchronize();
			timer_stop(11);			
		
		}
		
		if (east != -1){
			MPI_Irecv(buf1_flat, 10*nx*nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_e, MPI_COMM_WORLD, &mid);
		}		


		/**
		 * Send west.
		 */
		if (west != -1) {
		
			timer_start(11);			
		
			// Pack.
			packGrid.x = ceil( nx  / (fp_type) exblock ); packGrid.y = nz; packGrid.z = 1;
			packBlock.x = exblock; packBlock.y = 1; packBlock.z = 1;
			ex3_pack_west <<< packGrid, packBlock >>> (buffers->buf, buffers->u);		

			// Get buffer from the GPU.
			cudaMemcpy(buf_flat, buffers->buf, 10 * isiz3 * isiz2 * sizeof(fp_type), cudaMemcpyDeviceToHost);
			
			cudaThreadSynchronize();
			timer_stop(11);			

			MPI_Send(buf_flat, 10*nx*nz, MPI_FP_TYPE, west, from_e, MPI_COMM_WORLD);
			
		}		 

		/**
		 * Receive from east.
		 */
		if (east != -1) {
		
			MPI_Wait(&mid, &status);
			
			timer_start(11);				
			
			// Copy the buffer to the GPU.
			cudaMemcpy(buffers->buf1, buf1_flat, 10 * isiz3 * isiz2 * sizeof(fp_type), cudaMemcpyHostToDevice);
			
			// Unpack.
			unpackGrid.x = ceil( nx  / (fp_type) exblock ); unpackGrid.y = nz; unpackGrid.z = 1;
			unpackBlock.x = exblock; unpackBlock.y = 1; unpackBlock.z = 1;
			ex3_unpack_east <<< unpackGrid, unpackBlock >>> (buffers->buf1, buffers->u);
			
			cudaThreadSynchronize();
			timer_stop(11);			
		
		}

	}
	
}
