// Some macros to increase readability.
#define ursdblock (problem_height * (isiz2 + 4) * (isiz1 + 4))

/**
 * CUDA kernel to compute the regular-sparse, block upper triangular solution.
 *                     v <-- ( U-inv ) * v
 * // d = d, udx = a, udy = b, udz = c
 */
__global__ void buts_kernel(fp_type* v, fp_type* u, const int wave, const int starting_k, int* columns_d, int* rows_d, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	fp_type tmp, tmp1;
	
	// Constants.
	const fp_type fpone = 1.0e+00;
	
	// Use five temporary variables for aggregation.
	fp_type v0, v1, v2, v3, v4;
	fp_type vn0, vn1, vn2, vn3, vn4;
	fp_type un0, un1, un2, un3, un4;
	
	// Replace tmat55 with 25 fp_types to be stored in registers.
	fp_type tmat00, tmat01, tmat02, tmat03, tmat04,
			tmat10, tmat11, tmat12, tmat13, tmat14,
			tmat20, tmat21, tmat22, tmat23, tmat24,
			tmat30, tmat31, tmat32, tmat33, tmat34,
			tmat40, tmat41, tmat42, tmat43, tmat44;
		
	// Calculate actual thread i
	int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	int threads = (gridDim.x * blockDim.x);
	
	// Each thread actually processes (cells / threads) cells in a coalesced manner.
	int cell;
	for (cell = tid; cell < (isiz1 + 4) * (isiz2 + 4); cell += threads) {
	
		i = columns_d[cell];
		j = rows_d[cell];
		k = (starting_k - (kblock - 1)) + (wave - (i + j));

		// Check if this thread is active in this wave.
		/*int mem_offset = cell;
		if ( (wave) >= kblock - 1 ) {
			mem_offset = mem_offset - wave_offset_2d[(wave) - (kblock - 1)];
		}*/
		int depth = (wave - (i + j));
	
		if (i >= ist_d && i <= iend_d
			&& j >= jst_d && j <= jend_d
			&& k >= 1 && k <= nz_d - 2
			&& depth >= 0 && depth <= kblock - 1) {
			//&& mem_offset < wave_offset_3d[wave + 1] - wave_offset_3d[wave]) {

			int index = hyperplane_index(k, j, i, 0, wave_offset_2d, wave_offset_3d, thread_map_d);			
					
			// Update the values of v based on the cell's neighbour in the k direction.
			// Calculate the index for (k+1, j, i).
			int below = hyperplane_index(k+1, j, i, 0, wave_offset_2d, wave_offset_3d, thread_map_d);
		
			// Read in v neighbour.
			vn0 = v[below + 0 * ursdblock];
			vn1 = v[below + 1 * ursdblock];
			vn2 = v[below + 2 * ursdblock];
			vn3 = v[below + 3 * ursdblock];
			vn4 = v[below + 4 * ursdblock];
		
			// Read in u neighbour, for calculation of c.
			un0 = u[below + 0 * ursdblock];
			un1 = u[below + 1 * ursdblock];
			un2 = u[below + 2 * ursdblock];
			un3 = u[below + 3 * ursdblock];
			un4 = u[below + 4 * ursdblock];													
										
			vn0 = v[below + 0 * ursdblock];																									
			v0 = omega_d * ( jacu_c_value<0,0>(un0, un1, un2, un3, un4) * vn0 );
			v1 = omega_d * ( jacu_c_value<0,1>(un0, un1, un2, un3, un4) * vn0 );
			v2 = omega_d * ( jacu_c_value<0,2>(un0, un1, un2, un3, un4) * vn0 );
			v3 = omega_d * ( jacu_c_value<0,3>(un0, un1, un2, un3, un4) * vn0 );
			v4 = omega_d * ( jacu_c_value<0,4>(un0, un1, un2, un3, un4) * vn0 );																			
		
			vn1 = v[below + 1 * ursdblock];					
			v0 = v0 + omega_d * ( jacu_c_value<1,0>(un0, un1, un2, un3, un4) * vn1 );
			v1 = v1 + omega_d * ( jacu_c_value<1,1>(un0, un1, un2, un3, un4) * vn1 );
			v2 = v2 + omega_d * ( jacu_c_value<1,2>(un0, un1, un2, un3, un4) * vn1 );
			v3 = v3 + omega_d * ( jacu_c_value<1,3>(un0, un1, un2, un3, un4) * vn1 );
			v4 = v4 + omega_d * ( jacu_c_value<1,4>(un0, un1, un2, un3, un4) * vn1 );																			
		
			vn2 = v[below + 2 * ursdblock];					
			v0 = v0 + omega_d * ( jacu_c_value<2,0>(un0, un1, un2, un3, un4) * vn2 );
			v1 = v1 + omega_d * ( jacu_c_value<2,1>(un0, un1, un2, un3, un4) * vn2 );
			v2 = v2 + omega_d * ( jacu_c_value<2,2>(un0, un1, un2, un3, un4) * vn2 );	
			v3 = v3 + omega_d * ( jacu_c_value<2,3>(un0, un1, un2, un3, un4) * vn2 );	
			v4 = v4 + omega_d * ( jacu_c_value<2,4>(un0, un1, un2, un3, un4) * vn2 );																	
		
			vn3 = v[below + 3 * ursdblock];
			v0 = v0 + omega_d * ( jacu_c_value<3,0>(un0, un1, un2, un3, un4) * vn3 );
			v1 = v1 + omega_d * ( jacu_c_value<3,1>(un0, un1, un2, un3, un4) * vn3 );	
			v2 = v2 + omega_d * ( jacu_c_value<3,2>(un0, un1, un2, un3, un4) * vn3 );
			v3 = v3 + omega_d * ( jacu_c_value<3,3>(un0, un1, un2, un3, un4) * vn3 );
			v4 = v4 + omega_d * ( jacu_c_value<3,4>(un0, un1, un2, un3, un4) * vn3 );																			
		
			vn4 = v[below + 4 * ursdblock];
			v0 = v0 + omega_d * ( jacu_c_value<4,0>(un0, un1, un2, un3, un4) * vn4 );
			v1 = v1 + omega_d * ( jacu_c_value<4,1>(un0, un1, un2, un3, un4) * vn4 );								
			v2 = v2 + omega_d * ( jacu_c_value<4,2>(un0, un1, un2, un3, un4) * vn4 );	
			v3 = v3 + omega_d * ( jacu_c_value<4,3>(un0, un1, un2, un3, un4) * vn4 );	
			v4 = v4 + omega_d * ( jacu_c_value<4,4>(un0, un1, un2, un3, un4) * vn4 );
				

			// Update the values of v based on its neighbours in the j direction.
			int south = hyperplane_index(k, j+1, i, 0, wave_offset_2d, wave_offset_3d, thread_map_d);
		
			// Read in v neighbour.
			vn0 = v[south + 0 * ursdblock];
			vn1 = v[south + 1 * ursdblock];
			vn2 = v[south + 2 * ursdblock];
			vn3 = v[south + 3 * ursdblock];
			vn4 = v[south + 4 * ursdblock];
		
			// Read in u neighbour, for calculation of b.		
			un0 = u[south + 0 * ursdblock];
			un1 = u[south + 1 * ursdblock];
			un2 = u[south + 2 * ursdblock];
			un3 = u[south + 3 * ursdblock];
			un4 = u[south + 4 * ursdblock];					

			vn0 = v[south + 0 * ursdblock];																									
			v0 = v0 + omega_d * ( jacu_b_value<0,0>(un0, un1, un2, un3, un4) * vn0 );
			v1 = v1 + omega_d * ( jacu_b_value<0,1>(un0, un1, un2, un3, un4) * vn0 );
			v2 = v2 + omega_d * ( jacu_b_value<0,2>(un0, un1, un2, un3, un4) * vn0 );
			v3 = v3 + omega_d * ( jacu_b_value<0,3>(un0, un1, un2, un3, un4) * vn0 );
			v4 = v4 + omega_d * ( jacu_b_value<0,4>(un0, un1, un2, un3, un4) * vn0 );																			
		
			vn1 = v[south + 1 * ursdblock];					
			v0 = v0 + omega_d * ( jacu_b_value<1,0>(un0, un1, un2, un3, un4) * vn1 );
			v1 = v1 + omega_d * ( jacu_b_value<1,1>(un0, un1, un2, un3, un4) * vn1 );
			v2 = v2 + omega_d * ( jacu_b_value<1,2>(un0, un1, un2, un3, un4) * vn1 );
			v3 = v3 + omega_d * ( jacu_b_value<1,3>(un0, un1, un2, un3, un4) * vn1 );
			v4 = v4 + omega_d * ( jacu_b_value<1,4>(un0, un1, un2, un3, un4) * vn1 );																			
		
			vn2 = v[south + 2 * ursdblock];					
			v0 = v0 + omega_d * ( jacu_b_value<2,0>(un0, un1, un2, un3, un4) * vn2 );
			v1 = v1 + omega_d * ( jacu_b_value<2,1>(un0, un1, un2, un3, un4) * vn2 );
			v2 = v2 + omega_d * ( jacu_b_value<2,2>(un0, un1, un2, un3, un4) * vn2 );	
			v3 = v3 + omega_d * ( jacu_b_value<2,3>(un0, un1, un2, un3, un4) * vn2 );	
			v4 = v4 + omega_d * ( jacu_b_value<2,4>(un0, un1, un2, un3, un4) * vn2 );																	
		
			vn3 = v[south + 3 * ursdblock];
			v0 = v0 + omega_d * ( jacu_b_value<3,0>(un0, un1, un2, un3, un4) * vn3 );
			v1 = v1 + omega_d * ( jacu_b_value<3,1>(un0, un1, un2, un3, un4) * vn3 );	
			v2 = v2 + omega_d * ( jacu_b_value<3,2>(un0, un1, un2, un3, un4) * vn3 );
			v3 = v3 + omega_d * ( jacu_b_value<3,3>(un0, un1, un2, un3, un4) * vn3 );
			v4 = v4 + omega_d * ( jacu_b_value<3,4>(un0, un1, un2, un3, un4) * vn3 );																			
		
			vn4 = v[south + 4 * ursdblock];
			v0 = v0 + omega_d * ( jacu_b_value<4,0>(un0, un1, un2, un3, un4) * vn4 );
			v1 = v1 + omega_d * ( jacu_b_value<4,1>(un0, un1, un2, un3, un4) * vn4 );								
			v2 = v2 + omega_d * ( jacu_b_value<4,2>(un0, un1, un2, un3, un4) * vn4 );	
			v3 = v3 + omega_d * ( jacu_b_value<4,3>(un0, un1, un2, un3, un4) * vn4 );	
			v4 = v4 + omega_d * ( jacu_b_value<4,4>(un0, un1, un2, un3, un4) * vn4 );
				
				
			// Update the values of v based on its neighbours in the i direction.
			// Calculate the index of (k, j, i+1).
			int east = hyperplane_index(k, j, i+1, 0, wave_offset_2d, wave_offset_3d, thread_map_d);
		
			// Read in v neighbour.
			vn0 = v[east + 0 * ursdblock];
			vn1 = v[east + 1 * ursdblock];
			vn2 = v[east + 2 * ursdblock];
			vn3 = v[east + 3 * ursdblock];
			vn4 = v[east + 4 * ursdblock];
		
			// Read in u neighbour, for calculation of a.
			un0 = u[east + 0 * ursdblock];
			un1 = u[east + 1 * ursdblock];
			un2 = u[east + 2 * ursdblock];
			un3 = u[east + 3 * ursdblock];
			un4 = u[east + 4 * ursdblock];					

			vn0 = v[east + 0 * ursdblock];																									
			v0 = v0 + omega_d * ( jacu_a_value<0,0>(un0, un1, un2, un3, un4) * vn0 );
			v1 = v1 + omega_d * ( jacu_a_value<0,1>(un0, un1, un2, un3, un4) * vn0 );
			v2 = v2 + omega_d * ( jacu_a_value<0,2>(un0, un1, un2, un3, un4) * vn0 );
			v3 = v3 + omega_d * ( jacu_a_value<0,3>(un0, un1, un2, un3, un4) * vn0 );
			v4 = v4 + omega_d * ( jacu_a_value<0,4>(un0, un1, un2, un3, un4) * vn0 );																			
		
			vn1 = v[east + 1 * ursdblock];					
			v0 = v0 + omega_d * ( jacu_a_value<1,0>(un0, un1, un2, un3, un4) * vn1 );
			v1 = v1 + omega_d * ( jacu_a_value<1,1>(un0, un1, un2, un3, un4) * vn1 );
			v2 = v2 + omega_d * ( jacu_a_value<1,2>(un0, un1, un2, un3, un4) * vn1 );
			v3 = v3 + omega_d * ( jacu_a_value<1,3>(un0, un1, un2, un3, un4) * vn1 );
			v4 = v4 + omega_d * ( jacu_a_value<1,4>(un0, un1, un2, un3, un4) * vn1 );																			
		
			vn2 = v[east + 2 * ursdblock];					
			v0 = v0 + omega_d * ( jacu_a_value<2,0>(un0, un1, un2, un3, un4) * vn2 );
			v1 = v1 + omega_d * ( jacu_a_value<2,1>(un0, un1, un2, un3, un4) * vn2 );
			v2 = v2 + omega_d * ( jacu_a_value<2,2>(un0, un1, un2, un3, un4) * vn2 );	
			v3 = v3 + omega_d * ( jacu_a_value<2,3>(un0, un1, un2, un3, un4) * vn2 );	
			v4 = v4 + omega_d * ( jacu_a_value<2,4>(un0, un1, un2, un3, un4) * vn2 );																	
		
			vn3 = v[east + 3 * ursdblock];
			v0 = v0 + omega_d * ( jacu_a_value<3,0>(un0, un1, un2, un3, un4) * vn3 );
			v1 = v1 + omega_d * ( jacu_a_value<3,1>(un0, un1, un2, un3, un4) * vn3 );	
			v2 = v2 + omega_d * ( jacu_a_value<3,2>(un0, un1, un2, un3, un4) * vn3 );
			v3 = v3 + omega_d * ( jacu_a_value<3,3>(un0, un1, un2, un3, un4) * vn3 );
			v4 = v4 + omega_d * ( jacu_a_value<3,4>(un0, un1, un2, un3, un4) * vn3 );																			
		
			vn4 = v[east + 4 * ursdblock];
			v0 = v0 + omega_d * ( jacu_a_value<4,0>(un0, un1, un2, un3, un4) * vn4 );
			v1 = v1 + omega_d * ( jacu_a_value<4,1>(un0, un1, un2, un3, un4) * vn4 );								
			v2 = v2 + omega_d * ( jacu_a_value<4,2>(un0, un1, un2, un3, un4) * vn4 );	
			v3 = v3 + omega_d * ( jacu_a_value<4,3>(un0, un1, un2, un3, un4) * vn4 );	
			v4 = v4 + omega_d * ( jacu_a_value<4,4>(un0, un1, un2, un3, un4) * vn4 );					

			/**
			 * Diagonal block inversion.
			 */
			// Read in u values.			
			un0 = u[index + 0 * ursdblock];
			un1 = u[index + 1 * ursdblock];
			un2 = u[index + 2 * ursdblock];
			un3 = u[index + 3 * ursdblock];
			un4 = u[index + 4 * ursdblock];					 
			 
			tmat00 = jacu_d_value<0,0>(un0, un1, un2, un3, un4);
			tmat10 = jacu_d_value<1,0>(un0, un1, un2, un3, un4);
			tmat20 = jacu_d_value<2,0>(un0, un1, un2, un3, un4);
			tmat30 = jacu_d_value<3,0>(un0, un1, un2, un3, un4);
			tmat40 = jacu_d_value<4,0>(un0, un1, un2, un3, un4);
		
			tmat01 = jacu_d_value<0,1>(un0, un1, un2, un3, un4);
			tmat11 = jacu_d_value<1,1>(un0, un1, un2, un3, un4);
			tmat21 = jacu_d_value<2,1>(un0, un1, un2, un3, un4);
			tmat31 = jacu_d_value<3,1>(un0, un1, un2, un3, un4);
			tmat41 = jacu_d_value<4,1>(un0, un1, un2, un3, un4);
		
			tmat02 = jacu_d_value<0,2>(un0, un1, un2, un3, un4);
			tmat12 = jacu_d_value<1,2>(un0, un1, un2, un3, un4);
			tmat22 = jacu_d_value<2,2>(un0, un1, un2, un3, un4);
			tmat32 = jacu_d_value<3,2>(un0, un1, un2, un3, un4);
			tmat42 = jacu_d_value<4,2>(un0, un1, un2, un3, un4);
		
			tmat03 = jacu_d_value<0,3>(un0, un1, un2, un3, un4);
			tmat13 = jacu_d_value<1,3>(un0, un1, un2, un3, un4);
			tmat23 = jacu_d_value<2,3>(un0, un1, un2, un3, un4);
			tmat33 = jacu_d_value<3,3>(un0, un1, un2, un3, un4);
			tmat43 = jacu_d_value<4,3>(un0, un1, un2, un3, un4);
		
			tmat04 = jacu_d_value<0,4>(un0, un1, un2, un3, un4);
			tmat14 = jacu_d_value<1,4>(un0, un1, un2, un3, un4);
			tmat24 = jacu_d_value<2,4>(un0, un1, un2, un3, un4);
			tmat34 = jacu_d_value<3,4>(un0, un1, un2, un3, un4);
			tmat44 = jacu_d_value<4,4>(un0, un1, un2, un3, un4);

			// ip = 0.
			tmp1 = fpone / tmat00;
			tmp = tmp1 * tmat01;
			tmat11 = tmat11 - tmp * tmat10;
			tmat21 = tmat21 - tmp * tmat20;
			tmat31 = tmat31 - tmp * tmat30;
			tmat41 = tmat41 - tmp * tmat40;
			v1 = v1 - v0 * tmp;

			tmp = tmp1 * tmat02;
			tmat12 = tmat12 - tmp * tmat10;
			tmat22 = tmat22 - tmp * tmat20;
			tmat32 = tmat32 - tmp * tmat30;
			tmat42 = tmat42 - tmp * tmat40;
			v2 = v2 - v0 * tmp;
			   
			tmp = tmp1 * tmat03;
			tmat13 = tmat13 - tmp * tmat10;
			tmat23 = tmat23 - tmp * tmat20;
			tmat33 = tmat33 - tmp * tmat30;
			tmat43 = tmat43 - tmp * tmat40;
			v3 = v3 - v0 * tmp;

			tmp = tmp1 * tmat04;
			tmat14 = tmat14 - tmp * tmat10;
			tmat24 = tmat24 - tmp * tmat20;
			tmat34 = tmat34 - tmp * tmat30;
			tmat44 = tmat44 - tmp * tmat40;
			v4 = v4 - v0 * tmp;

			// ip = 1.
			tmp1 = fpone / tmat11;
			tmp = tmp1 * tmat12;
			tmat22 = tmat22 - tmp * tmat21;
			tmat32 = tmat32 - tmp * tmat31;
			tmat42 = tmat42 - tmp * tmat41;
			v2 = v2 - v1 * tmp;

			tmp = tmp1 * tmat13;
			tmat23 = tmat23 - tmp * tmat21;
			tmat33 = tmat33 - tmp * tmat31;
			tmat43 = tmat43 - tmp * tmat41;
			v3 = v3 - v1 * tmp;

			tmp = tmp1 * tmat14;
			tmat24 = tmat24 - tmp * tmat21;
			tmat34 = tmat34 - tmp * tmat31;
			tmat44 = tmat44 - tmp * tmat41;
			v4 = v4 - v1 * tmp;

			// ip = 2
			tmp1 = fpone / tmat22;
			tmp = tmp1 * tmat23;
			tmat33 = tmat33 - tmp * tmat32;
			tmat43 = tmat43 - tmp * tmat42;
			v3 = v3 - v2 * tmp;

			tmp = tmp1 * tmat24;
			tmat34 = tmat34 - tmp * tmat32;
			tmat44 = tmat44 - tmp * tmat42;
			v4 = v4 - v2 * tmp;

			// ip = 3
			tmp1 = fpone / tmat33;
			tmp = tmp1 * tmat34;
			tmat44 = tmat44 - tmp * tmat43;
			v4 = v4 - v3 * tmp;
					
			/**
			 * Back substitution.
			 */
			v4 = v4 / tmat44;
			 
			v3 = v3 - tmat43 * v4;
			v3 = v3 / tmat33;
			 
			v2 = v2
				- tmat32 * v3
				- tmat42 * v4;
			v2 = v2 / tmat22;

			v1 = v1
				- tmat21 * v2
				- tmat31 * v3
				- tmat41 * v4;
			v1 = v1 / tmat11;

			v0 = v0
				- tmat10 * v1
				- tmat20 * v2
				- tmat30 * v3
				- tmat40 * v4;
			v0 = v0 / tmat00;

			// Update the values of v.
			v[index + 0 * ursdblock] -= v0;
			v[index + 1 * ursdblock] -= v1;
			v[index + 2 * ursdblock] -= v2;
			v[index + 3 * ursdblock] -= v3;
			v[index + 4 * ursdblock] -= v4;
		
		}
		
	}

}
