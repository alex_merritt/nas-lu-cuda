/**
 * Device function for calculating, just-in-time, the value of a[k][j][i][l][m].
 */
template <int l, int m>
__device__ fp_type jacld_a_value(const fp_type u0, const fp_type u1, const fp_type u2, const fp_type u3, const fp_type u4) {

	// Some constants.
	const fp_type c1 = c1_def;
	const fp_type c2 = c2_def;
	const fp_type c3 = c3_def;
	const fp_type c4 = c4_def;
	const fp_type c5 = c5_def;		
	
	const fp_type fpzero = 0.0e+00;
	const fp_type fphalf = 0.50e+00;
	const fp_type fpone = 1.0e+00;
	const fp_type fptwo = 2.0e+00;
	const fp_type fpthree = 3.0e+00;
	const fp_type fpfour = 4.0e+00;	
	
	const fp_type r43 = ( fpfour / fpthree );
	const fp_type c1345 = c1 * c3 * c4 * c5;
	const fp_type c34 = c3 * c4;
	const fp_type tmp1 = fpone / u0;
	const fp_type tmp2 = tmp1 * tmp1;
	const fp_type tmp3 = tmp1 * tmp2;
	
	fp_type result;

	if (m == 0) {
		if (l == 0) result = -dt_d * tz1_d * dz1_d;
		if (l == 1) result = fpzero;
		if (l == 2) result =  fpzero;
		if (l == 3) result = -dt_d * tz2_d;
		if (l == 4) result = fpzero;
	}

	if (m == 1) {
		if (l == 0) result = -dt_d * tz2_d
						* ( - ( u1 * u3 ) * tmp2 )
						-dt_d * tz1_d * ( - c34 * tmp2 * u1 );
		if (l == 1) result = -dt_d * tz2_d * ( u3 * tmp1 )
						-dt_d * tz1_d * c34 * tmp1
						-dt_d * tz1_d * dz2_d;
		if (l == 2) result = fpzero;
		if (l == 3) result = -dt_d * tz2_d * ( u1 * tmp1 );
		if (l == 4) result = fpzero;
	}

	if (m == 2) {
		if (l == 0) result = -dt_d * tz2_d
						* ( - ( u2 * u3 ) * tmp2 )
						- dt_d * tz1_d * ( -c34 * tmp2 * u2 );
		if (l == 1) result = fpzero;
		if (l == 2) result =  -dt_d * tz2_d * ( u3 * tmp1 )
						-dt_d * tz1_d * ( c34 * tmp1 )
						-dt_d * tz1_d * dz3_d;
		if (l == 3) result = -dt_d * tz2_d * ( u2 * tmp1 );
		if (l == 4) result = fpzero;
	}

	if (m == 3) {
		if (l == 0) result = -dt_d * tz2_d
						* ( - (( u3 * tmp1 ) * ( u3 * tmp1 )) 
							+ fphalf * c2
							* ( ( u1 * u1
								+ u2 * u2
								+ u3 * u3 ) * tmp2 ) )
						-dt_d * tz1_d * ( -r43 * c34 * tmp2 * u3 );
		if (l == 1) result = -dt_d * tz2_d * ( - c2 * ( u1 * tmp1 ) );
		if (l == 2) result = -dt_d * tz2_d * ( - c2 * ( u2 * tmp1 ) );
		if (l == 3) result = -dt_d * tz2_d * ( fptwo - c2 ) * ( u3 * tmp1 )
						-dt_d * tz1_d * ( r43 * c34 * tmp1 )
						-dt_d * tz1_d * dz4_d;
		if (l == 4) result = -dt_d * tz2_d * c2;
	}

	if (m == 4) {
		if (l == 0) result = -dt_d * tz2_d
							* ( ( c2 * (  u1 * u1
										+ u2 * u2
										+ u3 * u3 ) * tmp2
								- c1 * ( u4 * tmp1 ) )
									 * ( u3 * tmp1 ) )
							-dt_d * tz1_d
							* ( - (       c34 - c1345 ) * tmp3 * ( u1 * u1 )
								- (       c34 - c1345 ) * tmp3 * ( u2 * u2 )
								- ( r43 * c34 - c1345 ) * tmp3 * ( u3 * u3 )
								- c1345 * tmp2 * u4 );					
		if (l == 1) result = -dt_d * tz2_d
							* ( - c2 * ( u1 * u3 ) * tmp2 )
							- dt_d * tz1_d * ( c34 - c1345 ) * tmp2 * u1;
		if (l == 2) result = -dt_d * tz2_d
							* ( - c2 * ( u2 * u3 ) * tmp2 )
							- dt_d * tz1_d * ( c34 - c1345 ) * tmp2 * u2;
		if (l == 3) result = -dt_d * tz2_d
						* ( c1 * ( u4 * tmp1 )
						- fphalf * c2
						* ( ( u1 * u1
							+ u2 * u2
							+ fpthree * u3 * u3 ) * tmp2 ) )
						-dt_d * tz1_d * ( r43 * c34 - c1345 ) * tmp2 * u3;
		if (l == 4) result = -dt_d * tz2_d
						* ( c1 * ( u3 * tmp1 ) )
						-dt_d * tz1_d * c1345 * tmp1
						-dt_d * tz1_d * dz5_d;
	}
	
	return result;

}
