// Macro!
#define ursdtile ((rhsgrid_x * rhsgrid_y) * (rhsblock_x * rhsblock_y * isiz3))

/**
 * CUDA kernel for updating rsd based on frct.
 */
__global__ void rhs_setup_kernel(fp_type* rsd, fp_type* frct) {

	// Calculate i and j values.
	const int i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	const int j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	const int k = (blockIdx.y / rhsgrid_y);
	
	if (i >= 2 && i <= nx_d + 1
		&& j >= 2 && j <= ny_d + 1
		&& k >= 0 && k <= nz_d - 1) {
		
			const int index = tiled_index(k, j, i, 0);

			rsd[index + 0 * ursdtile] = - frct[index + 0 * ursdtile];
			rsd[index + 1 * ursdtile] = - frct[index + 1 * ursdtile];
			rsd[index + 2 * ursdtile] = - frct[index + 2 * ursdtile];
			rsd[index + 3 * ursdtile] = - frct[index + 3 * ursdtile];
			rsd[index + 4 * ursdtile] = - frct[index + 4 * ursdtile];
	
	}

}
