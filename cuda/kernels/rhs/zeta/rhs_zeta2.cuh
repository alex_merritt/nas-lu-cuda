/**
 * CUDA kernel for the second part of zeta-direction flux differences.
 * Update rsd based on u.
 */
__global__ void rhs_zeta2_kernel(fp_type* rsd, fp_type* flux) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);

	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d - 2) {
		
			rsd[tiled_index(k, j, i, 0)] = rsd[tiled_index(k, j, i, 0)]
								- tz2_d * ( flux[tiled_index(k+1, j, i, 0)] - flux[tiled_index(k-1, j, i, 0)] );
								
			rsd[tiled_index(k, j, i, 1)] = rsd[tiled_index(k, j, i, 1)]
								- tz2_d * ( flux[tiled_index(k+1, j, i, 1)] - flux[tiled_index(k-1, j, i, 1)] );
								
			rsd[tiled_index(k, j, i, 2)] = rsd[tiled_index(k, j, i, 2)]
								- tz2_d * ( flux[tiled_index(k+1, j, i, 2)] - flux[tiled_index(k-1, j, i, 2)] );
								
			rsd[tiled_index(k, j, i, 3)] = rsd[tiled_index(k, j, i, 3)]
								- tz2_d * ( flux[tiled_index(k+1, j, i, 3)] - flux[tiled_index(k-1, j, i, 3)] );
								
			rsd[tiled_index(k, j, i, 4)] = rsd[tiled_index(k, j, i, 4)]
								- tz2_d * ( flux[tiled_index(k+1, j, i, 4)] - flux[tiled_index(k-1, j, i, 4)] );
			
	}
	
}
