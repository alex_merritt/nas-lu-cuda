/**
 * CUDA kernel for the fourth part of zeta-direction flux differences.
 * Update rsd based on u and flux.
 */
__global__ void rhs_zeta4_kernel(fp_type* u, fp_type* rsd, fp_type* flux) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	
	// Some constants.
	const fp_type c3 = c3_def;
	const fp_type c4 = c4_def;
	const fp_type fptwo = 2.0e+00;		
	
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);

	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d - 2) {
		
		rsd[tiled_index(k, j, i, 0)] = rsd[tiled_index(k, j, i, 0)]
							+ dz1_d * tz1_d * (             u[tiled_index(k-1, j, i, 0)]
											- fptwo * u[tiled_index(k, j, i, 0)]
											+           u[tiled_index(k+1, j, i, 0)] );
		rsd[tiled_index(k, j, i, 1)] = rsd[tiled_index(k, j, i, 1)]
							+ tz3_d * c3 * c4 * ( flux[tiled_index(k+1, j, i, 1)] - flux[tiled_index(k, j, i, 1)] )
							+ dz2_d * tz1_d * (             u[tiled_index(k-1, j, i, 1)]
											- fptwo * u[tiled_index(k, j, i, 1)]
											+           u[tiled_index(k+1, j, i, 1)] );
		rsd[tiled_index(k, j, i, 2)] = rsd[tiled_index(k, j, i, 2)]
							+ tz3_d * c3 * c4 * ( flux[tiled_index(k+1, j, i, 2)] - flux[tiled_index(k, j, i, 2)] )
							+ dz3_d * tz1_d * (             u[tiled_index(k-1, j, i, 2)]
											- fptwo * u[tiled_index(k, j, i, 2)]
											+           u[tiled_index(k+1, j, i, 2)] );
											
		rsd[tiled_index(k, j, i, 3)] = rsd[tiled_index(k, j, i, 3)]
							+ tz3_d * c3 * c4 * ( flux[tiled_index(k+1, j, i, 3)] - flux[tiled_index(k, j, i, 3)] )
							+ dz4_d * tz1_d * (             u[tiled_index(k-1, j, i, 3)]
											- fptwo * u[tiled_index(k, j, i, 3)]
											+           u[tiled_index(k+1, j, i, 3)] );
		rsd[tiled_index(k, j, i, 4)] = rsd[tiled_index(k, j, i, 4)]
							+ tz3_d * c3 * c4 * ( flux[tiled_index(k+1, j, i, 4)] - flux[tiled_index(k, j, i, 4)] )
							+ dz5_d * tz1_d * (             u[tiled_index(k-1, j, i, 4)]
											- fptwo * u[tiled_index(k, j, i, 4)]
											+           u[tiled_index(k+1, j, i, 4)] );
		
	}
	
}
