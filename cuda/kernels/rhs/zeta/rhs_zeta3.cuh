/**
 * CUDA kernel for the third part of zeta-direction flux differences.
 * Update flux based on u.
 */
__global__ void rhs_zeta3_kernel(fp_type* u, fp_type* flux) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	fp_type tmp;
	fp_type u21k, u31k, u41k, u51k;
	fp_type u21km1, u31km1, u41km1, u51km1;		
	
	// Some constants.
	const fp_type c1 = c1_def;
	const fp_type c5 = c5_def;		
	
	const fp_type fphalf = 0.50e+00;
	const fp_type fpone = 1.0e+00;
	const fp_type fpthree = 3.0e+00;
	const fp_type fpfour = 4.0e+00;
	const fp_type fpfive = 5.0e+00;
	const fp_type fpsix = 6.0e+00;
	
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);

	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d - 1) {

		tmp = fpone / u[tiled_index(k, j, i, 0)];
		
		u21k = tmp * u[tiled_index(k, j, i, 1)];
		u31k = tmp * u[tiled_index(k, j, i, 2)];
		u41k = tmp * u[tiled_index(k, j, i, 3)];
		u51k = tmp * u[tiled_index(k, j, i, 4)];
		
		tmp = fpone / u[tiled_index(k-1, j, i, 0)];
		
		u21km1 = tmp * u[tiled_index(k-1, j, i, 1)];
		u31km1 = tmp * u[tiled_index(k-1, j, i, 2)];
		u41km1 = tmp * u[tiled_index(k-1, j, i, 3)];
		u51km1 = tmp * u[tiled_index(k-1, j, i, 4)];
		
		flux[tiled_index(k, j, i, 1)] = tz3_d * ( u21k - u21km1 );
		flux[tiled_index(k, j, i, 2)] = tz3_d * ( u31k - u31km1 );
		flux[tiled_index(k, j, i, 3)] = (fpfour/fpthree) * tz3_d * ( u41k - u41km1 );
		flux[tiled_index(k, j, i, 4)] = fphalf * ( fpone - c1 * c5 )
							* tz3_d * ( ( u21k * u21k + u31k * u31k + u41k * u41k )
									- ( u21km1 * u21km1 + u31km1 * u31km1 + u41km1 * u41km1 ) )
							+ (fpone/fpsix)
							* tz3_d * ( u41k * u41k - u41km1 * u41km1 )
							+ c1 * c5 * tz3_d * ( u51k - u51km1 );
		
	}
	
}
