/**
 * CUDA kernel for the first part of zeta-direction flux differences.
 * Update flux based on u.
 */
__global__ void rhs_zeta1_kernel(fp_type* u, fp_type* flux) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	fp_type q, u41;
	
	// Some constants.
	const fp_type c1 = c1_def;
	const fp_type c2 = c2_def;	
	const fp_type fphalf = 0.50e+00;
	
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);

	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= jend_d
		&& k >= 0 && k <= nz_d - 1) {
		
		// Update flux.
		flux[tiled_index(k, j, i, 0)] = u[tiled_index(k, j, i, 3)];
		
		u41 = u[tiled_index(k, j, i, 3)] / u[tiled_index(k, j, i, 0)];
		
		q = fphalf * (   u[tiled_index(k, j, i, 1)] * u[tiled_index(k, j, i, 1)]
						 + u[tiled_index(k, j, i, 2)] * u[tiled_index(k, j, i, 2)]
						 + u[tiled_index(k, j, i, 3)] * u[tiled_index(k, j, i, 3)] )
						 / u[tiled_index(k, j, i, 0)];
		
		flux[tiled_index(k, j, i, 1)] = u[tiled_index(k, j, i, 1)] * u41;
		
		flux[tiled_index(k, j, i, 2)] = u[tiled_index(k, j, i, 2)] * u41;
		
		flux[tiled_index(k, j, i, 3)] = u[tiled_index(k, j, i, 3)] * u41 + c2 * ( u[tiled_index(k, j, i, 4)] - q );
		
		flux[tiled_index(k, j, i, 4)] = ( c1 * u[tiled_index(k, j, i, 4)] - c2 * q ) * u41;
	
	}

}
