/**
 * CUDA kernel for the fourth-order dissipation in the zeta direction.
 */
__global__ void rhs_zeta_dissipation_kernel(fp_type* u, fp_type* rsd) {

	/**
	 * Local variables.
	 */
	int i, j, k, m;	
	
	// Constants.
	const fp_type fpfour = 4.0e+00;
	const fp_type fpfive = 5.0e+00;
	const fp_type fpsix = 6.0e+00;
	
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);

	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= jend_d) {
		
		if (k == 1) {
			for (m = 0; m < 5; m++) {
		
				rsd[tiled_index(1, j, i, m)] = rsd[tiled_index(1, j, i, m)]
									 - dssp_d * ( + fpfive * u[tiled_index(1, j, i, m)]
												- fpfour * u[tiled_index(2, j, i, m)]
												+           u[tiled_index(3, j, i, m)] );
			}
		}
		
		else if (k == 2) {
			for (m = 0; m < 5; m++) {
			
				rsd[tiled_index(2, j, i, m)] = rsd[tiled_index(2, j, i, m)]
									 - dssp_d * ( - fpfour * u[tiled_index(1, j, i, m)]
									 			+ fpsix * u[tiled_index(2, j, i, m)]
									 			- fpfour * u[tiled_index(3, j, i, m)]
									 			+           u[tiled_index(4, j, i, m)] );
			}
		}

		// If k is in range, update rsd.
		if (k >= 3 && k <= nz_d - 4) {
		
			for (m = 0; m < 5; m++) {
		
				rsd[tiled_index(k, j, i, m)] = rsd[tiled_index(k, j, i, m)]
									 - dssp_d * (             u[tiled_index(k-2, j, i, m)]
									 			- fpfour * u[tiled_index(k-1, j, i, m)]
									 			+ fpsix * u[tiled_index(k, j, i, m)]
									 			- fpfour * u[tiled_index(k+1, j, i, m)]
									 			+           u[tiled_index(k+2, j, i, m)] );
			
			}
			
		}
		
		if (k == nz_d - 3) {
			for (m = 0; m < 5; m++) {
		
				rsd[tiled_index(nz_d-3, j, i, m)] = rsd[tiled_index(nz_d-3, j, i, m)]
									 - dssp_d * (           u[tiled_index(nz_d-5, j, i, m)]
									 			- fpfour * u[tiled_index(nz_d-4, j, i, m)]
									 			+ fpsix * u[tiled_index(nz_d-3, j, i, m)]
									 			- fpfour * u[tiled_index(nz_d-2, j, i, m)] );
			}
		}
		
		if (k == nz_d - 2) {
			for (m = 0; m < 5; m++) {			 			
			
				rsd[tiled_index(nz_d-2, j, i, m)] = rsd[tiled_index(nz_d-2, j, i, m)]
									 - dssp_d * (           u[tiled_index(nz_d-4, j, i, m)]
									 			- fpfour * u[tiled_index(nz_d-3, j, i, m)]
									 			+ fpfive * u[tiled_index(nz_d-2, j, i, m)] );
									 			
			}
		}
		
	}
	
}
