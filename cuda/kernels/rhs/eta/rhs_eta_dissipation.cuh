/**
 * CUDA kernel for the fourth-order dissipation in the eta-direction.
 */
template <int west, int east>
__global__ void rhs_eta_dissipation_kernel(fp_type* u, fp_type* rsd) {

	/**
	 * Local variables.
	 */
	int i, j, k, m;
	fp_type jst1, jend1;
	
	// Constants
	const fp_type fpfour = 4.0e+00;
	const fp_type fpfive = 5.0e+00;
	const fp_type fpsix  = 6.0e+00;

	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);
	
	// Check that i and j are allowable values.
	if (i >= ist_d && i <= iend_d
		&& k >= 1 && k <= nz_d - 2) {

		if (west == -1) {

			for (m = 0; m < 5; m++) {
				
				if (j == 3) {
					rsd[tiled_index(k, 3, i, m)] = rsd[tiled_index(k, 3, i, m)]
										 - dssp_d * ( + fpfive * u[tiled_index(k, 3, i, m)]
										 			- fpfour * u[tiled_index(k, 4, i, m)]
										 			+           u[tiled_index(k, 5, i, m)] );
				}
			
				if (j == 4) {
					rsd[tiled_index(k, 4, i, m)] = rsd[tiled_index(k, 4, i, m)]
										 - dssp_d * ( - fpfour * u[tiled_index(k, 3, i, m)]
													+ fpsix * u[tiled_index(k, 4, i, m)]
													- fpfour * u[tiled_index(k, 5, i, m)]
													+           u[tiled_index(k, 6, i, m)] );
				}

			}

		}

		// Update jst1 and jend1 based on east and west.
		if (west != -1) { jst1 = 2; }	
		if (east != -1) { jend1 = ny_d + 1; }						
		if (west == -1) { jst1 = 5; }			
		if (east == -1) { jend1 = ny_d - 2; }

		// If j is in range, update rsd.
		if (j >= jst1 && j <= jend1) {

			for (m = 0; m < 5; m++) {

				rsd[tiled_index(k, j, i, m)] = rsd[tiled_index(k, j, i, m)]
									- dssp_d * (              u[tiled_index(k, j-2, i, m)]
												- fpfour * u[tiled_index(k, j-1, i, m)]
												+ fpsix * u[tiled_index(k, j, i, m)]
												- fpfour * u[tiled_index(k, j+1, i, m)]
												+           u[tiled_index(k, j+2, i, m)] );

			}
	
		}

		if (east == -1) {

			for (m = 0; m < 5; m++) {

				if (j == ny_d - 1) {
					rsd[tiled_index(k, ny_d-1, i, m)] = rsd[tiled_index(k, ny_d-1, i, m)]
											- dssp_d * (              u[tiled_index(k, ny_d-3, i, m)]
														- fpfour * u[tiled_index(k, ny_d-2, i, m)]
														+ fpsix * u[tiled_index(k, ny_d-1, i, m)]
														- fpfour * u[tiled_index(k, ny_d, i, m)] );
				}
			
				if (j == ny_d) {
					rsd[tiled_index(k, ny_d, i, m)] = rsd[tiled_index(k, ny_d, i, m)]
											- dssp_d * (              u[tiled_index(k, ny_d-2, i, m)]
														- fpfour * u[tiled_index(k, ny_d-1, i, m)]
														+ fpfive * u[tiled_index(k, ny_d, i, m)] );
				}

			}

		}

	}
	
}
