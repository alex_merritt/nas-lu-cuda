// Macro!
#define ursdtile ((rhsgrid_x * rhsgrid_y) * (rhsblock_x * rhsblock_y * isiz3))

/**
 * CUDA kernel for the first part of eta-direction flux differences.
 * Update flux based on u.
 */
template <int west, int east>
__global__ void rhs_eta1_kernel(fp_type* u, fp_type* flux) {

	/**
	 * Local variables.
	 */
	fp_type q, u31;
	int L1, L2;
	
	// Some constants.
	const fp_type c1 = c1_def;
	const fp_type c2 = c2_def;
	const fp_type fphalf = 0.50e+00;

	// Calculate i and j values.
	const int i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	const int j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	const int k = (blockIdx.y / rhsgrid_y);
	
	// Set L1.
	if (west != -1) { L1 = 1; }
	if (west == -1) { L1 = 2; }
	
	// Set L2.
	if (east != -1) { L2 = ny_d + 2; }
	if (east == -1) { L2 = ny_d + 1; }
	
	if (i >= ist_d && i <= iend_d
		&& j >= L1 && j <= L2
		&& k >= 1 && k <= nz_d - 2) {
		
		// Read in the u values.
		const int index = tiled_index(k, j, i, 0);
	
		// Update flux.
		const fp_type u2 = u[index + 2 * ursdtile];		
		flux[index + 0 * ursdtile] = u2;
		
		const fp_type u0 = u[index + 0 * ursdtile];		
		u31 = u2 / u0;
		
		const fp_type u1 = u[index + 1 * ursdtile];
		const fp_type u3 = u[index + 3 * ursdtile];		
		q = fphalf * (  u1 * u1
						+ u2 * u2
						+ u3 * u3 )
						/ u0;

		flux[index + 1 * ursdtile] = u1 * u31;
		flux[index + 3 * ursdtile] = u3 * u31;
		
		const fp_type u4 = u[index + 4 * ursdtile];		
		flux[index + 2 * ursdtile] = u2 * u31 + c2 * ( u4 - q );		
		flux[index + 4 * ursdtile] = ( c1 * u4 - c2 * q ) * u31;
	
	}

}
