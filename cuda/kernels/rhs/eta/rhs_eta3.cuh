// Macro!
#define ursdtile ((rhsgrid_x * rhsgrid_y) * (rhsblock_x * rhsblock_y * isiz3))

/**
 * CUDA kernel for the third part of eta-direction flux differences.
 * Update flux based on u.
 */
template <int east>
__global__ void rhs_eta3_kernel(fp_type* u, fp_type* flux) {

	/**
	 * Local variables.
	 */
	int L2;	
	
	// Some constants.
	const fp_type c1 = c1_def;
	const fp_type c5 = c5_def;
	
	const fp_type fphalf = 0.50e+00;
	const fp_type fpone = 1.0e+00;
	const fp_type fpthree = 3.0e+00;
	const fp_type fpfour = 4.0e+00;
	const fp_type fpsix = 6.0e+00;

	// Calculate i and j values.
	const int i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	const int j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	const int k = (blockIdx.y / rhsgrid_y);
	
	// Update L2 based on east.
	if (east != -1) { L2 = ny_d + 2; }
	if (east == -1) { L2 = ny_d + 1; }	

	// Check that i and j are allowable values.
	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= L2
		&& k >= 1 && k <= nz_d - 2) {
		
		const int index = tiled_index(k, j, i, 0);
		const int index1 = tiled_index(k, j-1, i, 0);
		
		const fp_type tmp = fpone / u[index + 0 * ursdtile];
		const fp_type tmp1 = fpone / u[index1 + 0 * ursdtile];		
		
		const fp_type u21j = tmp * u[index + 1 * ursdtile];
		const fp_type u21jm1 = tmp1 * u[index1 + 1 * ursdtile];		
		flux[index + 1 * ursdtile] = ty3_d * ( u21j - u21jm1 );		
		
		const fp_type u31j = tmp * u[index + 2 * ursdtile];
		const fp_type u31jm1 = tmp1 * u[index1 + 2 * ursdtile];		
		flux[index + 2 * ursdtile] = (fpfour/fpthree) * ty3_d * (u31j - u31jm1);		
		
		const fp_type u41j = tmp * u[index + 3 * ursdtile];
		const fp_type u41jm1 = tmp1 * u[index1 + 3 * ursdtile];		
		flux[index + 3 * ursdtile] = ty3_d * ( u41j - u41jm1 );		
		
		const fp_type u51j = tmp * u[index + 4 * ursdtile];	
		const fp_type u51jm1 = tmp1 * u[index1 + 4 * ursdtile];
		flux[index + 4 * ursdtile] = fphalf * ( fpone - c1 * c5 )
							* ty3_d * ( ( u21j * u21j + u31j * u31j + u41j * u41j )
									- ( u21jm1 * u21jm1 + u31jm1 * u31jm1 + u41jm1 * u41jm1 ) )
									+ (fpone/fpsix)
									* ty3_d * ( u31j * u31j - u31jm1 * u31jm1 )
									+ c1 * c5 * ty3_d * ( u51j - u51jm1 );
		
	}
	
}

