// Macro!
#define ursdtile ((rhsgrid_x * rhsgrid_y) * (rhsblock_x * rhsblock_y * isiz3))

/**
 * CUDA kernel for the fourth part of eta-direction flux differences.
 * Update rsd based on u and flux.
 */
__global__ void rhs_eta4_kernel(fp_type* u, fp_type* rsd, fp_type* flux) {
	
	// Some constants.
	const fp_type c3 = c3_def;
	const fp_type c4 = c4_def;
	const fp_type fptwo = 2.0e+00;	

	// Calculate i and j values.
	const int i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	const int j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	const int k = (blockIdx.y / rhsgrid_y);
	
	// Check that i and j are allowable values.
	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d - 2) {
		
		// Calculate the indices.
		const int index = tiled_index(k,j,i,0);
		const int index1 = tiled_index(k,j+1,i,0);
		const int index2 = tiled_index(k,j-1,i,0);		

		rsd[index + 0 * ursdtile] = rsd[index + 0 * ursdtile]
							+ dy1_d * ty1_d * (             u[index2 + 0 * ursdtile]
											- fptwo * u[index + 0 * ursdtile]
											+           u[index1 + 0 * ursdtile] );
										
		rsd[index + 1 * ursdtile] = rsd[index + 1 * ursdtile]
							+ ty3_d * c3 * c4 * ( flux[index1 + 1 * ursdtile] - flux[index + 1 * ursdtile] )
							+ dy2_d * ty1_d * (             u[index2 + 1 * ursdtile]
											- fptwo * u[index + 1 * ursdtile]
											+           u[index1 + 1 * ursdtile] );
										
		rsd[index + 2 * ursdtile] = rsd[index + 2 * ursdtile]
							+ ty3_d * c3 * c4 * (flux[index1 + 2 * ursdtile] - flux[index + 2 * ursdtile] )
							+ dy3_d * ty1_d * (             u[index2 + 2 * ursdtile]
											- fptwo * u[index + 2 * ursdtile]
											+           u[index1 + 2 * ursdtile] );
										
		rsd[index + 3 * ursdtile] = rsd[index + 3 * ursdtile]
							+ ty3_d * c3 * c4 * (flux[index1 + 3 * ursdtile] - flux[index + 3 * ursdtile] )
							+ dy4_d * ty1_d * (             u[index2 + 3 * ursdtile]
											- fptwo * u[index + 3 * ursdtile]
											+           u[index1 + 3 * ursdtile] );
										
		rsd[index + 4 * ursdtile] = rsd[index + 4 * ursdtile]
							+ ty3_d * c3 * c4 * (flux[index1 + 4 * ursdtile] - flux[index + 4 * ursdtile] )
							+ dy5_d * ty1_d * (             u[index2 + 4 * ursdtile]
											- fptwo * u[index + 4 * ursdtile]
											+           u[index1 + 4 * ursdtile] );
		
	}
	
}

