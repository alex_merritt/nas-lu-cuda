// Macro!
#define ursdtile ((rhsgrid_x * rhsgrid_y) * (rhsblock_x * rhsblock_y * isiz3))

/**
 * CUDA kernel for the second part of eta-direction flux differences.
 * Update rsd based on flux.
 */
__global__ void rhs_eta2_kernel(fp_type* rsd, fp_type* flux) {

	// Calculate i and j values.
	const int i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	const int j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	const int k = (blockIdx.y / rhsgrid_y);
	
	// Check that i and j are allowable values.
	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d - 2) {
		
			// Calculate the indices.
			const int index = tiled_index(k, j, i, 0);
			const int index1 = tiled_index(k, j+1, i, 0);
			const int index2 = tiled_index(k, j-1, i, 0);
		
			rsd[index + 0 * ursdtile] -= ty2_d * ( flux[index1 + 0 * ursdtile] - flux[index2 + 0 * ursdtile] );
			rsd[index + 1 * ursdtile] -= ty2_d * ( flux[index1 + 1 * ursdtile] - flux[index2 + 1 * ursdtile] );
			rsd[index + 2 * ursdtile] -= ty2_d * ( flux[index1 + 2 * ursdtile] - flux[index2 + 2 * ursdtile] );
			rsd[index + 3 * ursdtile] -= ty2_d * ( flux[index1 + 3 * ursdtile] - flux[index2 + 3 * ursdtile] );
			rsd[index + 4 * ursdtile] -= ty2_d * ( flux[index1 + 4 * ursdtile] - flux[index2 + 4 * ursdtile] );																
		
	}
	
}

