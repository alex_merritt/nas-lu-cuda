/**
 * CUDA kernel for the second part of xi-direction flux differences.
 * Update rsd based on flux.
 */
__global__ void rhs_xi2_kernel(fp_type* rsd, fp_type* flux) {

	/**
	 * Local variables.
	 */
	int i, j, k;

	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);
	
	// Check that j and k are allowable values.
	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d - 2) {
		
		rsd[tiled_index(k, j, i, 0)] -= tx2_d * ( flux[tiled_index(k, j, i+1, 0)] - flux[tiled_index(k, j, i-1, 0)] );
		rsd[tiled_index(k, j, i, 1)] -= tx2_d * ( flux[tiled_index(k, j, i+1, 1)] - flux[tiled_index(k, j, i-1, 1)] );
		rsd[tiled_index(k, j, i, 2)] -= tx2_d * ( flux[tiled_index(k, j, i+1, 2)] - flux[tiled_index(k, j, i-1, 2)] );
		rsd[tiled_index(k, j, i, 3)] -= tx2_d * ( flux[tiled_index(k, j, i+1, 3)] - flux[tiled_index(k, j, i-1, 3)] );
		rsd[tiled_index(k, j, i, 4)] -= tx2_d * ( flux[tiled_index(k, j, i+1, 4)] - flux[tiled_index(k, j, i-1, 4)] );									
	
	}

}
