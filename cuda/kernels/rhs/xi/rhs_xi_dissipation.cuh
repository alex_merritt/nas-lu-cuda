/**
 * CUDA kernel for the fourth-order dissipation step in xi-direction.
 */
template <int north, int south>
__global__ void rhs_xi_dissipation_kernel(fp_type* u, fp_type* rsd) {

	/**
	 * Local variables.
	 */
	int i, j, k, m;
	fp_type ist1, iend1;		
	
	// Constants.
	const fp_type fpfour = 4.0e+00;
	const fp_type fpfive = 5.0e+00;
	const fp_type fpsix = 6.0e+00;

	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);

	// Check that j and k are allowable values.
	if (j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d -2) {
		
		if (north == -1) {

			for (m = 0; m < 5; m++) {
				
				if (i == 3) {
					rsd[tiled_index(k, j, 3, m)] -= dssp_d * (   + fpfive * u[tiled_index(k, j, 3, m)]
																	- fpfour * u[tiled_index(k, j, 4, m)]
																	+           u[tiled_index(k, j, 5, m)] );
				}
				
				if (i == 4) {
					rsd[tiled_index(k, j, 4, m)] -= dssp_d * (   - fpfour * u[tiled_index(k, j, 3, m)]
													 				+ fpsix * u[tiled_index(k, j, 4, m)]
														 			- fpfour * u[tiled_index(k, j, 5, m)]
														 			+           u[tiled_index(k, j, 6, m)] );
				}
	
			}

		}
		
		// Update ist1 and iend1 based on north and south.
		if (north != -1) { ist1 = 2; }
		if (south != -1) { iend1 = nx_d + 1; }		
		if (north == -1) { ist1 = 5; }
		if (south == -1) { iend1 = nx_d - 2; }

		// If i is in range, update rsd.
		if (i >= ist1 && i <= iend1) {

			for (m = 0; m < 5; m++) {
	
				rsd[tiled_index(k, j, i, m)] -= dssp_d * ( u[tiled_index(k, j, i-2, m)]
										 			- fpfour * u[tiled_index(k, j, i-1, m)]
										 			+ fpsix * u[tiled_index(k, j, i, m)]
										 			- fpfour * u[tiled_index(k, j, i+1, m)]
										 			+           u[tiled_index(k, j, i+2, m)] );
	
			}
		
		}

		if (south == -1) {

			for (m = 0; m < 5; m++) {
	
				if (i == nx_d - 1) {		
					rsd[tiled_index(k, j, nx_d-1, m)] -= dssp_d * (  u[tiled_index(k, j, nx_d-3, m)]
															- fpfour * u[tiled_index(k, j, nx_d-2, m)]
															+ fpsix * u[tiled_index(k, j, nx_d-1, m)]
															- fpfour * u[tiled_index(k, j, nx_d, m)] );
				}
												
				if (i == nx_d) {
					rsd[tiled_index(k, j, nx_d, m)] -= dssp_d * (    u[tiled_index(k, j, nx_d-2, m)]
												 			- fpfour * u[tiled_index(k, j, nx_d-1, m)]
												 			+ fpfive * u[tiled_index(k, j, nx_d, m)] );
				}
	
			}
		
		}
	
	}

}
