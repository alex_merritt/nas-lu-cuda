/**
 * CUDA kernel for the first part of xi-direction flux differences.
 * Update flux based on u.
 */
template <int north, int south>
__global__ void rhs_xi1_kernel(fp_type* u, fp_type* flux) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	fp_type q, u21;
	int L1, L2;
	
	// Some constants.
	const fp_type c1 = c1_def;
	const fp_type c2 = c2_def;	
	const fp_type fphalf = 0.50e+00;	
	
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);
	
	// Set L1.
	if (north != -1) { L1 = 1; }
	if (north == -1) { L1 = 2; }
	
	// Set L2.
	if (south != -1) { L2 = nx_d + 2; }
	if (south == -1) { L2 = nx_d + 1; }
	
	if (i >= L1 && i <= L2
		&& j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d - 2) {
	
		// Update flux.
		flux[tiled_index(k, j, i, 0)] = u[tiled_index(k, j, i, 1)];
		
		u21 = u[tiled_index(k, j, i, 1)] / u[tiled_index(k, j, i, 0)];
		
		q = fphalf * (  u[tiled_index(k, j, i, 1)] * u[tiled_index(k, j, i, 1)]
						+ u[tiled_index(k, j, i, 2)] * u[tiled_index(k, j, i, 2)]
						+ u[tiled_index(k, j, i, 3)] * u[tiled_index(k, j, i, 3)] )
						/ u[tiled_index(k, j, i, 0)];
						
		flux[tiled_index(k, j, i, 1)] = u[tiled_index(k, j, i, 1)] * u21 + c2 *
							( u[tiled_index(k, j, i, 4)] - q );
							
		flux[tiled_index(k, j, i, 2)] = u[tiled_index(k, j, i, 2)] * u21;
		
		flux[tiled_index(k, j, i, 3)] = u[tiled_index(k, j, i, 3)] * u21;
		
		flux[tiled_index(k, j, i, 4)] = ( c1 * u[tiled_index(k, j, i, 4)] - c2 * q ) * u21;
	
	}

}
