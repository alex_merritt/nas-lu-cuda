/**
 * CUDA kernel for the fourth part of xi-direction flux differences.
 * Update rsd based on u.
 */
__global__ void rhs_xi4_kernel(fp_type* u, fp_type* rsd, fp_type* flux) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	
	// Some constants.
	const fp_type c3 = c3_def;
	const fp_type c4 = c4_def;		
	const fp_type fptwo = 2.0e+00;

	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);
	
	// Check that j and k are allowable values.
	if (i >= ist_d && i <= iend_d
		&& j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d - 2) {

		rsd[tiled_index(k, j, i, 0)] += dx1_d * tx1_d * (  u[tiled_index(k, j, i-1, 0)]
													- fptwo * u[tiled_index(k, j, i, 0)]
													+           u[tiled_index(k, j, i+1, 0)] );
											
		rsd[tiled_index(k, j, i, 1)] += tx3_d * c3 * c4 * ( flux[tiled_index(k, j, i+1, 1)] - flux[tiled_index(k, j, i, 1)] )
												+ dx2_d * tx1_d *  (        u[tiled_index(k, j, i-1, 1)]
																- fptwo * u[tiled_index(k, j, i, 1)]
																+           u[tiled_index(k, j, i+1, 1)] );
											
		rsd[tiled_index(k, j, i, 2)] += tx3_d * c3 * c4 * ( flux[tiled_index(k, j, i+1, 2)] - flux[tiled_index(k, j, i, 2)] )
												+ dx3_d * tx1_d * (         u[tiled_index(k, j, i-1, 2)]
																- fptwo * u[tiled_index(k, j, i, 2)]
																+           u[tiled_index(k, j, i+1, 2)] );
											
		rsd[tiled_index(k, j, i, 3)] += tx3_d * c3 * c4 * ( flux[tiled_index(k, j, i+1, 3)] - flux[tiled_index(k, j, i, 3)] )
												+ dx4_d * tx1_d * (         u[tiled_index(k, j, i-1, 3)]
																- fptwo * u[tiled_index(k, j, i, 3)]
																+           u[tiled_index(k, j, i+1, 3)] );
											
		rsd[tiled_index(k, j, i, 4)] += tx3_d * c3 * c4 * ( flux[tiled_index(k, j, i+1, 4)] - flux[tiled_index(k, j, i, 4)] )
												+ dx5_d * tx1_d * (         u[tiled_index(k, j, i-1, 4)]
																- fptwo * u[tiled_index(k, j, i, 4)]
																+           u[tiled_index(k, j, i+1, 4)] );
																
	}

}
