/**
 * CUDA kernel for the third part of xi-direction flux differences.
 * Update flux (again) based on u.
 */
template <int south>
__global__ void rhs_xi3_kernel(fp_type* u, fp_type* flux) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	int L2;
	fp_type u21i, u31i, u41i, u51i;
	fp_type u21im1, u31im1, u41im1, u51im1;
	fp_type tmp;
	
	// Some constants.
	const fp_type c1 = c1_def;
	const fp_type c5 = c5_def;	
	
	const fp_type fphalf = 0.50e+00;
	const fp_type fpone = 1.0e+00;
	const fp_type fpthree = 3.0e+00;
	const fp_type fpfour = 4.0e+00;
	const fp_type fpsix = 6.0e+00;	

	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);
	
	// Update L2 based on south.
	if (south != -1) { L2 = nx_d + 2; }
	if (south == -1) { L2 = nx_d + 1; }	
	
	// Check that j and k are allowable values.
	if (i >= ist_d && i <= L2
		&& j >= jst_d && j <= jend_d
		&& k >= 1 && k <= nz_d - 2) {
	
		tmp = fpone / u[tiled_index(k, j, i, 0)];

		u21i = tmp * u[tiled_index(k, j, i, 1)];
		u31i = tmp * u[tiled_index(k, j, i, 2)];
		u41i = tmp * u[tiled_index(k, j, i, 3)];
		u51i = tmp * u[tiled_index(k, j, i, 4)];

		tmp = fpone / u[tiled_index(k, j, i-1, 0)];

		u21im1 = tmp * u[tiled_index(k, j, i-1, 1)];
		u31im1 = tmp * u[tiled_index(k, j, i-1, 2)];
		u41im1 = tmp * u[tiled_index(k, j, i-1, 3)];
		u51im1 = tmp * u[tiled_index(k, j, i-1, 4)];

		flux[tiled_index(k, j, i, 1)] = ( fpfour / fpthree ) * tx3_d * (u21i - u21im1);
		flux[tiled_index(k, j, i, 2)] = tx3_d * ( u31i - u31im1 );
		flux[tiled_index(k, j, i, 3)] = tx3_d * ( u41i - u41im1 );
		flux[tiled_index(k, j, i, 4)] = fphalf * ( fpone - c1 * c5 )
										* tx3_d * ( ( u21i * u21i + u31i * u31i + u41i * u41i )
												- ( u21im1 * u21im1 + u31im1 * u31im1 + u41im1 * u41im1 ) )
										+ (fpone/fpsix)
										* tx3_d * ( u21i * u21i - u21im1 * u21im1 )
										+ c1 * c5 * tx3_d * ( u51i - u51im1 );

	
	}

}
