// Some macros to increase readability.
#define ursdblock (problem_height * (isiz2 + 4) * (isiz1 + 4))

// Packs g into buf.
__global__ void ex3_unpack_north(fp_type* buf1, fp_type* g) {

	// Check if thread is within range.
	int j = 2 + (blockIdx.x * blockDim.x) + threadIdx.x;
	int k = blockIdx.y;
	if (j <= ny_d + 1 && k <= nz_d - 1) {
		
		int ipos1 = k*ny_d + j - 2;
		int ipos2 = ipos1 + ny_d*nz_d;
				
		g[tiled_index(k, j, 0, 0)] = buf1[(ipos1 * 5) + 0];
		g[tiled_index(k, j, 0, 1)] = buf1[(ipos1 * 5) + 1];
		g[tiled_index(k, j, 0, 2)] = buf1[(ipos1 * 5) + 2];
		g[tiled_index(k, j, 0, 3)] = buf1[(ipos1 * 5) + 3];
		g[tiled_index(k, j, 0, 4)] = buf1[(ipos1 * 5) + 4];

		g[tiled_index(k, j, 1, 0)] = buf1[(ipos2 * 5) + 0];
		g[tiled_index(k, j, 1, 1)] = buf1[(ipos2 * 5) + 1];
		g[tiled_index(k, j, 1, 2)] = buf1[(ipos2 * 5) + 2];
		g[tiled_index(k, j, 1, 3)] = buf1[(ipos2 * 5) + 3];
		g[tiled_index(k, j, 1, 4)] = buf1[(ipos2 * 5) + 4];
	
	}
	
}

// Packs g into buf.
__global__ void ex3_unpack_south(fp_type* buf1, fp_type* g) {

	// Check if thread is within range.
	int j = 2 + (blockIdx.x * blockDim.x) + threadIdx.x;
	int k = blockIdx.y;
	if (j <= ny_d + 1 && k <= nz_d - 1) {
		
		int ipos1 = k*ny_d + j - 2;
		int ipos2 = ipos1 + ny_d*nz_d;
	
		g[tiled_index(k, j, nx_d + 3, 0)] = buf1[(ipos1 * 5) + 0];
		g[tiled_index(k, j, nx_d + 3, 1)] = buf1[(ipos1 * 5) + 1];
		g[tiled_index(k, j, nx_d + 3, 2)] = buf1[(ipos1 * 5) + 2];
		g[tiled_index(k, j, nx_d + 3, 3)] = buf1[(ipos1 * 5) + 3];
		g[tiled_index(k, j, nx_d + 3, 4)] = buf1[(ipos1 * 5) + 4];

		g[tiled_index(k, j, nx_d + 2, 0)] = buf1[(ipos2 * 5) + 0];
		g[tiled_index(k, j, nx_d + 2, 1)] = buf1[(ipos2 * 5) + 1];
		g[tiled_index(k, j, nx_d + 2, 2)] = buf1[(ipos2 * 5) + 2];
		g[tiled_index(k, j, nx_d + 2, 3)] = buf1[(ipos2 * 5) + 3];
		g[tiled_index(k, j, nx_d + 2, 4)] = buf1[(ipos2 * 5) + 4];
	
	}
	
}

// Packs g into buf.
__global__ void ex3_unpack_west(fp_type* buf1, fp_type* g) {

	// Check if thread is within range.
	int i = 2 + (blockIdx.x * blockDim.x) + threadIdx.x;
	int k = blockIdx.y;
	if (i <= nx_d + 1 && k <= nz_d - 1) {
		
		int ipos1 = k*nx_d + i - 2;
		int ipos2 = ipos1 + nx_d*nz_d;
	
		g[tiled_index(k, 0, i, 0)] = buf1[(ipos1 * 5) + 0];
		g[tiled_index(k, 0, i, 1)] = buf1[(ipos1 * 5) + 1];
		g[tiled_index(k, 0, i, 2)] = buf1[(ipos1 * 5) + 2];
		g[tiled_index(k, 0, i, 3)] = buf1[(ipos1 * 5) + 3];
		g[tiled_index(k, 0, i, 4)] = buf1[(ipos1 * 5) + 4];

		g[tiled_index(k, 1, i, 0)] = buf1[(ipos2 * 5) + 0];
		g[tiled_index(k, 1, i, 1)] = buf1[(ipos2 * 5) + 1];
		g[tiled_index(k, 1, i, 2)] = buf1[(ipos2 * 5) + 2];
		g[tiled_index(k, 1, i, 3)] = buf1[(ipos2 * 5) + 3];
		g[tiled_index(k, 1, i, 4)] = buf1[(ipos2 * 5) + 4];
	
	}
	
}

// Packs g into buf.
__global__ void ex3_unpack_east(fp_type* buf1, fp_type* g) {

	// Check if thread is within range.
	int i = 2 + (blockIdx.x * blockDim.x) + threadIdx.x;
	int k = blockIdx.y;
	if (i <= nx_d + 1 && k <= nz_d - 1) {
		
		int ipos1 = k*nx_d + i - 2;
		int ipos2 = ipos1 + nx_d*nz_d;
	
		g[tiled_index(k, ny_d + 3, i, 0)] = buf1[(ipos1 * 5) + 0];
		g[tiled_index(k, ny_d + 3, i, 1)] = buf1[(ipos1 * 5) + 1];
		g[tiled_index(k, ny_d + 3, i, 2)] = buf1[(ipos1 * 5) + 2];
		g[tiled_index(k, ny_d + 3, i, 3)] = buf1[(ipos1 * 5) + 3];
		g[tiled_index(k, ny_d + 3, i, 4)] = buf1[(ipos1 * 5) + 4];

		g[tiled_index(k, ny_d + 2, i, 0)] = buf1[(ipos2 * 5) + 0];
		g[tiled_index(k, ny_d + 2, i, 1)] = buf1[(ipos2 * 5) + 1];
		g[tiled_index(k, ny_d + 2, i, 2)] = buf1[(ipos2 * 5) + 2];
		g[tiled_index(k, ny_d + 2, i, 3)] = buf1[(ipos2 * 5) + 3];
		g[tiled_index(k, ny_d + 2, i, 4)] = buf1[(ipos2 * 5) + 4];
	
	}	
	
}
