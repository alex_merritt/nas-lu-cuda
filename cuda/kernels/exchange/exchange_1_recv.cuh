// Some macros to increase readability.
#define ursdblock (problem_height * (isiz2 + 4) * (isiz1 + 4))

// Unpacks jrecv into g.
__global__ void ex1_unpack_north(fp_type* jrecv, fp_type* g, int k, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {

	// Check if thread is within range.
	int j = jst_d + (blockIdx.x * blockDim.x) + threadIdx.x;
	int z = blockIdx.y;
	if (j <= jend_d && k + z > 0 && k + z < isiz3 - 1) {
		
		int h_index = hyperplane_index(k + z, j, 1, 0, wave_offset_2d, wave_offset_3d, thread_map_d);
		int b_index = (z * (jend_d-jst_d+1) * 5) + ((j - jst_d) * 5);
		
		g[h_index + 0 * ursdblock] = jrecv[b_index + 0];
		g[h_index + 1 * ursdblock] = jrecv[b_index + 1];
		g[h_index + 2 * ursdblock] = jrecv[b_index + 2];
		g[h_index + 3 * ursdblock] = jrecv[b_index + 3];
		g[h_index + 4 * ursdblock] = jrecv[b_index + 4];																
	
	}
	
}

// Unpacks irecv into g.
__global__ void ex1_unpack_west(fp_type* irecv, fp_type* g, int k, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {

	// Check if thread is within range.
	int i = ist_d + (blockIdx.x * blockDim.x) + threadIdx.x;
	int z = blockIdx.y;
	if (i <= iend_d && k + z > 0 && k + z < isiz3 - 1) {
		
		int h_index = hyperplane_index(k + z, 1, i, 0, wave_offset_2d, wave_offset_3d, thread_map_d);
		int b_index = (z * (iend_d-ist_d+1) * 5) + ((i - ist_d) * 5);
		
		g[h_index + 0 * ursdblock] = irecv[b_index + 0];
		g[h_index + 1 * ursdblock] = irecv[b_index + 1];
		g[h_index + 2 * ursdblock] = irecv[b_index + 2];
		g[h_index + 3 * ursdblock] = irecv[b_index + 3];
		g[h_index + 4 * ursdblock] = irecv[b_index + 4];																
	
	}
	
}

// Unpacks jrecv into g.
__global__ void ex1_unpack_south(fp_type* jrecv, fp_type* g, int k, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {

	// Check if thread is within range.
	int j = jst_d + (blockIdx.x * blockDim.x) + threadIdx.x;
	int z = blockIdx.y;
	if (j <= jend_d && k + z - (kblock - 1) > 0 && k + z - (kblock - 1) < isiz3 - 1) {
		
		int h_index = hyperplane_index(k + z - (kblock - 1), j, nx_d + 2, 0, wave_offset_2d, wave_offset_3d, thread_map_d);
		int b_index = (z * (jend_d-jst_d+1) * 5) + ((j - jst_d) * 5);
		
		g[h_index + 0 * ursdblock] = jrecv[b_index + 0];
		g[h_index + 1 * ursdblock] = jrecv[b_index + 1];
		g[h_index + 2 * ursdblock] = jrecv[b_index + 2];
		g[h_index + 3 * ursdblock] = jrecv[b_index + 3];
		g[h_index + 4 * ursdblock] = jrecv[b_index + 4];																
	
	}
	
}

// Unpacks irecv into g.
__global__ void ex1_unpack_east(fp_type* irecv, fp_type* g, int k, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {

	// Check if thread is within range.
	int i = ist_d + (blockIdx.x * blockDim.x) + threadIdx.x;
	int z = blockIdx.y;
	if (i <= iend_d && k + z - (kblock - 1) > 0 && k + z - (kblock - 1) < isiz3 - 1) {
		
		int h_index = hyperplane_index(k + z - (kblock - 1), ny_d + 2, i, 0, wave_offset_2d, wave_offset_3d, thread_map_d);
		int b_index = (z * (iend_d-ist_d+1) * 5) + ((i - ist_d) * 5);
		
		g[h_index + 0 * ursdblock] = irecv[b_index + 0];
		g[h_index + 1 * ursdblock] = irecv[b_index + 1];
		g[h_index + 2 * ursdblock] = irecv[b_index + 2];
		g[h_index + 3 * ursdblock] = irecv[b_index + 3];
		g[h_index + 4 * ursdblock] = irecv[b_index + 4];																

	}
	
}
