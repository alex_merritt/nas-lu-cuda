// Some macros to increase readability.
#define ursdblock (problem_height * (isiz2 + 4) * (isiz1 + 4))

// Packs g into buf.
__global__ void ex3_pack_south(fp_type* buf, fp_type* g) {

	// Check if thread is within range.
	int j = 2 + (blockIdx.x * blockDim.x) + threadIdx.x;
	int k = blockIdx.y;
	if (j <= ny_d + 1 && k <= nz_d - 1) {
		
		int ipos1 = k*ny_d + j - 2;
		int ipos2 = ipos1 + ny_d*nz_d;
				
		buf[(ipos1 * 5) + 0] = g[tiled_index(k, j, nx_d, 0)];
		buf[(ipos1 * 5) + 1] = g[tiled_index(k, j, nx_d, 1)];			
		buf[(ipos1 * 5) + 2] = g[tiled_index(k, j, nx_d, 2)];			
		buf[(ipos1 * 5) + 3] = g[tiled_index(k, j, nx_d, 3)];			
		buf[(ipos1 * 5) + 4] = g[tiled_index(k, j, nx_d, 4)];

		buf[(ipos2 * 5) + 0] = g[tiled_index(k, j, nx_d + 1, 0)];
		buf[(ipos2 * 5) + 1] = g[tiled_index(k, j, nx_d + 1, 1)];
		buf[(ipos2 * 5) + 2] = g[tiled_index(k, j, nx_d + 1, 2)];
		buf[(ipos2 * 5) + 3] = g[tiled_index(k, j, nx_d + 1, 3)];
		buf[(ipos2 * 5) + 4] = g[tiled_index(k, j, nx_d + 1, 4)];	

	}

}

// Packs g into buf.
__global__ void ex3_pack_north(fp_type* buf, fp_type* g) {

	// Check if thread is within range.
	int j = 2 + (blockIdx.x * blockDim.x) + threadIdx.x;
	int k = blockIdx.y;
	if (j <= ny_d + 1 && k <= nz_d - 1) {
		
		int ipos1 = k*ny_d + j - 2;
		int ipos2 = ipos1 + ny_d*nz_d;
		
		buf[(ipos1 * 5) + 0] = g[tiled_index(k, j, 3, 0)];
		buf[(ipos1 * 5) + 1] = g[tiled_index(k, j, 3, 1)];			
		buf[(ipos1 * 5) + 2] = g[tiled_index(k, j, 3, 2)];			
		buf[(ipos1 * 5) + 3] = g[tiled_index(k, j, 3, 3)];			
		buf[(ipos1 * 5) + 4] = g[tiled_index(k, j, 3, 4)];

		buf[(ipos2 * 5) + 0] = g[tiled_index(k, j, 2, 0)];
		buf[(ipos2 * 5) + 1] = g[tiled_index(k, j, 2, 1)];
		buf[(ipos2 * 5) + 2] = g[tiled_index(k, j, 2, 2)];
		buf[(ipos2 * 5) + 3] = g[tiled_index(k, j, 2, 3)];
		buf[(ipos2 * 5) + 4] = g[tiled_index(k, j, 2, 4)];
	
	}

}

// Packs g into buf.
__global__ void ex3_pack_east(fp_type* buf, fp_type* g) {

	// Check if thread is within range.
	int i = 2 + (blockIdx.x * blockDim.x) + threadIdx.x;
	int k = blockIdx.y;
	if (i <= nx_d + 1 && k <= nz_d - 1) {
		
		int ipos1 = k*nx_d + i - 2;
		int ipos2 = ipos1 + nx_d*nz_d;
		
		buf[(ipos1 * 5) + 0] = g[tiled_index(k, ny_d, i, 0)];
		buf[(ipos1 * 5) + 1] = g[tiled_index(k, ny_d, i, 1)];			
		buf[(ipos1 * 5) + 2] = g[tiled_index(k, ny_d, i, 2)];			
		buf[(ipos1 * 5) + 3] = g[tiled_index(k, ny_d, i, 3)];			
		buf[(ipos1 * 5) + 4] = g[tiled_index(k, ny_d, i, 4)];

		buf[(ipos2 * 5) + 0] = g[tiled_index(k, ny_d + 1, i, 0)];
		buf[(ipos2 * 5) + 1] = g[tiled_index(k, ny_d + 1, i, 1)];
		buf[(ipos2 * 5) + 2] = g[tiled_index(k, ny_d + 1, i, 2)];
		buf[(ipos2 * 5) + 3] = g[tiled_index(k, ny_d + 1, i, 3)];
		buf[(ipos2 * 5) + 4] = g[tiled_index(k, ny_d + 1, i, 4)];
	
	}

}

// Packs g into buf.
__global__ void ex3_pack_west(fp_type* buf, fp_type* g) {

	// Check if thread is within range.
	int i = 2 + (blockIdx.x * blockDim.x) + threadIdx.x;
	int k = blockIdx.y;
	if (i <= nx_d + 1 && k <= nz_d - 1) {
			
		int ipos1 = k*nx_d + i - 2;
		int ipos2 = ipos1 + nx_d*nz_d;
		
		buf[(ipos1 * 5) + 0] = g[tiled_index(k, 3, i, 0)];
		buf[(ipos1 * 5) + 1] = g[tiled_index(k, 3, i, 1)];			
		buf[(ipos1 * 5) + 2] = g[tiled_index(k, 3, i, 2)];			
		buf[(ipos1 * 5) + 3] = g[tiled_index(k, 3, i, 3)];			
		buf[(ipos1 * 5) + 4] = g[tiled_index(k, 3, i, 4)];

		buf[(ipos2 * 5) + 0] = g[tiled_index(k, 2, i, 0)];
		buf[(ipos2 * 5) + 1] = g[tiled_index(k, 2, i, 1)];
		buf[(ipos2 * 5) + 2] = g[tiled_index(k, 2, i, 2)];
		buf[(ipos2 * 5) + 3] = g[tiled_index(k, 2, i, 3)];
		buf[(ipos2 * 5) + 4] = g[tiled_index(k, 2, i, 4)];
	
	}	

}
