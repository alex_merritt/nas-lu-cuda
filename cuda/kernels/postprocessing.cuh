/**
 * CUDA kernel for post-processing step of iteration loop.
 * Updates the variables.
 */
__global__ void postprocessing_kernel(fp_type* u, fp_type* rsd) {

	int i, j, k;
		
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);
	
	// Check i and j are in the correct range.
	if (i >= ist_d && i <= iend_d && j >= jst_d && j <= jend_d && k >=1 && k <= nz_d - 2) {
	
		// Calculate index and offset.
		int t_index = tiled_index(k, j, i, 0);
		int t_offset = (rhsgrid_x * rhsgrid_y) * (rhsblock_x * rhsblock_y * isiz3);	
	
		u[t_index + 0 * t_offset] += tmp_d * rsd[t_index + 0 * t_offset];
		u[t_index + 1 * t_offset] += tmp_d * rsd[t_index + 1 * t_offset];
		u[t_index + 2 * t_offset] += tmp_d * rsd[t_index + 2 * t_offset];
		u[t_index + 3 * t_offset] += tmp_d * rsd[t_index + 3 * t_offset];
		u[t_index + 4 * t_offset] += tmp_d * rsd[t_index + 4 * t_offset];								

	}
			
}
