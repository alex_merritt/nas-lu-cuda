// Define a variadic macro for debug statement.
#ifdef EMU
	#define debug(format, ...) printf(format, ## __VA_ARGS__)
#else
	#define debug(format, ...) // An empty line.
#endif
