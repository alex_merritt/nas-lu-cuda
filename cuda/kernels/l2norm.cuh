/**
 * CUDA kernel for calculating l2norm.
 * Currently calculated serially on 5 threads -- pretty awful.
 */
__global__ void l2norm_kernel(fp_type* rsd, fp_type* sum) {

	/**
	 * Local variables.
	 */
	int i, j, k, m;
	m = blockIdx.x;
	
	fp_type lsum = 0.0e+00;
	
	// Compute the sum for this m.
	for (k = 1; k <= nz_d -2; k++) {
		for (j = jst_d; j <= jend_d; j++) {
			for (i = ist_d; i <= iend_d; i++) {
			
				fp_type v = rsd[tiled_index(k, j, i, m)];
				lsum += v * v;
				
			}
		}
	}
	
	sum[m] = lsum;

}
