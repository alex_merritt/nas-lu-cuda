/**
 * Shift from flat to hyperplane layout.
 */
__global__ void flat_to_hyperplane_kernel(fp_type* flat_input, fp_type* hyperplane_output, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {
	
	int i, j;	
	
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = (blockIdx.y * rhsblock_y) + threadIdx.y;
	
	// Check we are within the right bounds for i and j.
	if (i < (isiz1 + 4) && j < (isiz2 + 4)) {
	
		// Iterate over blocks from level 0 to nz_d / blockDim.z.
		int k = 0;
		for (k = 0; k < nz_d; k++) {

			// For each thread, copy across each of its angles.	
			int m = 0;
			for (m = 0; m < 5; m++) {
				hyperplane_output[hyperplane_index(k, j, i, m, wave_offset_2d, wave_offset_3d, thread_map_d)] = flat_input[flat_index(k, j, i, m)];
			}
		
		}
		
	}

}

/**
 * Shift from hyperplane to flat layout.
 */
__global__ void hyperplane_to_flat_kernel(fp_type* hyperplane_input, fp_type* flat_output, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {

	int i, j;
		
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = (blockIdx.y * rhsblock_y) + threadIdx.y;
	
	// Check we are within the right bounds for i and j.
	if (i < (isiz1 + 4) && j < (isiz2 + 4)) {	
	
		// Iterate over blocks from level 0 to nz_d
		int k = 0;
		for (k = 0; k < nz_d; k++) {

			// For each thread, copy across each of its angles.	
			int m = 0;
			for (m = 0; m < 5; m++) {
				flat_output[flat_index(k, j, i, m)] = hyperplane_input[hyperplane_index(k, j, i, m, wave_offset_2d, wave_offset_3d, thread_map_d)];
			}
		
		}
		
	}

}

/**
 * Shift from flat to tiled layout.
 */
__global__ void flat_to_tiled_kernel(fp_type* flat_input, fp_type* tiled_output) {

	int i, j;
		
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = (blockIdx.y * rhsblock_y) + threadIdx.y;
	
	// Check we are within the right bounds for i and j.
	if (i < (isiz1 + 4) && j < (isiz2 + 4)) {
	
		// Iterate over blocks from level 0 to nz_d / blockDim.z.
		int k = 0;
		for (k = 0; k < nz_d; k++) {

			// For each thread, copy across each of its angles.	
			int m = 0;
			for (m = 0; m < 5; m++) {
				tiled_output[tiled_index(k, j, i, m)] = flat_input[flat_index(k, j, i, m)];
			}
		
		}
	
	}

}

/**
 * Shift from tiled to flat layout.
 */
__global__ void tiled_to_flat_kernel(fp_type* tiled_input, fp_type* flat_output) {

	int i, j;
		
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = (blockIdx.y * rhsblock_y) + threadIdx.y;
	
	// Check we are within the right bounds for i and j.
	if (i < (isiz1 + 4) && j < (isiz2 + 4)) {	
	
		// Iterate over blocks from level 0 to nz_d
		int k = 0;
		for (k = 0; k < nz_d; k++) {

			// For each thread, copy across each of its angles.	
			int m = 0;
			for (m = 0; m < 5; m++) {
				flat_output[flat_index(k, j, i, m)] = tiled_input[tiled_index(k, j, i, m)];
			}
		
		}
		
	}

}

/**
 * Shift from tiled to hyperplane layout.
 */
__global__ void tiled_to_hyperplane_kernel(fp_type* tiled_input, fp_type* hyperplane_output, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {

	int i, j, k;
		
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);
	
	// Check we are within the right bounds for i and j.
	if (i < (isiz1 + 4) && j < (isiz2 + 4)) {

		// Hyperplane index and offset.
		int h_index = hyperplane_index(k, j, i, 0, wave_offset_2d, wave_offset_3d, thread_map_d);
		int h_offset = problem_height * (isiz2 + 4) * (isiz1 + 4);

		// Tiled index and offset.		
		int t_index = tiled_index(k, j, i, 0);
		int t_offset = (rhsgrid_x * rhsgrid_y) * (rhsblock_x * rhsblock_y * isiz3);
		
		hyperplane_output[h_index + 0 * h_offset] = tiled_input[t_index + 0 * t_offset];
		hyperplane_output[h_index + 1 * h_offset] = tiled_input[t_index + 1 * t_offset];
		hyperplane_output[h_index + 2 * h_offset] = tiled_input[t_index + 2 * t_offset];
		hyperplane_output[h_index + 3 * h_offset] = tiled_input[t_index + 3 * t_offset];
		hyperplane_output[h_index + 4 * h_offset] = tiled_input[t_index + 4 * t_offset];						
	
	}

}

/**
 * Shift from hyperplane to tiled layout.
 */
__global__ void hyperplane_to_tiled_kernel(fp_type* hyperplane_input, fp_type* tiled_output, int* wave_offset_2d, int* wave_offset_3d, int* thread_map_d) {

	int i, j, k;
		
	// Calculate i and j values.
	i = (blockIdx.x * rhsblock_x) + threadIdx.x;
	j = ((blockIdx.y % rhsgrid_y) * rhsblock_y) + threadIdx.y;
	k = (blockIdx.y / rhsgrid_y);
	
	// Check we are within the right bounds for i and j.
	if (i < (isiz1 + 4) && j < (isiz2 + 4)) {
	
		// Hyperplane index and offset.
		int h_index = hyperplane_index(k, j, i, 0, wave_offset_2d, wave_offset_3d, thread_map_d);
		int h_offset = problem_height * (isiz2 + 4) * (isiz1 + 4);

		// Tiled index and offset.		
		int t_index = tiled_index(k, j, i, 0);
		int t_offset = (rhsgrid_x * rhsgrid_y) * (rhsblock_x * rhsblock_y * isiz3);	

		tiled_output[t_index + 0 * t_offset] = hyperplane_input[h_index + 0 * h_offset];
		tiled_output[t_index + 1 * t_offset] = hyperplane_input[h_index + 1 * h_offset];
		tiled_output[t_index + 2 * t_offset] = hyperplane_input[h_index + 2 * h_offset];
		tiled_output[t_index + 3 * t_offset] = hyperplane_input[h_index + 3 * h_offset];
		tiled_output[t_index + 4 * t_offset] = hyperplane_input[h_index + 4 * h_offset];						

	}

}
