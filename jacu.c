// C port of NPB3.2
// subroutine jacu(k)
#include "applu.h"

/**
 * Compute the upper triangular part of the jacobian matrix.
 */
void jacu(int starting_k) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	fp_type r43;
	fp_type c1345;
	fp_type c34;
	fp_type tmp1, tmp2, tmp3;
	
	// Constants.
	const fp_type c1 = c1_def;
	const fp_type c2 = c2_def;
	const fp_type c3 = c3_def;
	const fp_type c4 = c4_def;
	const fp_type c5 = c5_def;
	
	const fp_type fpzero = 0.0e+00;
	const fp_type fphalf = 0.50e+00;
	const fp_type fpone = 1.0e+00;
	const fp_type fptwo = 2.0e+00;
	const fp_type fpthree = 3.0e+00;
	const fp_type fpfour = 4.0e+00;	
	
	r43 = ( fpfour / fpthree );
	c1345 = c1 * c3 * c4 * c5;
	c34 = c3 * c4;

	for (k = starting_k; k > starting_k - kblock; k--) {	
	
		int level = k % kblock;	
	
		for (j = 0; j < ny + 4; j++) {
			for (i = 0; i < nx + 4; i++) {
			
				if (k >= 1 && k <= nz - 2
					&& j >= jst && j <= jend
					&& i >= ist && i <= iend) {				
		
					// Form the block diagonal.
					// (ie jacu_d).		
					tmp1 = fpone / u[k][j][i][0];
					tmp2 = tmp1 * tmp1;
					tmp3 = tmp1 * tmp2;
		
					d[level][j][i][0][0] = fpone
									+ dt * fptwo * (    tx1 * dx1
														+ ty1 * dy1
														+ tz1 * dz1 );
					d[level][j][i][1][0] = fpzero;
					d[level][j][i][2][0] = fpzero;
					d[level][j][i][3][0] = fpzero;
					d[level][j][i][4][0] = fpzero;						

					d[level][j][i][0][1] = dt * fptwo
									* (   tx1 * ( - r43 * c34 * tmp2 * u[k][j][i][1] )
										+ ty1 * ( -       c34 * tmp2 * u[k][j][i][1] )
										+ tz1 * ( -       c34 * tmp2 * u[k][j][i][1] ) );
					d[level][j][i][1][1] = fpone
									+ dt * fptwo
									* (   tx1 * r43 * c34 * tmp1
										+ ty1 *       c34 * tmp1
										+ tz1 *       c34 * tmp1 )
									+ dt * fptwo * (    tx1 * dx2
														+ ty1 * dy2
														+ tz1 * dz2 );
					d[level][j][i][2][1] = fpzero;
					d[level][j][i][3][1] = fpzero;
					d[level][j][i][4][1] = fpzero;
											
					d[level][j][i][0][2] = dt * fptwo
									* (   tx1 * ( -       c34 * tmp2 * u[k][j][i][2] )
										+ ty1 * ( - r43 * c34 * tmp2 * u[k][j][i][2] )
										+ tz1 * ( -       c34 * tmp2 * u[k][j][i][2] ) );
					d[level][j][i][1][2] = fpzero;
					d[level][j][i][2][2] = fpone
									+ dt * fptwo
									* (   tx1 *       c34 * tmp1
										+ ty1 * r43 * c34 * tmp1
										+ tz1 *       c34 * tmp1 )
									+ dt * fptwo * (    tx1 * dx3
														+ ty1 * dy3
														+ tz1 * dz3 );
					d[level][j][i][3][2] = fpzero;
					d[level][j][i][4][2] = fpzero;

					d[level][j][i][0][3] = dt * fptwo
									* (   tx1 * ( -       c34 * tmp2 * u[k][j][i][3] )
										+ ty1 * ( -       c34 * tmp2 * u[k][j][i][3] )
										+ tz1 * ( - r43 * c34 * tmp2 * u[k][j][i][3] ) );
					d[level][j][i][1][3] = fpzero;
					d[level][j][i][2][3] = fpzero;
					d[level][j][i][3][3] = fpone
									+ dt * fptwo
									* (   tx1 *       c34 * tmp1
										+ ty1 *       c34 * tmp1
										+ tz1 * r43 * c34 * tmp1 )
									+ dt * fptwo * (    tx1 * dx4
														+ ty1 * dy4
														+ tz1 * dz4 );
					d[level][j][i][4][3] = fpzero;
		
					d[level][j][i][0][4] = dt * fptwo
									 *  ( tx1 * ( - ( r43 * c34 - c1345 ) * tmp3 * ( u[k][j][i][1] * u[k][j][i][1] )
										 		  - (       c34 - c1345 ) * tmp3 * ( u[k][j][i][2] * u[k][j][i][2] )
												  - (       c34 - c1345 ) * tmp3 * ( u[k][j][i][3] * u[k][j][i][3] )										
												  - (             c1345 ) * tmp2 * ( u[k][j][i][4] ) )
										+ ty1 * ( - (       c34 - c1345 ) * tmp3 * ( u[k][j][i][1] * u[k][j][i][1] )
												  - ( r43 * c34 - c1345 ) * tmp3 * ( u[k][j][i][2] * u[k][j][i][2] )
												  - (       c34 - c1345 ) * tmp3 * ( u[k][j][i][3] * u[k][j][i][3] )
												  - (             c1345 ) * tmp2 * ( u[k][j][i][4] ) )
										+ tz1 * ( - (       c34 - c1345 ) * tmp3 * ( u[k][j][i][1] * u[k][j][i][1] )
												  - (       c34 - c1345 ) * tmp3 * ( u[k][j][i][2] * u[k][j][i][2] )
												  - ( r43 * c34 - c1345 ) * tmp3 * ( u[k][j][i][3] * u[k][j][i][3] )
												  - (             c1345 ) * tmp2 * u[k][j][i][4] ) );

					d[level][j][i][1][4] = dt * fptwo
									* (   tx1 * ( r43 * c34 - c1345 ) * tmp2 * u[k][j][i][1]
										+ ty1 * (       c34 - c1345 ) * tmp2 * u[k][j][i][1]
										+ tz1 * (       c34 - c1345 ) * tmp2 * u[k][j][i][1] );
					d[level][j][i][2][4] = dt * fptwo
									* (   tx1 * (       c34 - c1345 ) * tmp2 * u[k][j][i][2]
										+ ty1 * ( r43 * c34 - c1345 ) * tmp2 * u[k][j][i][2]
										+ tz1 * (       c34 - c1345 ) * tmp2 * u[k][j][i][2] );
					d[level][j][i][3][4] = dt * fptwo
									* (   tx1 * (       c34 - c1345 ) * tmp2 * u[k][j][i][3]
										+ ty1 * (       c34 - c1345 ) * tmp2 * u[k][j][i][3]
										+ tz1 * ( r43 * c34 - c1345 ) * tmp2 * u[k][j][i][3] );
					d[level][j][i][4][4] = fpone
									+ dt * fptwo * (    tx1 * c1345 * tmp1
														+ ty1 * c1345 * tmp1
														+ tz1 * c1345 * tmp1 )
									+ dt * fptwo * (    tx1 * dx5
														+ ty1 * dy5
														+ tz1 * dz5 );
												
					// Form the first block sub-diagonal.
					// (ie jacu_a).
					tmp1 = fpone / u[k][j][i+1][0];
					tmp2 = tmp1 * tmp1;
					tmp3 = tmp1 * tmp2;
			
					a[level][j][i][0][0] = -dt * tx1 * dx1;
					a[level][j][i][1][0] = dt * tx2;
					a[level][j][i][2][0] = fpzero;
					a[level][j][i][3][0] = fpzero;
					a[level][j][i][4][0] = fpzero;						

					a[level][j][i][0][1] = dt * tx2
									* ( - ( u[k][j][i+1][1] * tmp1 ) * ( u[k][j][i+1][1] * tmp1)
									+ c2 * fphalf * (   u[k][j][i+1][1] * u[k][j][i+1][1]
														+ u[k][j][i+1][2] * u[k][j][i+1][2]
														+ u[k][j][i+1][3] * u[k][j][i+1][3] ) * tmp2 )
									- dt * tx1 * ( - r43 * c34 * tmp2 * u[k][j][i+1][1] );
					a[level][j][i][1][1] = dt * tx2
									* ( ( fptwo - c2 ) * ( u[k][j][i+1][1] * tmp1 ) )
									- dt * tx1 * ( r43 * c34 * tmp1 )
									- dt * tx1 * dx2;			
					a[level][j][i][2][1] = dt * tx2
									* ( - c2 * ( u[k][j][i+1][2] * tmp1 ) );
					a[level][j][i][3][1] = dt * tx2
									* ( - c2 * ( u[k][j][i+1][3] * tmp1 ) );
					a[level][j][i][4][1] = dt * tx2 * c2;


					a[level][j][i][0][2] = dt * tx2
									* ( - ( u[k][j][i+1][1] * u[k][j][i+1][2] ) * tmp2 )
									- dt * tx1 * ( - c34 * tmp2 * u[k][j][i+1][2] );
					a[level][j][i][1][2] = dt * tx2 * ( u[k][j][i+1][2] * tmp1 );
					a[level][j][i][2][2] = dt * tx2 * ( u[k][j][i+1][1] * tmp1 )
									-dt * tx1 * ( c34 * tmp1 )
									-dt * tx1 * dx3;
					a[level][j][i][3][2] = fpzero;
					a[level][j][i][4][2] = fpzero;			

					a[level][j][i][0][3] = dt * tx2
									* ( - ( u[k][j][i+1][1] * u[k][j][i+1][3] ) * tmp2 )
									- dt * tx1 * ( - c34 * tmp2 * u[k][j][i+1][3] );
					a[level][j][i][1][3] = dt * tx2 * ( u[k][j][i+1][3] * tmp1 );
					a[level][j][i][2][3] = fpzero;
					a[level][j][i][3][3] = dt * tx2 * ( u[k][j][i+1][1] * tmp1 )
									-dt * tx1 * ( c34 * tmp1 )
									-dt * tx1 * dx4;
					a[level][j][i][4][3] = fpzero;
			
					a[level][j][i][0][4] = dt * tx2
									* ( ( c2 * (  u[k][j][i+1][1] * u[k][j][i+1][1]
												+ u[k][j][i+1][2] * u[k][j][i+1][2]
												+ u[k][j][i+1][3] * u[k][j][i+1][3] ) * tmp2
										- c1 * ( u[k][j][i+1][4] * tmp1 ) )
									* ( u[k][j][i+1][1] * tmp1 ) )
									- dt * tx1
									* ( - ( r43 * c34 - c1345 ) * tmp3 * ( u[k][j][i+1][1] * u[k][j][i+1][1] )
										- (       c34 - c1345 ) * tmp3 * ( u[k][j][i+1][2] * u[k][j][i+1][2] )
										- (       c34 - c1345 ) * tmp3 * ( u[k][j][i+1][3] * u[k][j][i+1][3] )
										- c1345 * tmp2 * u[k][j][i+1][4] );
					a[level][j][i][1][4] = dt * tx2
									* ( c1 * ( u[k][j][i+1][4] * tmp1 )
									- fphalf * c2
									* ( ( fpthree * u[k][j][i+1][1] * u[k][j][i+1][1]
										+ u[k][j][i+1][2] * u[k][j][i+1][2]
										+ u[k][j][i+1][3] * u[k][j][i+1][3] ) * tmp2 ) )
									- dt * tx1
									* ( r43 * c34 - c1345 ) * tmp2 * u[k][j][i+1][1];
					a[level][j][i][2][4] = dt * tx2
									* ( - c2 * ( u[k][j][i+1][2] * u[k][j][i+1][1] ) * tmp2 )
									-dt * tx1
									* (  c34 - c1345 ) * tmp2 * u[k][j][i+1][2];
					a[level][j][i][3][4] = dt * tx2
									* ( - c2 * ( u[k][j][i+1][3] * u[k][j][i+1][1] ) * tmp2 )
									- dt * tx1
									* ( c34 - c1345 ) * tmp2 * u[k][j][i+1][3];
					a[level][j][i][4][4] = dt * tx2
									* ( c1 * ( u[k][j][i+1][1] * tmp1 ) )
									- dt * tx1 * c1345 * tmp1
									- dt * tx1 * dx5;
							
					// Form the second block sub-diagonal.
					tmp1 = fpone / u[k][j+1][i][0];
					tmp2 = tmp1 * tmp1;
					tmp3 = tmp1 * tmp2;
			
					b[level][j][i][0][0] = -dt * ty1 * dy1;
					b[level][j][i][1][0] = fpzero;
					b[level][j][i][2][0] = dt * ty2;
					b[level][j][i][3][0] = fpzero;
					b[level][j][i][4][0] = fpzero;		
			
					b[level][j][i][0][1] = dt * ty2
									* ( - ( u[k][j+1][i][1] * u[k][j+1][i][2] ) * tmp2 )
									- dt * ty1 * ( - c34 * tmp2 * u[k][j+1][i][1] );
					b[level][j][i][1][1] = dt * ty2 * ( u[k][j+1][i][2] * tmp1 )
									- dt * ty1 * ( c34 * tmp1 )
									- dt * ty1 * dy2;
					b[level][j][i][2][1] = dt * ty2 * ( u[k][j+1][i][1] * tmp1 );
					b[level][j][i][3][1] = fpzero;
					b[level][j][i][4][1] = fpzero;
			
					b[level][j][i][0][2] = dt * ty2
									* ( - ( u[k][j+1][i][2] * tmp1 ) * ( u[k][j+1][i][2] * tmp1 )
									+ fphalf * c2 * ( ( u[k][j+1][i][1] * u[k][j+1][i][1]
														+ u[k][j+1][i][2] * u[k][j+1][i][2]
														+ u[k][j+1][i][3] * u[k][j+1][i][3] )
														* tmp2 ) )
									- dt * ty1 * ( - r43 * c34 * tmp2 * u[k][j+1][i][2] );
					b[level][j][i][1][2] = dt * ty2
									* ( - c2 * ( u[k][j+1][i][1] * tmp1 ) );
					b[level][j][i][2][2] = dt * ty2 * ( ( fptwo - c2 )
									* ( u[k][j+1][i][2] * tmp1 ) )
									- dt * ty1 * ( r43 * c34 * tmp1 )
									- dt * ty1 * dy3;
					b[level][j][i][3][2] = dt * ty2
									* ( - c2 * ( u[k][j+1][i][3] * tmp1 ) );
					b[level][j][i][4][2] = dt * ty2 * c2;

					b[level][j][i][0][3] = dt * ty2
									* ( - ( u[k][j+1][i][2] * u[k][j+1][i][3] ) * tmp2 )
									- dt * ty1 * ( - c34 * tmp2 * u[k][j+1][i][3] );
					b[level][j][i][1][3] = fpzero;
					b[level][j][i][2][3] = dt * ty2 * ( u[k][j+1][i][3] * tmp1 );
					b[level][j][i][3][3] = dt * ty2 * ( u[k][j+1][i][2] * tmp1 )
									- dt * ty1 * ( c34 * tmp1 )
									- dt * ty1 * dy4;
					b[level][j][i][4][3] = fpzero;
				       
				       
					b[level][j][i][0][4] = dt * ty2
									* ( ( c2 * (  u[k][j+1][i][1] * u[k][j+1][i][1]
												+ u[k][j+1][i][2] * u[k][j+1][i][2]
												+ u[k][j+1][i][3] * u[k][j+1][i][3] ) * tmp2
										- c1 * (  u[k][j+1][i][4] * tmp1 ) )
									* ( u[k][j+1][i][2] * tmp1 ) )
									- dt * ty1
									* ( - (       c34 - c1345 ) * tmp3 * ( u[k][j+1][i][1] * u[k][j+1][i][1] )
										- ( r43 * c34 - c1345 ) * tmp3 * ( u[k][j+1][i][2] * u[k][j+1][i][2] )							
										- (       c34 - c1345 ) * tmp3 * ( u[k][j+1][i][3] * u[k][j+1][i][3] )
										- c1345 * tmp2 * u[k][j+1][i][4] );
					b[level][j][i][1][4] = dt * ty2
									* ( - c2 * ( u[k][j+1][i][1] * u[k][j+1][i][2] ) * tmp2 )
									- dt * ty1
									* ( c34 - c1345 ) * tmp2 * u[k][j+1][i][1];
					b[level][j][i][2][4] = dt * ty2
									* ( c1 * ( u[k][j+1][i][4] * tmp1 )
									- fphalf * c2
									* ( ( u[k][j+1][i][1] * u[k][j+1][i][1]
										+ fpthree * u[k][j+1][i][2] * u[k][j+1][i][2]
										+ u[k][j+1][i][3] * u[k][j+1][i][3] ) * tmp2 ) )
									- dt * ty1
									* ( r43 * c34 - c1345 ) * tmp2 * u[k][j+1][i][2];
					b[level][j][i][3][4] = dt * ty2
									* ( - c2 * ( u[k][j+1][i][2] * u[k][j+1][i][3] ) * tmp2 )
									- dt * ty1 * ( c34 - c1345 ) * tmp2 * u[k][j+1][i][3];
					b[level][j][i][4][4] = dt * ty2
									* ( c1 * ( u[k][j+1][i][2] * tmp1 ) )
									- dt * ty1 * c1345 * tmp1
									- dt * ty1 * dy5;


					// Form the third block sub-diagonal.
					//(ie jacu_c).
					tmp1 = fpone / u[k+1][j][i][0];
					tmp2 = tmp1 * tmp1;
					tmp3 = tmp1 * tmp2;
			
					c[level][j][i][0][0] = -dt * tz1 * dz1;
					c[level][j][i][1][0] = fpzero;
					c[level][j][i][2][0] = fpzero;
					c[level][j][i][3][0] = dt * tz2;
					c[level][j][i][4][0] = fpzero;
			
					c[level][j][i][0][1] = dt * tz2
									* ( - ( u[k+1][j][i][1] * u[k+1][j][i][3] ) * tmp2 )
									- dt * tz1 * ( - c34 * tmp2 * u[k+1][j][i][1] );
					c[level][j][i][1][1] = dt * tz2 * ( u[k+1][j][i][3] * tmp1 )
									- dt * tz1 * c34 * tmp1
									- dt * tz1 * dz2;
					c[level][j][i][2][1] = fpzero;			
					c[level][j][i][3][1] = dt * tz2 * ( u[k+1][j][i][1] * tmp1 );
					c[level][j][i][4][1] = fpzero;
			
					c[level][j][i][0][2] = dt * tz2
									* ( - ( u[k+1][j][i][2] * u[k+1][j][i][3] ) * tmp2 )
									- dt * tz1 * ( - c34 * tmp2 * u[k+1][j][i][2] );
					c[level][j][i][1][2] = fpzero;
					c[level][j][i][2][2] = dt * tz2 * ( u[k+1][j][i][3]  * tmp1 )
									- dt * tz1 * ( c34 * tmp1 )
									- dt * tz1 * dz3;
					c[level][j][i][3][2] = dt * tz2 * ( u[k+1][j][i][2] * tmp1 );
					c[level][j][i][4][2] = fpzero;

					c[level][j][i][0][3] = dt * tz2
									* ( - ( u[k+1][j][i][3] * tmp1 ) * ( u[k+1][j][i][3] * tmp1 )
										+ fphalf * c2
										* ( ( u[k+1][j][i][1] * u[k+1][j][i][1]
											+ u[k+1][j][i][2] * u[k+1][j][i][2]
											+ u[k+1][j][i][3] * u[k+1][j][i][3] ) * tmp2 ) )
									-dt * tz1 * ( - r43 * c34 * tmp2 * u[k+1][j][i][3] );
					c[level][j][i][1][3] = dt * tz2
									* ( - c2 * ( u[k+1][j][i][1] * tmp1 ) );
					c[level][j][i][2][3] = dt * tz2
									* ( - c2 * ( u[k+1][j][i][2] * tmp1 ) );
					c[level][j][i][3][3] = dt * tz2 * ( fptwo - c2 )
									* ( u[k+1][j][i][3] * tmp1 )
									- dt * tz1 * ( r43 * c34 * tmp1 )
									- dt * tz1 * dz4;
					c[level][j][i][4][3] = dt * tz2 * c2;
			
					c[level][j][i][0][4] = dt * tz2
									* ( ( c2 * (  u[k+1][j][i][1] * u[k+1][j][i][1]
												+ u[k+1][j][i][2] * u[k+1][j][i][2]
												+ u[k+1][j][i][3] * u[k+1][j][i][3] ) * tmp2
										- c1 * ( u[k+1][j][i][4] * tmp1 ) )
											 * ( u[k+1][j][i][3] * tmp1 ) )
										- dt * tz1
										* ( - (       c34 - c1345 ) * tmp3 * ( u[k+1][j][i][1] * u[k+1][j][i][1] )
											- (       c34 - c1345 ) * tmp3 * ( u[k+1][j][i][2] * u[k+1][j][i][2] )
											- ( r43 * c34 - c1345 ) * tmp3 * ( u[k+1][j][i][3] * u[k+1][j][i][3] )
											- c1345 * tmp2 * u[k+1][j][i][4] );
					c[level][j][i][1][4] = dt * tz2
									* ( - c2 * ( u[k+1][j][i][1] * u[k+1][j][i][3] ) * tmp2 )
									- dt * tz1 * ( c34 - c1345 ) * tmp2 * u[k+1][j][i][1];
					c[level][j][i][2][4] = dt * tz2
									* ( - c2 * ( u[k+1][j][i][2] * u[k+1][j][i][3] ) * tmp2 )
									- dt * tz1 * ( c34 - c1345 ) * tmp2 * u[k+1][j][i][2];
					c[level][j][i][3][4] = dt * tz2
									* ( c1 * ( u[k+1][j][i][4] * tmp1 )
									- fphalf * c2
									* ( ( u[k+1][j][i][1] * u[k+1][j][i][1]
										+ u[k+1][j][i][2] * u[k+1][j][i][2]
										+ fpthree * u[k+1][j][i][3] * u[k+1][j][i][3] ) * tmp2 ) )
									- dt * tz1 * ( r43 * c34 - c1345 ) * tmp2 * u[k+1][j][i][3];
					c[level][j][i][4][4] = dt * tz2
									* ( c1 * ( u[k+1][j][i][3] * tmp1 ) )
									- dt * tz1 * c1345 * tmp1
									- dt * tz1 * dz5;
									
				}
			
			}
		
		}
		
	}

}
