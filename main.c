/**************************************************************************
*                                                                         *
*        N  A  S     P A R A L L E L     B E N C H M A R K S  3.2         *
*                                                                         *
*                                   L U                                   *
*                                                                         *
***************************************************************************
*                                                                         *
*    This benchmark is part of the NAS Parallel Benchmark 3.2 suite.      *
*    It is described in NAS Technical Reports 95-020 and 02-007           *
*                                                                         *
*    Permission to use, copy, distribute and modify this software         *
*    for any purpose with or without fee is hereby granted.  We           *
*    request, however, that all derived work reference the NAS            *
*    Parallel Benchmarks 3.2. This software is provided "as is"           *
*    without express or implied warranty.                                 *
*                                                                         *
*    Information on NPB 3.2, including the technical report, the          *
*    original specifications, source code, results and information        *
*    on how to submit new results, is available at:                       *
*                                                                         *
*           http://www.nas.nasa.gov/Software/NPB/                         *
*                                                                         *
*    Send comments or suggestions to  npb@nas.nasa.gov                    *
*                                                                         *
*          NAS Parallel Benchmarks Group                                  *
*          NASA Ames Research Center                                      *
*          Mail Stop: T27A-1                                              *
*          Moffett Field, CA   94035-1000                                 *
*                                                                         *
*          E-mail:  npb@nas.nasa.gov                                      *
*          Fax:     (650) 604-3957                                        *
*                                                                         *
***************************************************************************

* *************************************************************************
*                                                                         *
*  Authors: S. Weeratunga                                                 *
*           V. Venkatakrishnan                                            *
*           E. Barszcz                                                    *
*           M. Yarrow                                                     *
*                                                                         *
* *************************************************************************

* *************************************************************************
*                                                                         *
*   C Port: S.J. Pennycook                                                *
*           O.J  Perks                                                    *
*                                                                         *
* ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "applu.h"
#include "globals.h"
#include "alloc.h"
#include "mpinpb.h"
#include "timers.h"
#include "util.h"

/**
 * Driver for the performance evaluation of the solver for
 * five coupled parabolic/elliptic partial differential equations.
 */
int main(int argc, char* argv[]) {

	// Initialize communications.
	init_comm(argc, argv);
	timer_init(12);

	// Read if we are GPU or CPU.
	if (argc != 2) {
		printf("Usage: ./applu [--cpu or --gpu]\n");
		return -1;
	}
	#include "alloc.h"
	int running_on_cpu = 0;
	if (strcmp(argv[1], "--cpu") == 0) {
		running_on_cpu = 1;
	} else if (strcmp(argv[1], "--gpu") == 0) {
		running_on_cpu = 0;
	} else {
		printf("Usage: ./applu [--cpu or --gpu]\n");
		return -1;	
	}
		
	// Allocate memory for our arrays.
	u_flat = malloc( isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type) );
	rsd_flat = malloc( isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type) );
	frct_flat = malloc( isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type) );
	flux_flat = malloc( isiz3 * (isiz2 + 4) * (isiz1 + 4) * 5 * sizeof(fp_type) );			
	if (u_flat == NULL
		|| rsd_flat == NULL
		|| frct_flat == NULL
		|| flux_flat == NULL) {
		printf("alloc_ursd() failed.\n");
		exit(EXIT_FAILURE);
	}
	
	u = alloc_ursd(u_flat);
	rsd = alloc_ursd(rsd_flat);
	frct = alloc_ursd(frct_flat);
	flux = alloc_ursd(flux_flat);

	// Allocate memory for the a/b/c/d arrays if we are on the CPU.
	if (running_on_cpu) {
		a_flat = malloc( kblock * (isiz2 + 4) * (isiz1 + 4) * 5 * 5 * sizeof(fp_type) );
		b_flat = malloc( kblock * (isiz2 + 4) * (isiz1 + 4) * 5 * 5 * sizeof(fp_type) );
		c_flat = malloc( kblock * (isiz2 + 4) * (isiz1 + 4) * 5 * 5 * sizeof(fp_type) );
		d_flat = malloc( kblock * (isiz2 + 4) * (isiz1 + 4) * 5 * 5 * sizeof(fp_type) );	
		if (a_flat == NULL
			|| b_flat == NULL
			|| c_flat == NULL
			|| d_flat == NULL) {
			printf("alloc_abcd() failed.\n");
			exit(EXIT_FAILURE);
		}		

		a = alloc_abcd(a_flat);
		b = alloc_abcd(b_flat);
		c = alloc_abcd(c_flat);
		d = alloc_abcd(d_flat);
	}
	
	if (id == 0) {
		printf("\n");
		printf("===========================================\n");
	}
	
	// Read input data.
	read_input();
	
	// Set up processor grid.
	proc_grid();
	
	// Determine the neighbors.
	neighbors();
	
	// Set up sub-domain sizes - understand more later for per processor work.
	subdomain();
	
	if (id == 0) {
		printf("===========================================\n");		
	}
	
	// Allocate memory for the buffers, based on this subdomain size.
	buf_flat = malloc( 10 * isiz3 * isiz2 * sizeof(fp_type) );
	buf1_flat = malloc( 10 * isiz3 * isiz2 * sizeof(fp_type) );
	if (buf_flat == NULL
		|| buf1_flat == NULL) {
		printf("alloc_buffer() failed.\n");
		exit(EXIT_FAILURE);
	}

	buf  = alloc_buffer(buf_flat);
	buf1 = alloc_buffer(buf1_flat);	
	
	if (buf == NULL || buf1 == NULL) {
		printf("alloc_buffer() failed.\n");
		exit(EXIT_FAILURE);
	}
	
	// Allocate memory for MPI buffers.
	ibuf_flat = malloc( kblock * (iend - ist + 1) * 5 * sizeof(fp_type) );
	jbuf_flat = malloc( kblock * (jend - jst + 1) * 5 * sizeof(fp_type) );
	if (ibuf_flat == NULL
		|| jbuf_flat == NULL) {
		printf("alloc_ibuf/jbuf() failed.\n");
		exit(EXIT_FAILURE);
	}
	
	ibuf = alloc_ibuf(ibuf_flat);
	jbuf = alloc_jbuf(jbuf_flat);
	if (ibuf == NULL
		|| jbuf == NULL) {
		printf("alloc_ibuf/jbuf() failed.\n");
		exit(EXIT_FAILURE);
	}
		
	// Set up coefficients.
	setcoeff();
	
	// Set the masks required for comm.
	sethyper();
		
	// Set the boundary values for dependent variables.
	setbv();

	// Set the initial values for dependent variables.
	setiv();	
	
	// Compute the forcing term based on prescribed exact solution.
	erhs();
		
	// Perform one SSOR iteration to touch all data and program pages.
	if (running_on_cpu) {
		ssor(1);
	} else {
		ssor_gpu(1);
	}
	
	// Reset the boundary and initial values.
	setbv();
	setiv();
	
	// Perform the SSOR iterations - timed.
	if (running_on_cpu) {
		ssor(itmax);
	} else {
		ssor_gpu(itmax);
	}
	
	// Compute the solution error.
	error();
	
	// Compute the surface integral.
	pintgr();	
	
	// Verification test.
	if (id == 0) {
		
		verified = verify( rsdnm, errnm, frc, &class );
		mflops = ((float) itmax) * (1984.77 * ((float) nx0)
			* ((float) ny0)
			* ((float) nz0)
			-10923.3 * ((float) (nx0 + ny0 + nz0 ) / 3.0) * ((float) (nx0 + ny0 + nz0 ) / 3.0)
			+27770.9 * ((float) (nx0 + ny0 + nz0 ) / 3.0)
			-144010.0)
			/ (maxtime * 1000000.0);		

		printf(" itmax = %d\n", itmax);
		printf(" x = %d\n", nx0);
		printf(" y = %d\n", ny0);
		printf(" z = %d\n", nz0);
		printf("\n");
		
	}
	
	// Print timing results.
	int p;
	for (p = 0; p < num; p++) {
		if (id == p) {
			printf("\n");		
			printf(" Rank = %d\n", id);
			print_timers();																											
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	
	// Free memory for our arrays.
	free(u_flat);
	free(rsd_flat);
	free(frct_flat);
	free(flux_flat);
	free_ursd(u);
	free_ursd(rsd);
	free_ursd(frct);
	free_ursd(flux);
	
	if (running_on_cpu) {
		free(a_flat);
		free(b_flat);
		free(c_flat);
		free(d_flat);
		free_abcd(a);
		free_abcd(b);
		free_abcd(c);
		free_abcd(d);
	}
	
	free(buf_flat);
	free(buf1_flat);
	free_buffer(buf);
	free_buffer(buf1);
	
	free(ibuf_flat);
	free(jbuf_flat);
	free_ibuf(ibuf);
	free_jbuf(jbuf);
	
	timer_finalize();
	MPI_Finalize();
	return 0;

}
