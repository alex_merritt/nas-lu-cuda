/**
 * Kind of allows us to emulate the <float> and <double> templates
 * from C++.
 */
#ifndef __TYPES_H__
#define __TYPES_H__

	#ifdef SINGLE_PRECISION_LU
	typedef float fp_type;
	#define MPI_FP_TYPE MPI_FLOAT
	#endif

	#ifdef DOUBLE_PRECISION_LU
	typedef double fp_type;
	#define MPI_FP_TYPE MPI_DOUBLE
	#endif
	
#endif
