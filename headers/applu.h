// C port of NPB3.2
// applu.h

// size.h defines problem and decomposition sizes.
#include "size.h"
#include "types.h"

/**
 * isiz01,02,03 give maximum size.
 * ipr = 1 to print out verbose information.
 * omega = 2.0 is correct for all classes.
 * tolrsd is tolerance levels for steady state residuals.
 */
#define ipr_default 1
#define omega_default (1.20e+00)
#define tolrsd1_def (1.0e-08)
#define tolrsd2_def (1.0e-08)
#define tolrsd3_def (1.0e-08)
#define tolrsd4_def (1.0e-08)
#define tolrsd5_def (1.0e-08)
#define c1_def (1.40e+00)
#define c2_def (0.40e+00)
#define c3_def (1.00e-01)
#define c4_def (1.00e+00)
#define c5_def (1.40e+00)

/**
 * Grid.
 */
extern int nx, ny, nz;
extern int nx0, ny0, nz0;
extern int ipt, ist, iend;
extern int jpt, jst, jend;
extern int ii1, ii2;
extern int ji1, ji2;
extern int ki1, ki2;
extern fp_type dxi, deta, dzeta;
extern fp_type tx1, tx2, tx3;
extern fp_type ty1, ty2, ty3;
extern fp_type tz1, tz2, tz3;

/**
 * Dissipation.
 */
extern fp_type dx1, dx2, dx3, dx4, dx5;
extern fp_type dy1, dy2, dy3, dy4, dy5;
extern fp_type dz1, dz2, dz3, dz4, dz5;
extern fp_type dssp;

/**
 * Field variables and residuals.
 */
extern fp_type* u_flat;
extern fp_type* rsd_flat;
extern fp_type* frct_flat;
extern fp_type* flux_flat;

extern fp_type**** u;
extern fp_type**** rsd;
extern fp_type**** frct;
extern fp_type**** flux;

/**
 * Output control parameters.
 */
extern int ipr, inorm;

/**
 * Newton-raphson iteration control parameters.
 */
extern int itmax, invert;
extern fp_type dt, omega, tolrsd[5], rsdnm[5], errnm[5], frc, ttotal;

extern fp_type* a_flat;
extern fp_type* b_flat;
extern fp_type* c_flat;
extern fp_type* d_flat;
extern fp_type***** a;
extern fp_type***** b;
extern fp_type***** c;
extern fp_type***** d;

/**
 * Coefficients of the exact solution.
 */
extern fp_type ce[13][5];

/**
 * Multi-processor common blocks.
 */
extern int id, ndim, num, xdim, ydim, row, col;
extern int north, south, east, west;

#define from_s 1
#define from_n 2
#define from_e 3
#define from_w 4
#define npmax (isiz01 + isiz02)

extern int icommn[npmax+1], icomms[npmax+1], icomme[npmax+1], icommw[npmax+1];
extern fp_type *buf_flat, *buf1_flat;
extern fp_type **buf, **buf1;
extern fp_type *jbuf_flat, *ibuf_flat;
extern fp_type ***jbuf, ***ibuf;
extern fp_type maxtime;

// Function prototypes.
#include "functions.h"
