/**
 * Header for useful CUDA stuff.
 */
#ifndef __APPLU_CUDA_H__
#define __APPLU_CUDA_H__

	#include "types.h"

	typedef struct {
	
		// Solution arrays.
		fp_type* u;
		fp_type* rsd;
		fp_type* frct;
		fp_type* flux;
		
		// Array for l2norm reduction.
		fp_type* sum;
		
		// Rearrangement buffer.
		fp_type* rearrangement;
		
		// Lookup tables.
		int* columns;
		int* rows;
		int* wave2d_offsets;
		int* wave3d_offsets;
		int* thread_map;
		
		// Ex1 buffers.
		fp_type* ibuffer;
		fp_type* jbuffer;
		
		// Ex3 buffers.
		fp_type* buf;
		fp_type* buf1;
	
	} luBuffers;
	
	// Prototype functions.
	extern void allocate_buffers(luBuffers* buffers);
	extern void free_buffers(luBuffers* buffers);
	extern void prepare_lookup_tables(int* wave2d_offsets, int* wave3d_offsets, int* columns, int* rows, int* thread_map, luBuffers* buffers);
	extern void print_gpu_info();
	
	extern void flat_to_tiled(fp_type* buffer, fp_type* rearrangement, luBuffers* buffers);
	extern void tiled_to_flat(fp_type* buffer, fp_type* rearrangement, luBuffers* buffers);
	extern void tiled_to_hyperplane(fp_type* buffer, fp_type* rearrangement, luBuffers* buffers);
	extern void hyperplane_to_tiled(fp_type* buffer, fp_type* rearrangement, luBuffers* buffers);

#endif
