
/**
 * Shared header file for anything using MPI.
 */
#include <mpi.h>
extern int node, no_nodes, root, comm_setup, comm_solve, comm_rhs;

// Removed dp_type, since we know we are using MPI_DOUBLE everywhere.
