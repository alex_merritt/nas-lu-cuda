#ifndef __APPLU_FUNCTIONS__
#define __APPLU_FUNCTIONS__

extern void bcast_inputs();

extern void blts ( int ldmx, int ldmy, int ldmz,
			int nx, int ny, int nz, int starting_k,
			fp_type omega,
			fp_type**** v,
			fp_type***** ldz,
			fp_type***** ldy,
			fp_type***** ldx,
			fp_type***** d,			
			int ist, int iend, int jst, int jend,
			int nx0, int ny0, int ipt, int jpt);
			
extern void buts ( int ldmx, int ldmy, int ldmz,
			int nx, int ny, int nz, int starting_k,
			fp_type omega,
			fp_type**** v,
			fp_type*** tv,
			fp_type***** d,
			fp_type***** udx,
			fp_type***** udy,
			fp_type***** udz,
			int ist, int iend, int jst, int jend,
			int nx0, int ny0, int ipt, int jpt );
			
extern void erhs();

extern void error();
			
extern void exact(int i, int j, int k, fp_type u000ijk[5]);

extern void exchange_1(fp_type**** g, int k, int iex);		

extern void exchange_3(fp_type**** g, int iex);

extern void exchange_4(fp_type** g, fp_type** h, int ibeg, int ifin1, int jbeg, int jfin1);

extern void exchange_5(fp_type** g, int ibeg, int ifin1);

extern void exchange_6(fp_type** g, int jbeg, int jfin1);

extern void init_comm(int argc, char** argv);

extern void jacld(int starting_k);

extern void jacu(int starting_k);
			
extern void l2norm (int ldx, int ldy, int ldz,
			 int nx0, int ny0, int nz0,
			 int ist, int iend,
			 int jst, int jend,
			 fp_type**** v, fp_type sum[5]);
			 
extern void neighbors();			 

extern int nodedim(int num);

extern void pintgr();

extern void proc_grid();

extern void read_input();

extern void rhs();

extern void setbv();

extern void setcoeff();

extern void sethyper();

extern void setiv();

extern void ssor(int niter);

extern void subdomain();

extern int verify(fp_type xcr[5], fp_type xce[5], fp_type xci, char class_string);

#endif
