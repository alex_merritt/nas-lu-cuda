extern fp_type**** alloc_ursd(fp_type* space);
extern void free_ursd(fp_type**** pointers);
extern fp_type***** alloc_abcd(fp_type* space);
extern void free_abcd(fp_type***** pointers);
extern fp_type** alloc_buffer(fp_type* space);
extern void free_buffer(fp_type **pointers);
fp_type*** alloc_ibuf(fp_type* space);
void free_ibuf(fp_type*** pointers);
fp_type*** alloc_jbuf(fp_type* space);
void free_jbuf(fp_type*** pointers);
