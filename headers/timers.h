#ifndef __TIMERS_H__
#define __TIMERS_H__

	/**
	 * Timer struct.
	 */
	typedef struct {
	
		double cpu_start;
		double cpu_end;
		
		double wall_start;
		double wall_end;
		
		double cpu_total;
		double wall_total;
		
		int calls;
		
	} Timer;
	
	extern void timer_init(int number);
	extern void timer_reset();
	extern void timer_start(int i);
	extern void timer_stop(int i);
	extern void timer_pause(int i);
	extern void timer_restart(int i);
	extern void timer_finalize();
	
	extern double timer_cpu_total(int i);
	extern double timer_cpu_avg(int i);
	extern double timer_wall_total(int i);
	extern double timer_wall_avg(int i);
	
	extern void timer_print_format();
	extern void timer_print(const char* name, int i);

#endif
