/**
 * Definition of global variables defined by applu.h
 * 
 * Note: This file should ONLY be included by main.c.
 *       "#include applu.h" is sufficient to access these variables.
 */
int verified;
fp_type mflops;
char class;

int nx, ny, nz;
int nx0, ny0, nz0;
int ipt, ist, iend;
int jpt, jst, jend;
int ii1, ii2;
int ji1, ji2;
int ki1, ki2;
fp_type dxi, deta, dzeta;
fp_type tx1, tx2, tx3;
fp_type ty1, ty2, ty3;
fp_type tz1, tz2, tz3;
fp_type dx1, dx2, dx3, dx4, dx5;
fp_type dy1, dy2, dy3, dy4, dy5;
fp_type dz1, dz2, dz3, dz4, dz5;
fp_type dssp;
fp_type *u_flat, *rsd_flat, *frct_flat, *flux_flat;
fp_type ****u, ****rsd, ****frct, ****flux;
int ipr, inorm;
int itmax, invert;
fp_type dt, omega, tolrsd[5], rsdnm[5], errnm[5], frc, ttotal;
fp_type *a_flat, *b_flat, *c_flat, *d_flat;
fp_type *****a, *****b, *****c, *****d;
fp_type ce[13][5];
int id, ndim, num, xdim, ydim, row, col;
int north, south, east, west;
int icommn[npmax+1], icomms[npmax+1], icomme[npmax+1], icommw[npmax+1];
fp_type *buf_flat, *buf1_flat;
fp_type **buf, **buf1;
fp_type *jbuf_flat, *ibuf_flat;
fp_type ***jbuf, ***ibuf;
fp_type maxtime;

// Some MPI stuff.
int root;
