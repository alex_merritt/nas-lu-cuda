#ifndef __UTIL_H__
#define __UTIL_H__

	#include "applu.h"
	#include "timers.h"
	
	#define min(x,y) (x < y ? x : y)
	#define max(x,y) (x > y ? x : y)
	
	extern void swap_pointers(fp_type** x, fp_type** y);
	extern void parse_options(int argc, char* argv[]);
	extern void print_timers();
	extern void allocate_buffers();
	extern void free_buffers();

#endif
