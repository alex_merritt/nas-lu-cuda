// C port of NPB3.2
// subroutine exact( i, j, k, u000ijk )
#include "applu.h"

/**
 * Compute the exact solution at (i, j, k);
 */
void exact(int i, int j, int k, fp_type u000ijk[5]) {

	/**
	 * Local variables.
	 */
	int m;
	fp_type xi, eta, zeta;

	// Note: Originally i-1, j-1, k-1; this may need fixing later.
	/*xi = ((fp_type) (i - 1)) / (nx0 - 1);
	eta = ((fp_type) (j - 1)) / (ny0 - 1);
	zeta = ((fp_type) (k - 1)) / (nz - 1);*/
	xi = ((fp_type) (i - 2)) / (nx0 - 1);
	eta = ((fp_type) (j - 2)) / (ny0 - 1);
	zeta = ((fp_type) k) / (nz - 1);	

	for (m = 0; m < 5; m++) {

		u000ijk[m] = ce[0][m]
			+ ce[1][m] * xi
			+ ce[2][m] * eta
			+ ce[3][m] * zeta
			+ ce[4][m] * xi * xi
			+ ce[5][m] * eta * eta
			+ ce[6][m] * zeta * zeta
			+ ce[7][m] * xi * xi * xi
			+ ce[8][m] * eta * eta * eta
			+ ce[9][m] * zeta * zeta * zeta
			+ ce[10][m] * xi * xi * xi * xi
			+ ce[11][m] * eta * eta * eta * eta
			+ ce[12][m] * zeta * zeta * zeta * zeta;

		//printf("For i = %d, j = %d, k = %d, exact[%d] = %e.\n", i, j, k, m, u000ijk[m]);

	}

}
