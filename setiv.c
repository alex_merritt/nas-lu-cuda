// C port of NPB3.2
// subroutine setiv
#include "applu.h"

/**
 * Set the initial values of independent variables based on tri-linear
 * interpolation of boundary values in the computational space.
 */
void setiv() {

	/**
	 * Local variables.
	 */
	int i, j, k, m;
	int iglob, jglob;
	fp_type xi, eta, zeta;
	fp_type pxi, peta, pzeta;
	fp_type ue_1jk[5], ue_nx0jk[5], ue_i1k[5], ue_iny0k[5], ue_ij1[5], ue_ijnz[5];
	
	const fp_type fpone = 1.0e+00;

	for (k = 1; k <= nz - 2; k++) {
	
		zeta = ( (fp_type) k ) / (nz - 1);
		
		for (j = 2; j <= ny + 1; j++) {
		
			jglob = jpt + j;
			
			if (jglob != 2 && jglob != ny0 + 1) {
			
				eta = ( (fp_type) (jglob - 2) ) / (ny0 - 1);
				
				for (i = 2; i <= nx + 1; i++) {
				
					iglob = ipt + i;
					
					if (iglob != 2 && iglob != nx0 + 1) {
					
						xi = ( (fp_type) (iglob - 2) ) / (nx0 - 1);
						
						// Find exact solutions for all i, j, k.
						exact(2, jglob, k, ue_1jk);
						exact(nx0 + 1, jglob, k, ue_nx0jk);
						exact(iglob, 2, k, ue_i1k);
						exact(iglob, ny0 + 1, k, ue_iny0k);
						exact(iglob, jglob, 0, ue_ij1);
						exact(iglob, jglob, nz - 1, ue_ijnz);
						
						for (m = 0; m < 5; m++) {
						
							pxi = (fpone - xi) * ue_1jk[m]
											+ xi * ue_nx0jk[m];
											
							peta = (fpone - eta) * ue_i1k[m]
											+ eta  * ue_iny0k[m];
											
							pzeta = (fpone - zeta) * ue_ij1[m]
											 + zeta  * ue_ijnz[m];
											 
							u[k][j][i][m] = pxi + peta + pzeta
												- pxi * peta - peta * pzeta - pzeta * pxi
												+ pxi * peta * pzeta;
						
						}
						
					}
					
				}
				
			}
			
		}
		
	}

}
