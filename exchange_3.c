// C port of NPB3.2
// subroutine exchange_3
#include "mpinpb.h"
#include "applu.h"

/**
 * Compute the right hand side based on exact solution.
 */
void exchange_3(fp_type**** g, int iex) {

	/**
	 * Local variables.
	 */
	int i, j, k;
	int ipos1, ipos2;

	MPI_Request mid;
	MPI_Status status;
	
	/**
	 * Communicate in the south and north directions.
	 */
	if (iex == 0) {

		if (north != -1) {
			MPI_Irecv(buf1_flat, 10*ny*nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_n, MPI_COMM_WORLD, &mid);
		}

		/**
		 * Send south.
		 */
		if (south != -1) {
		
			for (k = 0; k <= nz - 1; k++) {
				for (j = 2; j <= ny + 1; j++) {
				
					ipos1 = k*ny + j - 2;
					ipos2 = ipos1 + ny*nz;
	
					buf[ipos1][0] = g[k][j][nx][0];		
					buf[ipos1][1] = g[k][j][nx][1];
					buf[ipos1][2] = g[k][j][nx][2];
					buf[ipos1][3] = g[k][j][nx][3];
					buf[ipos1][4] = g[k][j][nx][4];
					
					buf[ipos2][0] = g[k][j][nx+1][0];
					buf[ipos2][1] = g[k][j][nx+1][1];
					buf[ipos2][2] = g[k][j][nx+1][2];
					buf[ipos2][3] = g[k][j][nx+1][3];
					buf[ipos2][4] = g[k][j][nx+1][4];
																													
				}
			}

			MPI_Send(buf_flat, 10*ny*nz, MPI_FP_TYPE, south, from_n, MPI_COMM_WORLD);
			
		}

		/**
		 * Receive from north.
		 */
		if (north != -1) {
			
			MPI_Wait(&mid, &status);
			
			for (k = 0; k <= nz - 1; k++) {
				for (j = 2; j <= ny + 1; j++) {
				
					ipos1 = k*ny + j - 2;
					ipos2 = ipos1 + ny*nz;					
					
					g[k][j][0][0] = buf1[ipos1][0];
					g[k][j][0][1] = buf1[ipos1][1];
					g[k][j][0][2] = buf1[ipos1][2];
					g[k][j][0][3] = buf1[ipos1][3];
					g[k][j][0][4] = buf1[ipos1][4];
					
					g[k][j][1][0] = buf1[ipos2][0];
					g[k][j][1][1] = buf1[ipos2][1];
					g[k][j][1][2] = buf1[ipos2][2];
					g[k][j][1][3] = buf1[ipos2][3];
					g[k][j][1][4] = buf1[ipos2][4];					
					
				}
			}
			
		}

		if (south != -1) {
			MPI_Irecv(buf1_flat, 10*ny*nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_s, MPI_COMM_WORLD, &mid);
		}
		
		/**
		 * Send north.
		 */
		if (north != -1) {
		
			for (k = 0; k <= nz - 1; k++) {
				for (j = 2; j <= ny + 1; j++) {
				
					ipos1 = k*ny + j - 2;
					ipos2 = ipos1 + ny*nz;
					
					buf[ipos1][0] = g[k][j][3][0];
					buf[ipos1][1] = g[k][j][3][1];
					buf[ipos1][2] = g[k][j][3][2];
					buf[ipos1][3] = g[k][j][3][3];
					buf[ipos1][4] = g[k][j][3][4];
					
					buf[ipos2][0] = g[k][j][2][0];
					buf[ipos2][1] = g[k][j][2][1];
					buf[ipos2][2] = g[k][j][2][2];
					buf[ipos2][3] = g[k][j][2][3];
					buf[ipos2][4] = g[k][j][2][4];									
				
				}
			}
			
			MPI_Send(buf_flat, 10*ny*nz, MPI_FP_TYPE, north, from_s, MPI_COMM_WORLD);	
			
		}
		
		/**
		 * Receive from south.
		 */
		if (south != -1) {
		
			MPI_Wait(&mid, &status);
			
			for (k = 0; k <= nz - 1; k++){
				for (j = 2; j <= ny + 1; j++){
				
					ipos1 = k*ny + j - 2;
					ipos2 = ipos1 + ny*nz;
					
					g[k][j][nx+3][0] = buf1[ipos1][0];
					g[k][j][nx+3][1] = buf1[ipos1][1];
					g[k][j][nx+3][2] = buf1[ipos1][2];
					g[k][j][nx+3][3] = buf1[ipos1][3];
					g[k][j][nx+3][4] = buf1[ipos1][4];
					
					g[k][j][nx+2][0] = buf1[ipos2][0];
					g[k][j][nx+2][1] = buf1[ipos2][1];
					g[k][j][nx+2][2] = buf1[ipos2][2];
					g[k][j][nx+2][3] = buf1[ipos2][3];
					g[k][j][nx+2][4] = buf1[ipos2][4];
					
				}
			}
		
		}		 

	/**
	 * Communicate in the east and west directions.
	 */
	} else {
	
		if (west != -1) {
			MPI_Irecv(buf1_flat, 10*nx*nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_w, MPI_COMM_WORLD, &mid);
		}	
	
		/**
		 * Send east.
		 */
		if (east != -1) {
		
			for (k = 0; k <= nz - 1; k++) {
				for (i = 2; i <= nx + 1; i++){
				
					ipos1 = k*nx + i - 2;
					ipos2 = ipos1 + nx*nz;
					
					buf[ipos1][0] = g[k][ny][i][0];
					buf[ipos1][1] = g[k][ny][i][1];
					buf[ipos1][2] = g[k][ny][i][2];
					buf[ipos1][3] = g[k][ny][i][3];
					buf[ipos1][4] = g[k][ny][i][4];
					
					buf[ipos2][0] = g[k][ny+1][i][0];
					buf[ipos2][1] = g[k][ny+1][i][1];
					buf[ipos2][2] = g[k][ny+1][i][2];
					buf[ipos2][3] = g[k][ny+1][i][3];
					buf[ipos2][4] = g[k][ny+1][i][4];
					
				}
			}

			MPI_Send(buf_flat, 10*nx*nz, MPI_FP_TYPE, east, from_w, MPI_COMM_WORLD);
			
		}	 

		/**
		 * Receive from west.
		 */
		if (west != -1) {
		
			MPI_Wait(&mid, &status);
			
			for (k = 0; k <= nz - 1; k++){
				for (i = 2; i <= nx + 1; i++){
				
					ipos1 = k*nx + i - 2;
					ipos2 = ipos1 + nx*nz;
					
					g[k][0][i][0] = buf1[ipos1][0];
					g[k][0][i][1] = buf1[ipos1][1];
					g[k][0][i][2] = buf1[ipos1][2];
					g[k][0][i][3] = buf1[ipos1][3];
					g[k][0][i][4] = buf1[ipos1][4];
					
					g[k][1][i][0] = buf1[ipos2][0];
					g[k][1][i][1] = buf1[ipos2][1];
					g[k][1][i][2] = buf1[ipos2][2];
					g[k][1][i][3] = buf1[ipos2][3];
					g[k][1][i][4] = buf1[ipos2][4];
					
				}
			}
		
		}
		
		if (east != -1){
			MPI_Irecv(buf1_flat, 10*nx*nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_e, MPI_COMM_WORLD, &mid);
		}		


		/**
		 * Send west.
		 */
		if (west != -1) {
		
			for (k = 0; k <= nz - 1; k++){
				for (i = 2; i <= nx + 1; i++){
				
					ipos1 = k*nx + i - 2;
					ipos2 = ipos1 + nx*nz;
					
					buf[ipos1][0] = g[k][3][i][0];
					buf[ipos1][1] = g[k][3][i][1];
					buf[ipos1][2] = g[k][3][i][2];
					buf[ipos1][3] = g[k][3][i][3];
					buf[ipos1][4] = g[k][3][i][4];
					
					buf[ipos2][0] = g[k][2][i][0];
					buf[ipos2][1] = g[k][2][i][1];
					buf[ipos2][2] = g[k][2][i][2];
					buf[ipos2][3] = g[k][2][i][3];
					buf[ipos2][4] = g[k][2][i][4];
					
				}
			}

			MPI_Send(buf_flat, 10*nx*nz, MPI_FP_TYPE, west, from_e, MPI_COMM_WORLD);
			
		}		 

		/**
		 * Receive from east.
		 */
		if (east != -1) {
		
			MPI_Wait(&mid, &status);
			
			for (k = 0; k <= nz - 1; k++) {
				for (i = 2; i <= nx + 1; i++) {
				
					ipos1 = k*nx + i - 2;
					ipos2 = ipos1 + nx*nz;
					
					g[k][ny+3][i][0] = buf1[ipos1][0];
					g[k][ny+3][i][1] = buf1[ipos1][1];
					g[k][ny+3][i][2] = buf1[ipos1][2];
					g[k][ny+3][i][3] = buf1[ipos1][3];
					g[k][ny+3][i][4] = buf1[ipos1][4];
					
					g[k][ny+2][i][0] = buf1[ipos2][0];
					g[k][ny+2][i][1] = buf1[ipos2][1];
					g[k][ny+2][i][2] = buf1[ipos2][2];
					g[k][ny+2][i][3] = buf1[ipos2][3];
					g[k][ny+2][i][4] = buf1[ipos2][4];
					
				}
			}
		
		}

	}
	
}
