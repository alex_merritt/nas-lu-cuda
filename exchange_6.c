//subroutine exchange_6(g,jbeg,jfin1)

/**
 *   compute the right hand side based on exact solution
*/

//     implicit none

//      include 'mpinpb.h'
//      include 'applu.incl'

#include "mpinpb.h"
#include "applu.h"

void exchange_6(fp_type** g, int jbeg, int jfin1){


/**
 *  input parameters
*/
//      fp_type precision  g(0:isiz2+1,0:isiz3+1)
//      integer jbeg, jfin1

/**
 *  local parameters
*/

/**
      integer k
      fp_type precision  dum(1024)

      integer msgid3
      integer status(MPI_status_SIZE)
      integer IERROR
*/

	int k;
	fp_type dum[1024];
	MPI_Request msgid3;
	MPI_Status status;

/**
 *   communicate in the east and west directions
*/


/**
 *   receive from east
*/

/**
      if (jfin1.eq.ny) then
        call MPI_IRECV( dum,
     >                  nz,
     >                  dp_type,
     >                  MPI_ANY_SOURCE,
     >                  from_e,
     >                  MPI_COMM_WORLD,
     >                  msgid3,
     >                  IERROR )

        call MPI_WAIT( msgid3, status, IERROR )

        do k = 1,nz
          g(ny+1,k) = dum(k)
        end do

      end if
*/

	if( jfin1 == ny){
		MPI_Irecv(dum, nz, MPI_FP_TYPE, MPI_ANY_SOURCE, from_e, MPI_COMM_WORLD, &msgid3);
		MPI_Wait(&msgid3, &status);
		
		for(k = 1; k <= nz; k++){
			g[k][ny+1] =dum[k-1];
		}
		
	}      

/*
 *   send west
*/

/**
      if (jbeg.eq.1) then
        do k = 1,nz
          dum(k) = g(1,k)
        end do

        call MPI_SEND( dum,
     >                 nz,
     >                 dp_type,
     >                 west,
     >                 from_e,
     >                 MPI_COMM_WORLD,
     >                 IERROR )

      end if
*/

	if(jbeg == 1){
		for(k = 1; k <= nz; k++){
			dum[k-1] = g[k][1];
		}
		
		MPI_Send( dum, nz, MPI_FP_TYPE, west, from_e, MPI_COMM_WORLD); 
	}

//      return
//      end   
}  
