// C port of NPB3.2
#define VERSION "3.2"
#define FILENAME "size.h"
#include <stdio.h>
#include <math.h>

#define max(x,y) ((x > y) ? x : y)

/**
 * Integer log base two.
 * Return error if argument isn't a power of two or is less than or equal to zero.
 */
int ilog2(int i) {

	int log2;
	int exp2 = 1;

	if (i <= 0) {
		return -1;
	}

	for (log2 = 0; log2 < 20; log2++) {
	
		if (exp2 == i) {
			return log2;
		}
		exp2 *= 2;
	
	}
	return -1;

}

/**
 * Integer power of two.
 */
int ipow2(int i) {

	int pow2 = 1;
	
	if (i < 0) {
		return -1;
	}
	
	if (i == 0) {
		return 1;
	}
	
	while (i--) {
		pow2 *= 2;
	}
	
	return pow2;

}

/**
 * Creates an appropriate size.h file for a given class and number of processors.
 */
int main(int argc, char* argv[]) {

	// Check we have the right number of command line arguments.
	if (argc != 14) {
		//printf("Usage: ./setparams [number of processors] [problem class] [k-blocking] [gpu-blocking] [gpu tile size] [gpu grid size] [rhs-blocking] [rhs tile size] [rhs grid size]\n");
		printf("Usage: ./setparams [number of processors] [problem class] [k-blocking] [gpublock x] [gpublock y] [gpublock z] [gpugrid x] [gpugrid y] [rhsblock x] [rhsblock y] [rhsblock z] [rhsgrid x] [rhsgrid y]\n");		
		return -1;
	}
	
	// Define some variables for later.
	int nprocs, class, kblock, gpublock_x, gpublock_y, gpublock_z, gpugrid_x, gpugrid_y;
	int rhsblock_x, rhsblock_y, rhsblock_z, rhsgrid_x, rhsgrid_y;
	int isiz1, isiz2, itmax, inorm, problem_size;
	int xdiv, ydiv;
	char* dt_default;
	
	// Read the command line arguments.
	/*nprocs = atoi(argv[1]);
	class = *argv[2];
	kblock = atoi(argv[3]);
	gpublock = atoi(argv[4]);
	gputile = atoi(argv[5]);
	gpugrid = atoi(argv[6]);
	rhsblock = atoi(argv[7]);
	rhstile = atoi(argv[8]);
	rhsgrid = atoi(argv[9]);*/
	nprocs = atoi(argv[1]);
	class = *argv[2];
	kblock = atoi(argv[3]);
	gpublock_x = atoi(argv[4]);
	gpublock_y = atoi(argv[5]);
	gpublock_z = atoi(argv[6]);
	gpugrid_x = atoi(argv[7]);
	gpugrid_y = atoi(argv[8]);
	rhsblock_x = atoi(argv[9]);
	rhsblock_y = atoi(argv[10]);
	rhsblock_z = atoi(argv[11]);
	rhsgrid_x = atoi(argv[12]);
	rhsgrid_y = atoi(argv[13]);	
	
	// Open a size.h file for writing.
	FILE* file;
	file = fopen(FILENAME, "w");
	if (!file) {
		printf("Cannot open %s for writing.\n", FILENAME);
		return -1;
	}
	
	// Write the header.
	fprintf(file, "/**\n");
	fprintf(file, " * Define the problem and sub-problem sizes.\n");	
	fprintf(file, " */\n");
	fprintf(file, "\n");
	
	// Set the defaults for a given class.
	if      (class == 'S') { problem_size =  12; dt_default = "0.5e+00"; itmax =  50; }
	else if (class == 'W') { problem_size =  33; dt_default = "1.5e-03"; itmax = 300; }
	else if (class == 'A') { problem_size =  64; dt_default = "2.0e+00"; itmax = 250; }
	else if (class == 'B') { problem_size = 102; dt_default = "2.0e+00"; itmax = 250; }
	else if (class == 'C') { problem_size = 162; dt_default = "2.0e+00"; itmax = 250; }
	else if (class == 'D') { problem_size = 408; dt_default = "1.0e+00"; itmax = 300; }
	else if (class == 'E') { problem_size = 1020; dt_default = "0.5e+00"; itmax = 300; }
	else {
		printf("Unknown class %c.\n", class);
		fclose(file);
		return -1;
	}
	
	// Calculate the sub-problem size.
	inorm = itmax;
	xdiv = ydiv = ilog2(nprocs) / 2;
	if (xdiv + ydiv != ilog2(nprocs)) {
		xdiv += 1;
	}
	xdiv = ipow2(xdiv);
	ydiv = ipow2(ydiv);
	isiz1 = problem_size / xdiv;
	if (isiz1 * xdiv < problem_size) {
		isiz1++;
	}
	isiz2 = problem_size / ydiv;
	if (isiz2 * ydiv < problem_size) {
		isiz2++;
	}
	
	// Write the number of processors.
	fprintf(file, "// Number of processors compiled for.\n");
	fprintf(file, "#define nnodes_compiled %d\n", nprocs);
	fprintf(file, "\n");
	
	// Write the full array size.
	fprintf(file, "// Full array size.\n");
	fprintf(file, "#define isiz01 %d\n", problem_size);
	fprintf(file, "#define isiz02 %d\n", problem_size);
	fprintf(file, "#define isiz03 %d\n", problem_size);
	fprintf(file, "\n");
	
	// Write the array size per processor.
	fprintf(file, "// Array size per processor.\n");
	fprintf(file, "#define isiz1 %d\n", isiz1);
	fprintf(file, "#define isiz2 %d\n", isiz2);
	fprintf(file, "#define isiz3 isiz03\n");
	fprintf(file, "\n");
	
	// Write the other information.
	fprintf(file, "// Number of iterations, etc.\n");
	fprintf(file, "#define itmax_default %d\n", itmax);
	fprintf(file, "#define inorm_default %d\n", inorm);
	fprintf(file, "#define dt_default %s\n", dt_default);	
	fprintf(file, "\n");

	// Write the kblocking and gpu information.
	fprintf(file, "#define kblock %d\n", kblock);
	
	//fprintf(file, "#define problem_height %d\n", max(kblock, problem_size));
	
	// Defining problem height as the amount of memory that should be allocated in z direction for ursd.
	if (kblock >= problem_size) {
		fprintf(file, "#define problem_height %d\n", kblock);
	} else if ( (problem_size % kblock) != 0 ) {
		fprintf(file, "#define problem_height %d\n", ((problem_size / kblock) * kblock) + kblock);
	} else {
		fprintf(file, "#define problem_height %d\n", problem_size);
	}
	
	fprintf(file, "#define gpublock_x %d\n", gpublock_x);
	fprintf(file, "#define gpublock_y %d\n", gpublock_y);
	fprintf(file, "#define gpublock_z %d\n", gpublock_z);
	fprintf(file, "#define gpugrid_x %d\n", gpugrid_x);
	fprintf(file, "#define gpugrid_y %d\n", gpugrid_y);
	
	fprintf(file, "#define rhsblock_x %d\n", rhsblock_x);
	fprintf(file, "#define rhsblock_y %d\n", rhsblock_y);
	fprintf(file, "#define rhsblock_z %d\n", rhsblock_z);
	fprintf(file, "#define rhsgrid_x %d\n", rhsgrid_x);
	fprintf(file, "#define rhsgrid_y %d\n", rhsgrid_y);	
	
	/*fprintf(file, "#define gpublock %d\n", gpublock);
	fprintf(file, "#define gputile %d\n", gputile);
	fprintf(file, "#define gpugrid %d\n", gpugrid);
	fprintf(file, "#define rhstile %d\n", rhstile);
	fprintf(file, "#define rhsgrid %d\n", rhsgrid);
	fprintf(file, "#define rhsblock	%d\n", rhsblock);*/

	// Close the file and exit cleanly.
	fclose(file);
	return 0;

}
