// C port of NPB3.2
// subroutine exchange_1(g, k, iex)
#include "mpinpb.h"
#include "applu.h"
#include <stdio.h>
#include <string.h>

/**
 * iex = 0 : Receive north/west.
 * iex = 1 : Receive south/east.
 * iex = 2 : Send south/east.
 * iex = 3 : Send north/west.
 */
void exchange_1(fp_type**** g, int k, int iex){

	int i, j, z;
	
	MPI_Status status;

	/**
	 * Receive north/west.
	 */
	if (iex == 0) {
	
		// Receive from north.
		if (north != -1) {
		
			MPI_Recv(jbuf_flat, kblock*(jend-jst+1)*5, MPI_FP_TYPE, north, from_n, MPI_COMM_WORLD, &status);

            for (z = 0; z < kblock; z++) {
			    if (k + z >= 1 && k + z <= nz - 2) {
				    for (j = jst; j <= jend; j++) {		    				    				    				    
					
					    g[k + z][j][1][0] = jbuf[z][j - jst][0];
					    g[k + z][j][1][1] = jbuf[z][j - jst][1];
					    g[k + z][j][1][2] = jbuf[z][j - jst][2];
					    g[k + z][j][1][3] = jbuf[z][j - jst][3];
					    g[k + z][j][1][4] = jbuf[z][j - jst][4];																								
					
				    }
			    }
			}
		
		}
		
		// Receive from west.
		if (west != -1) {
		
			MPI_Recv(ibuf_flat, kblock*(iend-ist+1)*5, MPI_FP_TYPE, west, from_w, MPI_COMM_WORLD, &status);

            for (z = 0; z < kblock; z++) {
			    if (k + z >= 1 && k + z <= nz - 2) {
				    for (i = ist; i <= iend; i++) {
					
					    g[k + z][1][i][0] = ibuf[z][i - ist][0];
					    g[k + z][1][i][1] = ibuf[z][i - ist][1];
					    g[k + z][1][i][2] = ibuf[z][i - ist][2];
					    g[k + z][1][i][3] = ibuf[z][i - ist][3];
					    g[k + z][1][i][4] = ibuf[z][i - ist][4];																						
					
				    }	
			    }
			}
		
		}
	
	/**
	 * Receive south/east.
	 */
	} else if (iex == 1) {
	
		// Receive from south.
		if (south != -1) {
		
          	MPI_Recv(jbuf_flat, kblock*(jend-jst+1)*5, MPI_FP_TYPE, south, from_s, MPI_COMM_WORLD, &status);
          	
          	for (z = 0; z < kblock; z++) {
			    if (k + z - (kblock - 1) >= 1 && k + z - (kblock - 1) <= nz - 2) {
				    for (j = jst; j <= jend; j++) {
					
					    g[k + z - (kblock - 1)][j][nx+2][0] = jbuf[z][j - jst][0];
					    g[k + z - (kblock - 1)][j][nx+2][1] = jbuf[z][j - jst][1];
					    g[k + z - (kblock - 1)][j][nx+2][2] = jbuf[z][j - jst][2];
					    g[k + z - (kblock - 1)][j][nx+2][3] = jbuf[z][j - jst][3];
					    g[k + z - (kblock - 1)][j][nx+2][4] = jbuf[z][j - jst][4];
					
				    }
			    }
			}
		
		}
		
		// Receive from east.
		if (east != -1) {
		
			MPI_Recv(ibuf_flat, kblock*(iend-ist+1)*5, MPI_FP_TYPE, east, from_e, MPI_COMM_WORLD, &status);
	
	        for (z = 0; z < kblock; z++) {
			    if (k + z - (kblock - 1) >= 1 && k + z - (kblock - 1) <= nz - 2) {
				    for (i = ist; i <= iend; i++) {
				
					    g[k + z - (kblock - 1)][ny+2][i][0] = ibuf[z][i - ist][0];
					    g[k + z - (kblock - 1)][ny+2][i][1] = ibuf[z][i - ist][1];
					    g[k + z - (kblock - 1)][ny+2][i][2] = ibuf[z][i - ist][2];
					    g[k + z - (kblock - 1)][ny+2][i][3] = ibuf[z][i - ist][3];
					    g[k + z - (kblock - 1)][ny+2][i][4] = ibuf[z][i - ist][4];
					
				    }
			    }
			}
		
		}

	/**
	 * Send south/east.
	 */
	} else if (iex == 2) {
	
		// Send south.
		if (south != -1) {

            for (z = 0; z < kblock; z++) {
			    if (k + z - (kblock-1) >= 1 && k + z - (kblock-1) <= nz - 2) {
			
				    for (j = jst; j <= jend; j++) {
					    jbuf[z][j - jst][0] = g[k + z - (kblock-1)][j][nx+1][0];
					    jbuf[z][j - jst][1] = g[k + z - (kblock-1)][j][nx+1][1];
					    jbuf[z][j - jst][2] = g[k + z - (kblock-1)][j][nx+1][2];
					    jbuf[z][j - jst][3] = g[k + z - (kblock-1)][j][nx+1][3];
					    jbuf[z][j - jst][4] = g[k + z - (kblock-1)][j][nx+1][4];
				    }
				
			    }
			}
			
			// Send south.			
			MPI_Send(jbuf_flat, kblock*(jend-jst+1)*5, MPI_FP_TYPE, south, from_n, MPI_COMM_WORLD);			
			
		}
		
		// Send east.
		if (east != -1) {

            for (z = 0; z < kblock; z++) {
			    if (k + z - (kblock-1) >= 1 && k + z - (kblock-1) <= nz - 2) {

				    for (i = ist; i <= iend; i++) {
					    ibuf[z][i - ist][0] = g[k + z - (kblock-1)][ny+1][i][0];
					    ibuf[z][i - ist][1] = g[k + z - (kblock-1)][ny+1][i][1];
					    ibuf[z][i - ist][2] = g[k + z - (kblock-1)][ny+1][i][2];
					    ibuf[z][i - ist][3] = g[k + z - (kblock-1)][ny+1][i][3];
					    ibuf[z][i - ist][4] = g[k + z - (kblock-1)][ny+1][i][4];
				    }

			    }
			}
						
			MPI_Send(ibuf_flat, kblock*(iend-ist+1)*5, MPI_FP_TYPE, east, from_w, MPI_COMM_WORLD);
			
		}		
	
	/**
	 * Send north/west.
	 */
	} else {
	
		// Send north.
		if (north != -1) {

            for (z = 0; z < kblock; z++) {
			    if (k + z >= 1 && k <= nz - 2) {
			
				    for (j = jst; j <= jend; j++) {
					    jbuf[z][j - jst][0] = g[k + z][j][2][0];
					    jbuf[z][j - jst][1] = g[k + z][j][2][1];
					    jbuf[z][j - jst][2] = g[k + z][j][2][2];
					    jbuf[z][j - jst][3] = g[k + z][j][2][3];
					    jbuf[z][j - jst][4] = g[k + z][j][2][4];
				    }

			    }   
			}

			MPI_Send(jbuf_flat, kblock*(jend-jst+1)*5, MPI_FP_TYPE, north, from_s, MPI_COMM_WORLD);		
			
		}

		// Send west.
		if (west != -1) {
		
            for (z = 0; z < kblock; z++) {		
			    if (k + z >= 1 && k <= nz - 2) {

				    for (i = ist; i <= iend; i++) {
					    ibuf[z][i - ist][0] = g[k + z][2][i][0];
					    ibuf[z][i - ist][1] = g[k + z][2][i][1];
					    ibuf[z][i - ist][2] = g[k + z][2][i][2];
				        ibuf[z][i - ist][3] = g[k + z][2][i][3];
					    ibuf[z][i - ist][4] = g[k + z][2][i][4];
				    }
		
			    }
			}

			MPI_Send(ibuf_flat, kblock*(iend-ist+1)*5, MPI_FP_TYPE, west, from_e, MPI_COMM_WORLD);
			
		}

	}

}
