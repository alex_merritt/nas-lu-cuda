// C port of NPB3.2
// subroutine ssor(niter)
#include "applu.h"
#include "mpinpb.h"
#include "timers.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * To perform pseudo-time stepping SSOR iterations for give nonlinear pde's.
 */
void ssor(int niter) {

	/**
	 * Local variables.
	 */
	int i, j, k, m, l;
	int istep;
	fp_type tmp;
	fp_type delunm[5], ***tv;
	root = 0;
	
	// Allocate memory for tv.
	tv = (fp_type***) malloc( (isiz2 + 4) * sizeof(fp_type**) );
	for (k = 0; k < (isiz2 + 4); k++) {
		tv[k] = (fp_type**) malloc ( (isiz1 + 4) * sizeof(fp_type*) );	
	}		
	for (k = 0; k < (isiz2 + 4); k++) {
		for (j = 0; j < (isiz1 + 4); j++) {
			tv[k][j] = (fp_type*) malloc ( 5 * sizeof(fp_type) );
		}
	}			
	
	// Begin pseudo-time stepping iterations.
	const fp_type fpzero = 0.0e+00;
	const fp_type fpone = 1.0e+00;
	const fp_type fptwo = 2.0e+00;
	tmp = fpone / ( omega * ( fptwo - omega ) );
	
	// Initialize a, b, c, d to zero (guarantees that page tables have been formed, if
	// applicable on given architecture, before timestepping.
	for (k = 0; k < kblock; k++) {
		for (j = 0; j < (isiz2 + 4); j++) {
			for (i = 0; i < (isiz1 + 4); i++) {
				for (l = 0; l < 5; l++) {
					for (m = 0; m < 5; m++) {
						a[k][j][i][l][m] = fpzero;
						b[k][j][i][l][m] = fpzero;
						c[k][j][i][l][m] = fpzero;
						d[k][j][i][l][m] = fpzero;																		
					}
				}
			}
		}
	}
	
	// Compute the steady-state residuals.
	rhs();

	// Compute the L2 norms of newton iteration residuals.
	l2norm(isiz1, isiz2, isiz3, nx0, ny0, nz0, ist, iend, jst, jend, rsd, rsdnm);
	MPI_Barrier(MPI_COMM_WORLD);

	// Make sure all aggregate timers are initialised.
	timer_reset();

	// Start the timer for the ssor loop.
	timer_start(0);
		
	// Start the timestep loop - do for niter times.
	for (istep = 1; istep <= niter; istep++) {
	
		if (niter == 1) {
			printf(" k-blocking factor of %d.\n", kblock);
		}
	
		if (id == 0) {
			if (istep % 20 == 0 || istep == itmax || istep == 1) {
				if (niter > 1) {
					printf(" Pseudo-time SSOR iteration no. = %d.\n", istep);
				}
			}
		}
		
		// Perform SSOR iteration.
		timer_start(1);		
		for (k = 0; k < nz; k++) {
			for (j = 0; j < ny + 4; j++) {
				for (i = 0; i < nx + 4; i++) {
				
					if (k >= 1 && k <= nz - 2
						&& j >= jst && j <= jend
						&& i >= ist && i <= iend) {				
				
						for (m = 0; m < 5; m++) {						
							rsd[k][j][i][m] = dt * rsd[k][j][i][m];
						}
					
					}
					
				}
			}
		}
		timer_stop(1);
		
		// For each tile in k-dimension (from bottom to top).		
		for (k = 0; k < nz; k += kblock) {
			
			// Form the lower triangular part of the jacobian matrix.
			timer_start(2);
			jacld(k);								
			timer_stop(2);

			// Perform the lower triangular solution.
			timer_start(3);
			blts(isiz1, isiz2, isiz3, nx, ny, nz, k, omega, rsd, a, b, c, d, ist, iend, jst, jend, nx0, ny0, ipt, jpt);				
			timer_stop(3);				
			
		}		
	
		// For each tile in k-dimension (from top to bottom).
		for (k = nz - 1; k >= 0; k -= kblock) {
			
			// Form the strictly upper triangular part of the jacobian matrix.
			timer_start(4);
			jacu(k);					
			timer_stop(4);
			
			// Perform the upper triangular solution.
			timer_start(5);
			buts(isiz1, isiz2, isiz3, nx, ny, nz, k, omega, rsd, tv, d, a, b, c, ist, iend, jst, jend, nx0, ny0, ipt, jpt);			
			timer_stop(5);
			
		}

		// Update the variables.
		timer_start(6);	
		for (k = 0; k < nz; k++) {
			for (j = 0; j < ny + 4; j++) {
				for (i = 0; i < nx + 4; i++) {
				
					if (k >= 1 && k <= nz - 2
						&& j >= jst && j <= jend
						&& i >= ist && i <= iend) {					
				
						for (m = 0; m < 5; m++) {
							u[k][j][i][m] = u[k][j][i][m] + tmp * rsd[k][j][i][m];
						}
						
					}
					
				}
			}
		}
		timer_stop(6);

		// Compute the max-norms of newton iteration corrections.
		// l2norm seems to contain a collective MPI operation.
		if (istep % inorm == 0) {
			l2norm(isiz1, isiz2, isiz3, nx0, ny0, nz0, ist, iend, jst, jend, rsd, delunm);
			if (ipr == 1 && id == 0) {
				printf(" RMS-norm of SSOR-iteration correction for first pde = %e.\n", delunm[0]);
				printf(" RMS-norm of SSOR-iteration correction for second pde = %e.\n", delunm[1]);
				printf(" RMS-norm of SSOR-iteration correction for third pde = %e.\n", delunm[2]);
				printf(" RMS-norm of SSOR-iteration correction for fourth pde = %e.\n", delunm[3]);
				printf(" RMS-norm of SSOR-iteration correction for fifth pde = %e.\n", delunm[4]);	
			} else if (ipr == 2 && id == 0) {
				printf(" %d, %f.\n", istep, delunm[4]);
			}
		}
		
		// Compute the steady-state residuals.
		timer_start(8);
		rhs();
		timer_stop(8);	

		// Compute the max-norms of newton iteration residuals.
		// l2norm seems to contain a collective MPI operation.
		if (istep % inorm == 0 || istep == itmax ) {
			l2norm(isiz1, isiz2, isiz3, nx0, ny0, nz0, ist, iend, jst, jend, rsd, rsdnm);
			if (ipr == 1 && id == 0) {
				printf(" RMS-norm of steady state residual for first pde = %e.\n", rsdnm[0]);
				printf(" RMS-norm of steady state residual for second pde = %e.\n", rsdnm[1]);
				printf(" RMS-norm of steady state residual for third pde = %e.\n", rsdnm[2]);
				printf(" RMS-norm of steady state residual for fourth pde = %e.\n", rsdnm[3]);
				printf(" RMS-norm of steady state residual for fifth pde = %e.\n", rsdnm[4]);																
			}
		}
		
		// Check the newton-iteration residuals against the tolerance levels.
		if (rsdnm[0] < tolrsd[0] && rsdnm[1] < tolrsd[1] && rsdnm[2] < tolrsd[2]
			&& rsdnm[3] < tolrsd[3] && rsdnm[4] < tolrsd[4]) {
			if (ipr == 1 && id == 0) {
				printf(" Convergence was achieved after %d pseudo-time steps.\n", istep);
				return;
			}
		}
	
	}
	
	// End the timers.
	timer_stop(0);
	
	// Free memory for tv.
	for (k = 0; k < (isiz2 + 4); k++) {
		for (j = 0; j < (isiz1 + 4); j++) {
			free(tv[k][j]);
		}
	}	
	for (k = 0; k < (isiz2 + 4); k++) {
		free(tv[k]);	
	}
	free(tv);
	
}
